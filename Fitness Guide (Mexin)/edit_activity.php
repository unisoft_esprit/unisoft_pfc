<?php 
    include 'inc/connection.php';
    include 'inc/functions.php';
    include 'inc/form_functions.php';
    include 'inc/session.php';
?>


<?php confirm_logged_in(); ?>
<?php
        
        if (isset($_POST['submit'])) {
                            /*
                                fk_activity_type = (SELECT name_activity_type FROM ftw_activities_type WHERE name_activity_type = '{$activity_type}'), 
                            target_activity = '{$target_activity}' ,
                            benefits_activity = '{$benefits_activity}'
                            */
            
                // Perform Update
                $id = $_GET['act_id'];
                $name_activity= mysql_prep($_POST['name_activity']);
                $activity_type =mysql_prep( $_POST['activity_type']);
                $target_activity = mysql_prep($_POST['target_activity']);
                $benefits_activity = mysql_prep($_POST['benefits_activity']);
                $featured_img = $_POST['featured_img'];
                $Video = $_POST['Video'];
                
                $query = "UPDATE ftw_activities SET 
                            name_activity = '{$name_activity}',
                            fk_activity_type = (SELECT activity_type_name FROM ftw_activities_type WHERE activity_type_name = '{$activity_type}'), 
                            target_activity = '{$target_activity}' ,
                            benefits_activity = '{$benefits_activity}'
                            
                        WHERE id_activity = {$id}";
                $result = mysql_query($query, $connection);
                confirm_query($result);
                 $query = "UPDATE ftw_medias_activities SET 
                            link_media = '{$featured_img}' ,
                            
                        WHERE fk_id_club = {$id} AND type_media = 'img' ";
                $result = mysql_query($query, $connection);
                confirm_query($result);
                $query = "UPDATE ftw_medias_activities SET 
                            link_media = '{$Video}' ,
                            
                        WHERE fk_id_club = {$id} AND type_media = 'vid' ";
                $result = mysql_query($query, $connection);
                confirm_query($result);
                
                
            
            
            echo "$message";
            
            
        } // end: if (isset($_POST['submit']))
?>
<?php find_selected_Activity(); ?>

<!DOCTYPE html>
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
    <!--<![endif]-->
    <head>

        <!-- Basic Page Needs
        ================================================== -->
        <meta charset="utf-8">
        <title>Edit Activity</title>
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Mobile Specific Metas
        ================================================== -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <!-- CSS
        ================================================== -->
        <link rel="stylesheet" href="css/style.css" type="text/css"  media="all">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,900|Roboto|Roboto+Slab:300,400' rel='stylesheet' type='text/css'>

        <!-- JS
        ================================================== -->
        <script type="text/javascript" src="js/jquery.min.js" ></script>
        <!--[if lt IE 9]>
        <script src="js/modernizr.custom.11889.js" type="text/javascript"></script>
        <![endif]-->
        <!-- HTML5 Shiv events (end)-->
        <script type="text/javascript" src="js/nav-resp.js"></script>
        <!-- Favicons
        ================================================== -->
        <link rel="shortcut icon" href="images/favicon.ico">

    </head>
    <body>

        <!-- Primary Page Layout
        ================================================== -->

        <div id="boxed-wrap" class="colorskin-0">
            <div class="top-bar">
                <div class="container">
                    <div class="top-links">
                        <a href="#">Form</a> | <a href="#">Terms</a> | <a href="#">Contact</a>
                    </div>
                    <div class="socailfollow">
                        <a href="#" class="facebook"><i class="icomoon-facebook"></i></a><a href="#" class="dribble"><i class="icomoon-dribbble"></i></a><a href="#" class="vimeo"><i class="icomoon-vimeo"></i></a><a href="#" class="google"><i class="icomoon-google"></i></a><a href="#" class="twitter"><i class="icomoon-twitter"></i></a>
                    </div>
                </div>
            </div>
            <header id="header">
                <?php include 'inc/admin_header.php' ?>

            </header>
            <!-- end-header -->
             <?php find_selected_Activity(); ?>
            <section id="headline">
                <div class="container">
                    <h3>Edit Activity <br> <small><?php  echo $sel_activity['name_activity'];?></small> </h3>
                </div>
            </section>
            <section class="container page-content" >

                <hr class="vertical-space3">
           
            <form action="edit_activity.php?act_id=<?php echo urlencode($sel_activity['id_activity'])?>" method="post" id="frmContact">
                <div class="seven columns">
                    
                    <div class="contact-form">
                        <div class="clr"></div>
                        <br />
                            <h5>Activity Name</h5>
                            <input name="name_activity" type="text" class="txbx"  id="name_activity" name="name_activity" value="<?php  echo $sel_activity['name_activity'];?>"/>
                            <br />
                            <h5>Select Activity Type</h5>
                            <select name="activity_type" class=" txbx" id="activity_type">
                                                    <?php 
                                                        $activity_type_set = get_all_activities_type();
                                                         while ( $activity_type = mysql_fetch_array($activity_type_set) ) { ?>
                                                    <option value="<?php $activity_type['activity_type_name'] ?>" <?php  //if ($sel_activity['activity_type']==$activity_type['activity_type_name']) {echo " selected";}?> >
                                                    <?php echo $activity_type['activity_type_name'] ?></option> 
                                                    <?php } ?>

                                                    <?php
                                                        /*$subject_set = get_all_subjects();
                                                        $subject_count = mysql_num_rows($subject_set);
                                                        // $subject_count + 1 b/c we are adding a subject
                                                        for($count=1; $count <= $subject_count+1; $count++) {
                                                            echo "<option value=\"{$count}\"";
                                                            if ($sel_subject['position'] == $count) {
                                                                echo " selected";
                                                            } 
                                                            echo ">{$count}</option>";
                                                        }*/
                                                    ?>
                                                </select> 
                            <br />
                            <div class="erabox">
                                <h5>Description</h5>
                                <textarea name="description_activity" class="txbx era" ><?php  echo $sel_activity['description_activity'];?></textarea>
<br />                                

                                <div id="spanMessage"></div>
                            </div>
                        
                    </div><!-- end-contact-form  -->
                </div>

                <div class="eight columns">
                    <div class="contact-form">
                        
                            
                                <h5>Target Text</h5>
                                <textarea name="target_activity" class="txbx era" ><?php  echo $sel_activity['target_activity'];?></textarea>
                                <br />                               

                                <div id="spanMessage"></div>
                            
                            <div class="erabox">
                                <h5>Benefits Text</h5>
                                <textarea name="benefits_activity" class="txbx era" ><?php  echo $sel_activity['benefits_activity'];?></textarea>
                                <br />                               

                                <div id="spanMessage"></div>



                                <h5>Featured image</h5>
                            <input name="featured_img" type="text" class="txbx"  id="name_activity" name="name_activity" value="<?php  echo $sel_activity_img;?>"/>
                            <br />

                                

                                <h5>Video</h5>
                                <input name="Video" type="text" class="txbx"  id="name_activity" name="name_activity" value="<?php  echo $sel_activity_vid ; ?>"/>
                                <br />
                            </div>
                    </div><!-- end-contact-form  -->

                </div>
                <center><input name="submit" type="submit" class="sendbtn green large" value="Edit Activity" id="btnSend"/></center>
                
            </form>

                
                    
                
                <div class="white-space"></div>
            </section><!-- container -->
        

            <footer id="footer">
                <section class="container footer-in">
                    <div class="one-third column contact-inf">
                        <h4 class="subtitle">Contact Information</h4>
                        <br />
                        <p>
                            <strong>Address: </strong> No.28 - 63739 street lorem ipsum City, Country
                        </p>
                        <p>
                            <strong>Phone: </strong> + 1 (234) 567 8901
                        </p>
                        <p>
                            <strong>Fax: </strong> + 1 (234) 567 8901
                        </p>
                        <p>
                            <strong>Email: </strong> support@yoursite.com
                        </p>
                        <h4 class="subtitle">Stay Connected</h4>
                        <div class="socailfollow">
                            <a href="#" class="facebook"><i class="icomoon-facebook"></i></a><a href="#" class="dribble"><i class="icomoon-dribbble"></i></a><a href="#" class="pinterest"><i class="icomoon-pinterest-2" aria-hidden="true"></i></a><a href="#" class="vimeo"><i class="icomoon-vimeo"></i></a><a href="#" class="google"><i class="icomoon-google"></i></a><a href="#" class="twitter"><i class="icomoon-twitter"></i></a><a href="#" class="youtube"><i class="icomoon-youtube"></i></a>
                        </div>
                    </div>
                    <!-- end-contact-info /end -->

                    <div class="one-third column">
                        <h4 class="subtitle">latest tweet</h4>
                        <br />
                        <div class="lts-tweets">
                            <i class="icomoon-twitter"></i>
                            <h3><a href="https://twitter.com/webnus">@webnus</a></h3>
                            <h5 id="twitter"></h5>
                        </div>
                    </div>
                    <!-- tweets  /end -->

                    <div class="one-third column">
                        <h4 class="subtitle">flickr photostream</h4>
                        <br />
                        <div class="flickr-feed">
                            <script type="text/javascript" src="http://www.flickr.com/badge_code.gne?count=12&amp;display=random&amp;size=square&amp;nsid=36587311@N08&amp;raw=1"></script>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <!-- flickr /end -->
                </section>
                <!-- end-footer-in -->
                <section class="footbot">
                    <div class="container">
                        <div class="footer-navi">
                            © 2012. All Rights Reserved. Powered by <a href="http://wordpress.org/">WordPress</a>
                        </div>
                        <!-- footer-navigation /end -->
                        <img src="images/logo-footer-retina.png" width="65" alt="">
                    </div>
                </section>
                <!-- end-footbot -->
            </footer>
            <!-- end-footer -->
            <span id="scroll-top"><a class="scrollup"><i class="icomoon-arrow-up"></i></a></span>
        </div><!-- end-wrap -->

        <!-- End Document
        ================================================== -->
        <script type="text/javascript" src="js/mexin-custom.js" ></script>
        <script type="text/javascript" src="js/doubletaptogo.js" ></script>
        <script type="text/javascript" src="js/bootstrap-alert.js"></script>

    </body>
</html>