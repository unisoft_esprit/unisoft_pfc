<?php 
    include 'inc/connection.php';
    include 'inc/functions.php';
?>
<!DOCTYPE html>
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>

	<!-- Basic Page Needs
  ================================================== -->
	<meta charset="utf-8">
	<title>Columns - Mexin</title>
	<meta name="description" content="">
	<meta name="author" content="">

	<!-- Mobile Specific Metas
  ================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- CSS
  ================================================== -->
	<link rel="stylesheet" href="css/style.css" type="text/css"  media="all">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,900|Roboto|Roboto+Slab:300,400' rel='stylesheet' type='text/css'>

	<!-- JS
  ================================================== -->
   <script type="text/javascript" src="js/jquery.min.js" ></script>
	<!--[if lt IE 9]>
	<script src="js/modernizr.custom.11889.js" type="text/javascript"></script>
	<![endif]-->
		<!-- HTML5 Shiv events (end)-->
    <script type="text/javascript" src="js/nav-resp.js"></script>
	<!-- Favicons
  ================================================== -->
	<link rel="shortcut icon" href="images/favicon.ico">

    </head>
<body>
	<?php 
        
        $sel_activity = get_activity_by_id($_GET['act_id']);

    ?>

	<!-- Primary Page Layout
	================================================== -->

<div id="boxed-wrap" class="colorskin-0">
<div class="top-bar">
<div class="container">
<div class="top-links"> <a href="#">Form</a> | <a href="#">Terms</a> | <a href="#">Contact</a></div>
<div class="socailfollow"><a href="#" class="facebook"><i class="icomoon-facebook"></i></a> <a href="#" class="dribble"><i class="icomoon-dribbble"></i></a>  <a href="#" class="vimeo"><i class="icomoon-vimeo"></i></a><a href="#" class="google"><i class="icomoon-google"></i></a> <a href="#" class="twitter"><i class="icomoon-twitter"></i></a></div>
</div>
</div>
<header id="header">
	<?php include 'inc/admin_header.php' ?>

</header>
<!-- end-header -->

<section id="headline">
<div class="container">
<h3><?php echo $sel_activity['name_activity']; ?><br><small><?php echo $sel_activity['fk_activity_type']; ?></h3>
</div>
</section>
<section class="container page-content" >
<hr class="vertical-space2">
<div class="sixteen columns">

	
	<div class="one_third">
		<img src="<?php echo get_featured_img_act($_GET['act_id']) ; ?>" alt="">
	</div>
	<div class="one_half column-last">
		<h4 class="sub-header">Activity Description </h4>
        <p><?php echo $sel_activity['description_activity']; ?></p>
	</div>
	<br />
	<hr class="vertical-space2">
	<ul id="myTab" class="nav nav-tabs">
        <li class="active"><a href="#Targets" data-toggle="tab">Target</a>
        </li>
        <li ><a href="#Benefits" data-toggle="tab">Benefits</a>
        </li>
        <li ><a href="#Video" data-toggle="tab">Video</a>
        </li>
        
        
    </ul>
    <div id="myTabContent" class="tab-content">
        <div class="tab-pane active" id="Targets">
            <h4 class="sub-header">Target</h4>
          	<table class="table table-hover table-striped">
            <tbody>
                <?php explode_text_to_row($sel_activity['target_activity']);?>
             </tbody>
        	</table>
        </div>
        <div class="tab-pane " id="Benefits">
            <h4 class="sub-header">Benefits</h4>
            <table class="table table-hover table-striped">
                <tbody>
                    <?php explode_text_to_row($sel_activity['benefits_activity']);?>
                 </tbody>
            </table>
        </div>

        <div class="tab-pane active" id="Video">
            <h4 class="sub-header">Video</h4>
            <table class="table table-hover table-striped">
            <tbody>
              <object type="application/x-shockwave-flash" data="http://www.masalledesport.com/./includes/flamL/plugins/flash/player.swf" width="500" height="390" id="video" style="visibility: visible;">
                      <param name="allowfullscreen" value="true">
                      <param name="allowScriptAccess" value="always">
                      <param name="flashvars" value="file=<?php echo get_video_act($_GET['act_id']) ; ?>&amp;autostart=true">
                    </object>
             </tbody>
          </table>
        </div>
	
    </div>

	
	
</div>






</section><!-- container -->

<footer id="footer">
    <section class="container footer-in">
	      <div class="one-third column contact-inf">
        <h4 class="subtitle">Contact Information</h4>
        <br />
        <p><strong>Address: </strong> No.28 - 63739 street lorem ipsum City, Country</p>
        <p><strong>Phone: </strong> + 1 (234) 567 8901 </p>
        <p><strong>Fax: </strong> + 1 (234) 567 8901 </p>
        <p><strong>Email: </strong> support@yoursite.com </p>
		<h4 class="subtitle">Stay Connected</h4>
		        <div class="socailfollow"><a href="#" class="facebook"><i class="icomoon-facebook"></i></a> <a href="#" class="dribble"><i class="icomoon-dribbble"></i></a> <a href="#" class="pinterest"><i class="icomoon-pinterest-2" aria-hidden="true"></i></a> <a href="#" class="vimeo"><i class="icomoon-vimeo"></i></a><a href="#" class="google"><i class="icomoon-google"></i></a> <a href="#" class="twitter"><i class="icomoon-twitter"></i></a> <a href="#" class="youtube"><i class="icomoon-youtube"></i></a> </div>
      </div>
      <!-- end-contact-info /end -->

      <div class="one-third column">
        <h4 class="subtitle">latest tweet</h4>
        <br />
        <div class="lts-tweets">
		<i class="icomoon-twitter"></i>
		<h3><a href="https://twitter.com/webnus">@webnus</a></h3>
		<h5 id="twitter"></h5>
        </div>
      </div>
      <!-- tweets  /end -->
	  
	  <div class="one-third column">
        <h4 class="subtitle">flickr photostream</h4>
        <br />
        <div class="flickr-feed">
          <script type="text/javascript" src="http://www.flickr.com/badge_code.gne?count=12&amp;display=random&amp;size=square&amp;nsid=36587311@N08&amp;raw=1"></script>
          <div class="clear"></div>
        </div>
      </div>
      <!-- flickr /end -->
	   </section>
    <!-- end-footer-in -->
    <section class="footbot">
	<div class="container">
      <div class="footer-navi">© 2012. All Rights Reserved. Powered by <a href="http://wordpress.org/">WordPress</a>  </div>
	  <!-- footer-navigation /end -->
      <img src="images/logo-footer-retina.png" width="65" alt="">	  </div>
	  </section>
    <!-- end-footbot -->
  </footer>
<!-- end-footer -->
<span id="scroll-top"><a class="scrollup"><i class="icomoon-arrow-up"></i></a></span>
</div><!-- end-wrap -->

<!-- End Document
================================================== -->
<
<script type="text/javascript" src="js/mexin-custom.js" ></script>
<script type="text/javascript" src="js/doubletaptogo.js" ></script>
<script src="js/bootstrap-alert.js"></script>
<script src="js/bootstrap-dropdown.js"></script>
<script src="js/bootstrap-tab.js"></script>
<script src="js/bootstrap-tooltip.js"></script>
<!--
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="http://getbootstrap.com/dist/js/bootstrap.min.js"></script>
<script src="http://getbootstrap.com/assets/js/docs.min.js"></script>
-->
</body>
</html>