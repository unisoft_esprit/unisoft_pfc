<?php 
    include 'inc/connection.php';
    include 'inc/functions.php';
?>



<!DOCTYPE html>
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>

	<!-- Basic Page Needs
  ================================================== -->
	<meta charset="utf-8">
	<title>Columns - Mexin</title>
	<meta name="description" content="">
	<meta name="author" content="">

	<!-- Mobile Specific Metas
  ================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- CSS
  ================================================== -->
	<link rel="stylesheet" href="css/style.css" type="text/css"  media="all">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,900|Roboto|Roboto+Slab:300,400' rel='stylesheet' type='text/css'>

	<!-- JS
  ================================================== -->
   <script type="text/javascript" src="js/jquery.min.js" ></script>
   <script  type="text/javascript" src="js/jquery.sticky.js"></script>
	<!--[if lt IE 9]>
	<script src="js/modernizr.custom.11889.js" type="text/javascript"></script>
	<![endif]-->
		<!-- HTML5 Shiv events (end)-->
    <script type="text/javascript" src="js/nav-resp.js"></script>
	<!-- Favicons
  ================================================== -->
	<link rel="shortcut icon" href="images/favicon.ico">

    </head>
<body>

	<!-- Primary Page Layout
	================================================== -->

<div id="boxed-wrap" class="colorskin-0">
<div class="top-bar">
<div class="container">
<div class="top-links"> <a href="#">Form</a> | <a href="#">Terms</a> | <a href="#">Contact</a></div>
<div class="socailfollow"><a href="#" class="facebook"><i class="icomoon-facebook"></i></a> <a href="#" class="dribble"><i class="icomoon-dribbble"></i></a>  <a href="#" class="vimeo"><i class="icomoon-vimeo"></i></a><a href="#" class="google"><i class="icomoon-google"></i></a> <a href="#" class="twitter"><i class="icomoon-twitter"></i></a></div>
</div>
</div>
<div class = "nav " >
<header id="header">
	
		<?php include 'inc/admin_header.php' ?>
	
	
</header>
</div>
<!-- end-header -->

<section id="headline">
<div class="container ">
<h3>Clubs List</h3>
</div>
</section>
<section class="container page-content" >
<hr class="vertical-space2">
<div><a href="ftw_admin_add_club.php"><button type="button" class="green large">Add</button></a></div>
<div class="sixteen columns">

	<?php 
		$club_set = get_all_clubs();
		while ($club = mysql_fetch_array($club_set)) {
			

	?>

	<hr>
	<div class="one_fifth">
		<img src="<?php echo get_featured_img($club['id_club']) ?>" class="media-object img-rounded" alt="Image">
		
		<a href="edit_club.php?clb_id=<?php echo urlencode($club['id_club']); ?>"><button type="button" class="small orange center">Edit</button></a>
		<a href="delete_club.php?clb_id=<?php echo urlencode($club['id_club']); ?>"><button type="button" class="small red">Delete</button></a>
		
	</div>
	<div class="two_third">
		<h4 class="media-heading"><a href="ftw_admin_club_single.php?clb_id=<?php echo urlencode($club['id_club']); ?>"><?php echo $club['name_club']; ?></a></h4>
        <p><?php echo_200($club['description_club']); ?><br></p>
	</div>
	<?php } ?>
	<hr>
</div>

</section><!-- container -->
<footer id="footer">
    <section class="container footer-in">
	      <div class="one-third column contact-inf">
        <h4 class="subtitle">Contact Information</h4>
        <br />
        <p><strong>Address: </strong> No.28 - 63739 street lorem ipsum City, Country</p>
        <p><strong>Phone: </strong> + 1 (234) 567 8901 </p>
        <p><strong>Fax: </strong> + 1 (234) 567 8901 </p>
        <p><strong>Email: </strong> support@yoursite.com </p>
		<h4 class="subtitle">Stay Connected</h4>
		        <div class="socailfollow"><a href="#" class="facebook"><i class="icomoon-facebook"></i></a> <a href="#" class="dribble"><i class="icomoon-dribbble"></i></a> <a href="#" class="pinterest"><i class="icomoon-pinterest-2" aria-hidden="true"></i></a> <a href="#" class="vimeo"><i class="icomoon-vimeo"></i></a><a href="#" class="google"><i class="icomoon-google"></i></a> <a href="#" class="twitter"><i class="icomoon-twitter"></i></a> <a href="#" class="youtube"><i class="icomoon-youtube"></i></a> </div>
      </div>
      <!-- end-contact-info /end -->

      <div class="one-third column">
        <h4 class="subtitle">latest tweet</h4>
        <br />
        <div class="lts-tweets">
		<i class="icomoon-twitter"></i>
		<h3><a href="https://twitter.com/webnus">@webnus</a></h3>
		<h5 id="twitter"></h5>
        </div>
      </div>
      <!-- tweets  /end -->
	  
	  <div class="one-third column">
        <h4 class="subtitle">flickr photostream</h4>
        <br />
        <div class="flickr-feed">
          <script type="text/javascript" src="http://www.flickr.com/badge_code.gne?count=12&amp;display=random&amp;size=square&amp;nsid=36587311@N08&amp;raw=1"></script>
          <div class="clear"></div>
        </div>
      </div>
      <!-- flickr /end -->
	   </section>
    <!-- end-footer-in -->
    <section class="footbot">
	<div class="container">
      <div class="footer-navi">© 2012. All Rights Reserved. Powered by <a href="http://wordpress.org/">WordPress</a>  </div>
	  <!-- footer-navigation /end -->
      <img src="images/logo-footer-retina.png" width="65" alt="">	  </div>
	  </section>
    <!-- end-footbot -->
  </footer>
<!-- end-footer -->
<span id="scroll-top"><a class="scrollup"><i class="icomoon-arrow-up"></i></a></span>
</div><!-- end-wrap -->

<!-- End Document
================================================== -->
<
<script type="text/javascript" src="js/mexin-custom.js" ></script>
<script type="text/javascript" src="js/doubletaptogo.js" ></script>
<script src="js/bootstrap-alert.js"></script>
<script src="js/bootstrap-dropdown.js"></script>
<script src="js/bootstrap-tab.js"></script>
<script src="js/bootstrap-tooltip.js"></script>

<!--
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="http://getbootstrap.com/dist/js/bootstrap.min.js"></script>
<script src="http://getbootstrap.com/assets/js/docs.min.js"></script>
-->
</body>
</html>