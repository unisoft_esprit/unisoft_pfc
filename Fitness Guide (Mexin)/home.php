<!DOCTYPE html>
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>

	<!-- Basic Page Needs
  ================================================== -->
	<meta charset="utf-8">
	<title>Fitness & Workout </title>
	<meta name="description" content="">
	<meta name="author" content="">

	<!-- Mobile Specific Metas
  ================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- CSS
  ================================================== -->
	<link rel="stylesheet" href="css/style.css" type="text/css"  media="all">
	<link rel="stylesheet" href="css/style-selector.css" type="text/css">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,900|Roboto|Roboto+Slab:300,400' rel='stylesheet' type='text/css'>

	<!-- JS
  ================================================== -->
   <script type="text/javascript" src="js/jquery.min.js" ></script>
	<!--[if lt IE 9]>
	<script src="js/modernizr.custom.11889.js" type="text/javascript"></script>
	<![endif]-->
		<!-- HTML5 Shiv events (end)-->
    <script type="text/javascript" src="js/nav-resp.js"></script>
	<script type="text/javascript" src="js/colorize.js"></script>
		<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','../../www.google-analytics.com/analytics.js','ga');
	
	  ga('create', 'UA-43921752-1', 'webnus.net');
	  ga('send', 'pageview');
	
	</script>
	<!-- Favicons
  ================================================== -->
	<link rel="shortcut icon" href="images/favicon.ico">

    </head>
<body>

	<!-- Primary Page Layout
	================================================== -->

<div id="boxed-wrap" class="colorskin-0">
<div class="top-bar">
<div class="container">
<div class="top-links"> <a href="#">Form</a> | <a href="#">Terms</a> | <a href="#">Contact</a></div>
<div class="socailfollow"><a href="#" class="facebook"><i class="icomoon-facebook"></i></a> <a href="#" class="dribble"><i class="icomoon-dribbble"></i></a>  <a href="#" class="vimeo"><i class="icomoon-vimeo"></i></a><a href="#" class="google"><i class="icomoon-google"></i></a> <a href="#" class="twitter"><i class="icomoon-twitter"></i></a></div>
</div>
</div>
<header id="header">
<div  class="container">
<div class="four columns logo"><a href="index-2.html"><img src="images/logo-retina1.png" width="120" id="img-logo" alt="logo"></a></div>
<nav id="nav-wrap" class="nav-wrap1 twelve columns">
		<div id="search-form">
					<form action="#" method="get">
						<input type="text" class="search-text-box" id="search-box">
					</form>
				</div>
					<ul id="nav">
				<li class="current"><a href="index-2.html">Home</a>
						<ul>
							<li><a href="index-2.html">Home 1 - Main Page</a></li>
							<li class="current"><a href="home2.html">Home 2 - Full Sections</a></li>
							<li><a href="home3.html">Home 3 - Parallax Sections</a></li>
							<li><a href="home4.html">Home 4 - Dark Half</a></li>
							<li class="submenu"><a href="#">Headers</a>
								<ul>
									<li><a href="header-fixed.html">Header Fixed</a></li>
									<li><a href="header2.html">Header 2</a></li>
									<li><a href="header3.html">Header 3</a></li>
									<li><a href="header4.html">Header 4</a></li>
									<li><a href="header5.html">Header 5</a></li>
								</ul>
							</li>
					</ul>
				</li>
				<li><a  href="#">Pages</a>
					<ul>
						<li><a href="services.html">Services</a></li>
						<li><a href="about.html">About us</a></li>
						<li><a href="about-me.html">About me</a></li>
						<li><a href="ourteam.html">Our team</a></li>
						<li><a href="sidebar-right.html">Right Side-Bar</a></li>
						<li><a href="sidebar-left.html">Left Side-Bar</a></li>
						<li><a href="faq.html">FAQ</a></li>
						<li><a href="contact.html">Contact</a></li>
						<li><a href="404.html">404 Not Found</a></li>
					</ul>
				</li>
				<li><a href="#">Features</a>
					<ul>
						<li><a href="elements.html">Elements</a></li>
						<li><a href="icons.html">Icons</a></li>
						<li><a href="pricing.html">Pricing Tables</a></li>
						<li><a href="columns.html">Columns</a></li>
						<li><a href="timeline1.html">Timeline</a></li>
						<li><a href="left-nav-page.html">Left Navigation</a></li>
						<li class="submenu"><a href="#">Sub Menu</a>
							<ul>
							<li><a href="#">Menu Item 01</a></li>
							<li><a href="#">Menu Item 02</a></li>
							<li><a href="#">Menu Item 03</a></li>
							</ul>
					  </li>
					</ul>
				</li>
				<li><a  href="blog.html">Blog</a>
					<ul>
						<li><a href="blog.html">Blog 1</a></li>
						<li><a href="blog-leftsidebar.html">Blog 1 - Left Sidebar</a></li>
						<li><a href="blog2.html">Blog 2</a></li>
						<li><a href="blog2-leftsidebar.html">Blog 2 - Left Sidebar</a></li>
						<li><a href="blog-bothsidebar.html">Blog - Both Sidebar</a></li>
						<li><a href="timeline1.html">Blog - Timeline</a></li>
						<li><a href="blog-single.html">Blog - Single Post</a></li>
					</ul>
					</li>
				<li><a href="portfolio2col.html">work</a>
				    <ul>
						<li><a href="portfolio2col.html">Portfolio 2 Columns</a></li>
						<li><a href="portfolio3col.html">Portfolio 3 Columns</a></li>
						<li><a href="portfolio4col.html">Portfolio 4 Columns</a></li>
						<li><a href="portfolio-pin.html">Portfolio Pinterest</a></li>
						<li><a href="timeline2.html">Portfolio Timeline</a></li>
						<li><a href="portfolio-item.html">Portfolio Item (Single)</a></li>
					</ul>
				</li>
			</ul>
	</nav>
		<!-- /nav-wrap -->
</div>
		<div id="search-form2">
					<form action="#" method="get">
						<input type="text" class="search-text-box2">
					</form>
				</div>
</header>
<!-- end-header -->

<section id="hero" class="tbg1">
    <div id="layerslider-container-fw">
      <div id="layerslider" style="width: 100%; height: 436px; margin: 0px auto; ">
	  
        <div class="ls-layer" style="slidedirection: top; slidedelay: 6000; durationin: 1500; durationout: 1500; delayout: 500;">
		
		 <img src="images/slide-pics/slide1-bg.jpg" class="ls-bg" alt="">
		 
		 <img src="http://adonisgoldenratioreviews1.com/wp-content/uploads/2013/08/FitnessMan.png" class="ls-s6" alt="" style="top: 134px; left: 30px;  slidedirection : bottom; slideoutdirection : bottom; durationin : 1500; durationout : 750; easingin : easeInOutQuint; easingout : easeInOutQuint; delayin : 600;">
		 
		  <img src="http://jatomi.com/img/kobieta.png" class="ls-s6" alt="" style="top: 36px; left: 190px;  slidedirection : bottom; slideoutdirection : bottom; durationin : 1500; durationout : 750; easingin : easeInOutQuint; easingout : easeInOutQuint; delayin : 1000;">
		 
          <h1 class="ls-s3" style="position: absolute; top:100px; left: 700px; slidedirection : top; slideoutdirection : top; durationin : 3000; durationout : 750; easingin : easeInOutQuint; easingout : easeInBack; delayin : 1000;">Sports<br>
            & Fitness</h1>
			
          <h4 class="ls-s3 l1-s1" style="position: absolute; top:230px; left: 700px; slidedirection : bottom; slideoutdirection : bottom; durationin : 3000; durationout : 750; easingin : easeInOutQuint; easingout : easeInBack; delayin : 1000;">are the best way to keep you healthy </h4>

		  
          
		  
		   </div>
		   
        <div class="ls-layer" style="slidedirection: right; slidedelay: 5000; durationin: 1500; durationout: 1500;">
		
		 <img src="http://www.serenevilla.com/images2010/fitness-gym.jpg" class="ls-bg" alt=""> 
		 
          <h1 class="ls-s3" style="position: absolute; font-family:Arial; top:114px; left: 30px; slidedirection : right; slideoutdirection : left; durationin : 3000; durationout : 750; easingin : easeInOutQuint; easingout : easeInBack; delayin : 400;">Find Your workout area </h1>
			
          <h4 class="ls-s3 l1-s1" style="position: absolute; top:189px; left: 30px; border-color:#fff; slidedirection : bottom; slideoutdirection : left; durationin : 3000; durationout : 750; easingin : easeInOutQuint; easingout : easeInBack; delayin : 500;">Near your location in simple clicks</h4>
		  
		  
          
		  
		   </div>
		   
		   
		    <div class="ls-layer" style="slidedirection: right; slideoutdirection : top; slidedelay: 5000; durationin: 1500; durationout: 1500; delayout: 500;"> 
		
		 <img src="images/slide-pics/slide3-bg.jpg" class="ls-bg" alt="">
		 
		 <img src="http://www.wersellsbikeandskishop.com/wp-content/uploads/2012/02/FitnessCouple.png" class="ls-s6" alt="" style="top: 28px; left: 20px;  slidedirection : fade; slideoutdirection : fade; durationin : 1500; durationout : 750; easingin : easeInOutQuint; easingout : easeInOutQuint; delayin : 500;"> 
		 
		           <h1 class="ls-s3" style="position: absolute; top:120px; left: 700px; color:#fff; slidedirection : top; slideoutdirection : top; durationin : 3000; durationout : 750; easingin : easeInOutQuint; easingout : easeInBack; delayin : 600;">Get Coached </h1>
			
          <h4 class="ls-s3 l1-s1" style="position: absolute; top:196px; left: 700px; color:#fff; border-color:#fff; slidedirection : bottom; slideoutdirection : bottom; durationin : 3000; durationout : 750; easingin : easeInOutQuint; easingout : easeInBack; delayin : 700;">With more than 90 Fitness Program made by the best Coach </h4>
		 
		  
		 
		   </div>
		   
		   <div class="ls-layer" style="slidedirection: top; slideoutdirection : top; slidedelay: 5000; durationin: 1500; durationout: 1500; delayout: 500;"> 
		
		 <img src="images/slide-pics/slide4-bg.jpg" class="ls-bg" alt="">
		 
		 <img src="http://www.bodynewfitness.it/img/bodyNewFitness.png" class="ls-s6" alt="" style="top: 28px; left: 640px;  slidedirection : bottom; slideoutdirection : bottom; durationin : 1500; durationout : 750; easingin : easeInOutQuint; easingout : easeInOutQuint; delayin : 300;"> 
		 
		           <h1 class="ls-s3" style="position: absolute; top:120px; left: 30px;  slidedirection : top; slideoutdirection : top; durationin : 3000; durationout : 750; easingin : easeInOutQuint; easingout : easeInBack; delayin : 500;">Fitness & Workout </h1>
			
          <h4 class="ls-s3 l1-s1" style="position: absolute; top:198px; left: 30px;  slidedirection : bottom; slideoutdirection : bottom; durationin : 3000; durationout : 750; easingin : easeInOutQuint; easingout : easeInBack; delayin : 600;">Bring you the best lifestyle to stay healthy and well coached </h4>
		 
		 
		 
		   </div>
        
		   
      </div>
    </div>
  </section>
<section class="blox dark nopad section-bg2">
    <div class="container aligncenter">
      <hr class="vertical-space2">
      
      <h1 class="mex-title">find your way to get healthy</h1>
      <hr class="vertical-space1">
      <div class="sixteen columns">
        <div class="one_third">
          <div class="icon-box6"> <i class="icomoon-clock-6"></i>
            <h4>Activities</h4>
            <p>Take a tour and get all the informations you need about our clubs activities </p>
            <a href="activities_list.php" class="magicmore">Learn more</a> </div>
        </div>
        <div class="one_third">
          <div class="icon-box6"> <i class="icomoon-phone-2"></i>
            <h4>Clubs</h4>
            <p>Browse all the clubs and get more informations about their programs and activities </p>
            <a href="clubs_list.php" class="magicmore">Learn more</a> </div>
        </div>
        <div class="one_third column-last">
          <div class="icon-box6"> <i class="icomoon-envelop-opened"></i>
            <h4>Programs</h4>
            <p>Find all informations you need to get fit and have a healthy life style</p>
            <a href="programs_list.php" class="magicmore">Learn more</a> </div>
        </div>
      </div>
    </div>
  </section>
<!-- end-hero-->

<section class="blox dark">
    <div class="container">
      <div class="sixteen columns">
        <div class="one_half">
          <div class="vertical-space4"></div>
          <h2>Retina Ready & Ultra Responsive</h2>
          <h5><strong>Nesciunt tofu stumptown aliqua, retro synth master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica. Reprehenderit butcher retro keffiyeh dreamcatcher synth. Cosby sweater eu banh mi, qui irure terry richardson ex squid. </strong></h5>
          <br>
          <a class="button green large">Purchase Template</a> </div>
        <div class="one_half column-last"><img src="images/retina-ready-pic1.png" alt=""></div>
      </div>
    </div>
  </section>
<section class="container">
    <div class="vertical-space4"></div>
    <div class="sixteen columns aligncenter">
      <h1>Mexin Beautiful, simple and easy to use </h1>
      <h4>Mexin is an all-in-one solution for business websites that is super sleek finding harmony between simplicity and striking design elements. It is fully Responsive and Retina Ready.</h4>
      <br>
      <a class="button red large">Purchase Template</a> </div>
    <hr class="vertical-space1">
    <!-- Latest-from-Blog-start -->
    <div class="latest-f-blog">
      <h4 class="subtitle">Latest from Blog</h4>
      <article class="one-third column alpha blog-post"> <a href="#" ><img src="images/blog-post-pic1.jpg" alt=""></a>
        <h5><a href="#" >Job Seeking Out of Your Industry</a></h5>
        <p>10 MAY 2013 </p>
      </article>
      <article class="one-third column blog-post"> <a href="#" ><img src="images/blog-post-pic3.jpg" alt=""></a>
        <h5><a href="#" >Magnifying Uses of Small Spaces</a></h5>
        <p>10 MAY 2013 </p>
      </article>
      <article class="one-third column omega blog-post"> <a href="#" ><img src="images/blog-post-pic2.jpg" alt=""></a>
        <h5><a href="#" >What Your College Degree Will Be Worth </a></h5>
        <p>10 MAY 2013 </p>
      </article>
      <hr class="vertical-space1">
    </div>
    <!-- Latest-from-Blog-end -->
    <div class="qot-week">
      <div class="qot-pic"></div>
      <h6 class="qot-title">Quote of the week</h6>
      <blockquote>
        <h3>Care and accuracy in the design process show respect towards the consumer.</h3>
        <cite title="">John Smith <small> - Manager at Mexin</small></cite></blockquote>
    </div>
    <hr class="vertical-space2">
  </section>
<!-- end- -->

<footer id="footer">
    <section class="container footer-in">
      <div class="one-third column contact-inf">
        <h4 class="subtitle">Contact Information</h4>
        <br />
        <p><strong>Address: </strong> No.28 - 63739 street lorem ipsum City, Country</p>
        <p><strong>Phone: </strong> + 1 (234) 567 8901 </p>
        <p><strong>Fax: </strong> + 1 (234) 567 8901 </p>
        <p><strong>Email: </strong> support@yoursite.com </p>
        <h4 class="subtitle">Stay Connected</h4>
        <div class="socailfollow"><a href="#" class="facebook"><i class="icomoon-facebook"></i></a> <a href="#" class="dribble"><i class="icomoon-dribbble"></i></a> <a href="#" class="pinterest"><i class="icomoon-pinterest-2" aria-hidden="true"></i></a> <a href="#" class="vimeo"><i class="icomoon-vimeo"></i></a><a href="#" class="google"><i class="icomoon-google"></i></a> <a href="#" class="twitter"><i class="icomoon-twitter"></i></a> <a href="#" class="youtube"><i class="icomoon-youtube"></i></a> </div>
      </div>
      <!-- end-contact-info /end -->
      <div class="one-third column">
        <h4 class="subtitle">latest tweet</h4>
        <br />
        <div class="lts-tweets"> <i class="icomoon-twitter"></i>
          <h3><a href="https://twitter.com/webnus">@webnus</a></h3>
          <h5 id="twitter"></h5>
        </div>
      </div>
      <!-- tweets  /end -->
      <div class="one-third column">
        <h4 class="subtitle">flickr photostream</h4>
        <br />
        <div class="flickr-feed">
          <script type="text/javascript" src="http://www.flickr.com/badge_code.gne?count=12&amp;display=random&amp;size=square&amp;nsid=36587311@N08&amp;raw=1"></script>
          <div class="clear"></div>
        </div>
      </div>
      <!-- flickr /end -->
    </section>
    <!-- end-footer-in -->
    <section class="footbot">
      <div class="container">
        <div class="footer-navi">© 2012. All Rights Reserved. Powered by <a href="http://wordpress.org/">WordPress</a> </div>
        <!-- footer-navigation /end -->
        <img src="images/logo-footer-retina.png" width="65" alt=""> </div>
    </section>
    <!-- end-footbot -->
  </footer>
<!-- end-footer -->
<span id="scroll-top"><a class="scrollup"><i class="icomoon-arrow-up"></i></a></span> 
 </div><!-- end-wrap -->

<!-- end-style-selector -->



<!-- End Document
================================================== -->
<script type="text/javascript" src="js/jcarousel.js" ></script>
<script type="text/javascript">
jQuery(document).ready(function() {
jQuery('#latest-projects').jcarousel(), jQuery('#our-clients').jcarousel();
});
</script>  
<script type="text/javascript" src="js/mexin-custom.js" ></script>
<script type="text/javascript" src="js/doubletaptogo.js" ></script>
<script src="layerslider/jQuery/jquery-easing-1.3.js" type="text/javascript"></script>
<script src="layerslider/js/layerslider.kreaturamedia.jquery.js" type="text/javascript"></script>
<script type="text/javascript" src="js/layerslider-init.js"></script>
<script src="js/bootstrap-alert.js"></script>
<script src="js/bootstrap-dropdown.js"></script>
<script src="js/bootstrap-tab.js"></script>
</body>
</html>