<?php 
    include 'inc/connection.php';
    include 'inc/functions.php';
?>


<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <?php include 'inc/head.php'; ?>
    </head>

    <!-- Body -->
    <!-- In the PHP version you can set the following options from the config file -->
    <!-- Add the class .hide-side-content to <body> to hide side content by default -->
    <body>
        <!-- Page Container -->
        <!-- In the PHP version you can set the following options from the config file -->
        <!-- Add the class .full-width for a full width page -->
        <div id="page-container" class="full-width">
            <!-- Header -->
            <?php include 'inc/top.php'; ?>
            <!-- END Header -->
            <!-- Left Sidebar -->
            <?php require 'inc/side.php'; ?>
            <!-- END Left Sidebar -->
            <!-- Pre Page Content -->
            <div id="pre-page-content">
                <h1><i class="glyphicon-shopping_bag themed-color"></i>Add Club<br><small>Create New Club</small></h1>
            </div>
            <!-- END Pre Page Content -->

            <!-- Page Content -->
            <div id="page-content">
                <!-- Breadcrumb -->
                <!-- You can have the breadcrumb stick on scrolling just by adding the following attributes with their values (data-spy="affix" data-offset-top="250") -->
                <!-- You can try it on other elements too :-), the sticky position and style can be adjusted in the css/main.css with .affix class -->
                <ul class="breadcrumb" data-spy="affix" data-offset-top="250">
                    <li>
                        <a href="index.html"><i class="glyphicon-display"></i></a> <span class="divider"><i class="icon-angle-right"></i></span>
                    </li>
                    <li>
                        <a href="#">Ready UI</a> <span class="divider"><i class="icon-angle-right"></i></span>
                    </li>
                    <li class="active"><a href="">Add Club</a></li>
                </ul>
                <!-- END Breadcrumb -->

                <!-- Products List Block -->
                <div class="block block-themed block-last">
                    <!-- Products List Title -->
                    <div class="block-title">
                        
                        <h4><i class="icon-asterisk"></i>Create Club</h4>
                    </div>
                    <!-- END Products List Title -->

                    <!-- Products List Content -->
                    <div class="block-content">
                        <div class="row-fluid row-items">
                            
                            <form action="create_club.php" method="post" class="form-inline" >
                                <!-- div.row-fluid -->
                                <h4 class="sub-header">Essential information</h4>
                                <div class="row-fluid">
                                    <!-- 1st Column -->
                                    <div class="span6">
                                    
                                        <div class="control-group">
                                            <label class="control-label" for="columns-text">Club Name</label>
                                            <div class="controls">
                                                <input type="text" id="columns-text" name="name_club">
                                                
                                            </div>
                                        </div>
                                        
                                        <div class="control-group">
                                            <label class="control-label" for="columns-textarea">Description</label>
                                            <div class="controls">
                                                <textarea id="textarea-medium" name="description_club" class="textarea-medium" rows="6">...</textarea>
                                            </div>
                                        </div>

                                        <div class="control-group">
                                            <label class="control-label" for="columns-text">Club Phone</label>
                                            <div class="controls">
                                                <input type="text" id="columns-text" name="phone_club">
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END 1st Column -->

                                    <!-- 2nd Column -->
                                    <div class="span6">
                                       
                                       
                                         <div class="control-group">
                                            <label class="control-label" for="columns-text">Club Email</label>
                                            <div class="controls">
                                                <input type="text" id="columns-text" name="email_club">
                                                
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label" for="columns-textarea">Location Text</label>
                                            <div class="controls">
                                                <textarea id="textarea-medium" name="location_club" class="textarea-medium" rows="6">...</textarea>
                                                <span class="help-block">Write the adresse of the club</span>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END 2nd Column -->
                                </div>
                                <!-- END div.row-fluid -->
                                <div class="row-fluid">
                                    <!-- Media / Club Form -->
                                    <h4 class="sub-header">Media</h4>
                                    <div class=".dropzone">
                        <div class="gallery" data-toggle="lightbox-gallery">
                            <div class="row-fluid row-items">
                            <h3>Featured Image</h3>
                            <label class="control-label" for="columns-text">Link</label>
                                <div class="controls">
                                    <input type="text" id="columns-text" name="featured_img">
                                    
                                </div>
                            <!-- TODO : Dynamic Image input
                                <div class="span4 gallery-image">
                                    <img src="img/placeholders/image_720x450_light.png" alt="image">
                                    <div class="gallery-image-options">
                                        <a href="img/placeholders/image_720x450_light.png" class="gallery-link badge badge-inverse"><i class="icon-search"></i></a>
                                        <a href="javascript:void(0)" class="badge badge-success" data-toggle="modal" data-target="#myModal"><i class="icon-save"></i></a>
                                        <a href="javascript:void(0)" class="badge badge-info"><i class="icon-cloud-download"></i></a>
                                    </div>
                                </div>
                                <!-- Modal -->
                             <!--  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                  <div class="modal-dialog">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                                      </div>
                                      <div class="modal-body">
                                        ...
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary">Save changes</button>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <!-- END Modal -->

                                
                                
                                
                            </div>
                            <div class="row-fluid row-items">

                                <div class="span2 gallery-image">
                                    <img src="img/placeholders/image_720x450_light.png" alt="image">
                                    <div class="gallery-image-options">
                                        <a href="img/placeholders/image_720x450_light.png" class="gallery-link badge badge-inverse"><i class="icon-search"></i></a>
                                        <a href="javascript:void(0)" class="badge badge-success"><i class="icon-save"></i></a>
                                    </div>
                                </div>
                                <div class="span2 gallery-image">
                                    <img src="img/placeholders/image_720x450_light.png" alt="image">
                                    <div class="gallery-image-options">
                                        <a href="img/placeholders/image_720x450_light.png" class="gallery-link badge badge-inverse"><i class="icon-search"></i></a>
                                        <a href="javascript:void(0)" class="badge badge-neutral"><i class="icon-star"></i></a>
                                    </div>
                                </div>
                                <div class="span2 gallery-image">
                                    <img src="img/placeholders/image_720x450_light.png" alt="image">
                                    <div class="gallery-image-options">
                                        <a href="img/placeholders/image_720x450_light.png" class="gallery-link badge badge-inverse"><i class="icon-search"></i></a>
                                        <a href="javascript:void(0)" class="badge badge-info"><i class="icon-cloud-upload"></i></a>
                                    </div>
                                </div>
                                <div class="span2 gallery-image">
                                    <img src="img/placeholders/image_720x450_light.png" alt="image">
                                    <div class="gallery-image-options">
                                        <a href="img/placeholders/image_720x450_light.png" class="gallery-link badge badge-inverse"><i class="icon-search"></i></a>
                                        <a href="javascript:void(0)" class="badge badge-info"><i class="icon-cloud-download"></i></a>
                                    </div>
                                </div>
                                <div class="span2 gallery-image">
                                    <img src="img/placeholders/image_720x450_light.png" alt="image">
                                    <div class="gallery-image-options">
                                        <a href="img/placeholders/image_720x450_light.png" class="gallery-link badge badge-inverse"><i class="icon-search"></i></a>
                                        <a href="javascript:void(0)" class="badge badge-neutral"><i class="icon-star"></i></a>
                                    </div>
                                </div>
                                <div class="span2 gallery-image">
                                    <img src="img/placeholders/image_720x450_light.png" alt="image">
                                    <div class="gallery-image-options">
                                        <a href="img/placeholders/image_720x449_light.png" class="gallery-link badge badge-inverse"><i class="icon-search"></i></a>
                                        <a href="javascript:void(0)" class="badge badge-info"><i class="icon-cloud-upload"></i></a>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row-fluid row-items">
                                <h3>Video</h3>
                                <div class="span6 gallery-image">
                                    <img src="img/placeholders/image_1680x1050_dark.png" alt="image">
                                    <div class="gallery-image-options ">
                                        <a href="img/placeholders/image_1680x1050_dark.png" class="gallery-link badge badge-inverse"><i class="icon-search"></i></a>
                                        <a href="javascript:void(0)" class="badge badge-success"><i class="icon-save"></i></a>
                                        <a href="javascript:void(0)" class="badge badge-info"><i class="icon-cloud-download"></i></a>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                                    
                                    <!-- END 1st Column -->

                                
                                </div>
                                <!-- END div.row-fluid -->
                                <div class="form-actions">
                                    <button type="reset" class="btn btn-danger"><i class="icon-repeat"></i> Reset</button>
                                    <button type="submit" class="btn btn-success" value="Add Club"><i class="icon-ok"></i> Submit</button>
                                </div>
                            </form>
                                
                        </div>
                    </div>
                    <!-- END Products List Content -->
                </div>
                <!-- END Products List Block -->
            </div>
            <!-- END Page Content -->

            <!-- Footer -->
            <?php include 'inc/footer.php'; ?>
            <!-- END Footer -->
        </div>
        <!-- END Page Container -->

        <!-- Scroll to top link, check main.js - scrollToTop() -->
        <a href="#" id="to-top"><i class="icon-chevron-up"></i></a>

        <!-- User Modal Account, appears when clicking on 'User Settings' link found on user dropdown menu (header, top right) -->
        <div id="modal-user-account" class="modal hide fade">
            <!-- Modal Body -->
            <div class="modal-body remove-padding">
                <!-- Modal Tabs -->
                <div class="block-tabs">
                    <div class="block-options">
                        <a href="javascript:void(0)" class="btn btn-danger" data-dismiss="modal"><i class="icon-remove"></i></a>
                    </div>
                    <ul class="nav nav-tabs" data-toggle="tabs">
                        <li class="active"><a href="#modal-user-account-account"><i class="icon-cog"></i> Account</a></li>
                        <li><a href="#modal-user-account-profile"><i class="icon-user"></i> Profile</a></li>
                    </ul>
                    <div class="tab-content">
                        <!-- Account Tab Content -->
                        <div class="tab-pane active" id="modal-user-account-account">
                            <form action="index.html" method="post" class="form-horizontal" onsubmit="return false;">
                                <div class="control-group">
                                    <label class="control-label" for="modal-account-username">Username</label>
                                    <div class="controls">
                                        <input type="text" id="modal-account-username" name="modal-account-username" value="admin" class="disabled" disabled>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="modal-account-email">Email</label>
                                    <div class="controls">
                                        <input type="text" id="modal-account-email" name="modal-account-email" value="admin@exampleapp.com">
                                    </div>
                                </div>
                                <h4 class="sub-header">Change Password</h4>
                                <div class="control-group">
                                    <label class="control-label" for="modal-account-pass">Current Password</label>
                                    <div class="controls">
                                        <input type="password" id="modal-account-pass" name="modal-account-pass">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="modal-account-newpass">New Password</label>
                                    <div class="controls">
                                        <input type="password" id="modal-account-newpass" name="modal-account-newpass">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="modal-account-newrepass">Retype New Password</label>
                                    <div class="controls">
                                        <input type="password" id="modal-account-newrepass" name="modal-account-newrepass">
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- END Account Tab Content -->

                        <!-- Profile Tab Content -->
                        <div class="tab-pane" id="modal-user-account-profile">
                            <form action="index.html" method="post" class="form-horizontal" onsubmit="return false;">
                                <div class="control-group">
                                    <label class="control-label" for="modal-profile-name">Name</label>
                                    <div class="controls">
                                        <input type="text" id="modal-profile-name" name="modal-profile-name" value="John Doe">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="modal-profile-gender">Gender</label>
                                    <div class="controls">
                                        <select id="modal-profile-gender" name="modal-profile-name">
                                            <option value="m">Male</option>
                                            <option value="f">Female</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="modal-profile-birthdate">Birthdate</label>
                                    <div class="controls">
                                        <div class="input-append">
                                            <input type="text" id="modal-profile-birthdate" name="modal-profile-birthdate" class="input-small input-datepicker">
                                            <span class="add-on"><i class="icon-calendar"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="modal-profile-skills">Skills</label>
                                    <div class="controls">
                                        <select id="modal-profile-skills" name="modal-profile-skills" class="select-chosen" multiple>
                                            <option value="html" selected>html</option>
                                            <option value="css" selected>css</option>
                                            <option value="javascript">javascript</option>
                                            <option value="php">php</option>
                                            <option value="mysql">mysql</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="modal-profile-bio">Bio</label>
                                    <div class="controls">
                                        <textarea id="modal-profile-bio" name="modal-profile-bio" class="textarea-elastic" rows="3">Bio Information..</textarea>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- END Profile Tab Content -->
                    </div>
                </div>
                <!-- END Modal Tabs -->
            </div>
            <!-- END Modal Body -->

            <!-- Modal footer -->
            <div class="modal-footer">
                <button class="btn btn-success" data-dismiss="modal"><i class="icon-save"></i> Save</button>
            </div>
            <!-- END Modal footer -->
        </div>
        <!-- END User Modal Settings -->

        <!-- Excanvas for Flot (Charts plugin) support on IE8 -->
        <!--[if lte IE 8]><script src="js/helpers/excanvas.min.js"></script><![endif]-->

        <!-- Get Jquery library from Google ... -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <!-- ... but if something goes wrong get Jquery from local file -->
        <script>!window.jQuery && document.write(unescape('%3Cscript src="js/vendor/jquery-1.9.1.min.js"%3E%3C/script%3E'));</script>

        <!-- Bootstrap.js -->
        <script src="js/vendor/bootstrap.min.js"></script>

        <!--
        Include Google Maps API for global use.
        If you don't want to use  Google Maps API globally, just remove this line and the gmaps.js plugin from js/plugins.js (you can put it in a seperate file)
        Then iclude them both in the pages you would like to use the google maps functionality
        -->
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>

        <!-- Jquery plugins and custom javascript code -->
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
    </body>
</html>