<?php 
    include 'inc/connection.php';
    include 'inc/functions.php';
?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
       <?php include 'inc/head.php'; ?>
    </head>

    <!-- Body -->
    <!-- In the PHP version you can set the following options from the config file -->
    <!-- Add the class .hide-side-content to <body> to hide side content by default -->
    <body>

        <?php 
        
        $sel_club= get_club_by_id($_GET['clb_id']);

        ?>
        <!-- Page Container -->
        <!-- In the PHP version you can set the following options from the config file -->
        <!-- Add the class .full-width for a full width page -->
        <div id="page-container" class="full-width">
            <!-- Header -->
            <!-- In the PHP version you can set the following options from the config file -->
            <!-- Add the class .navbar-fixed-top or .navbar-fixed-bottom for a fixed header on top or bottom respectively -->
            <!-- If you add the class .navbar-fixed-top remember to add the class .header-fixed-top to <body> element! -->
            <!-- If you add the class .navbar-fixed-bottom remember to add the class .header-fixed-bottom to <body> element! -->
            <!-- <header class="navbar navbar-inverse navbar-fixed-top"> -->
            <!-- <header class="navbar navbar-inverse navbar-fixed-bottom"> -->
            <?php include 'inc/top.php'; ?>
            <!-- END Header -->
            <!-- Left Sidebar -->
            <!-- In the PHP version you can set the following options from the config file -->
            <!-- Add the class .sticky for a sticky sidebar -->
            <?php include 'inc/side.php'; ?>
            <!-- END Left Sidebar -->
            <!-- Pre Page Content -->
            <div id="pre-page-content">
                <h1><i class="glyphicon-camera_small themed-color"></i><?php echo $sel_club['name_club']; ?><br><small><!--Club Detail--></small></h1>
            </div>
            <!-- END Pre Page Content -->

            <!-- Page Content -->
            <div id="page-content">
                <!-- Breadcrumb -->
                <!-- You can have the breadcrumb stick on scrolling just by adding the following attributes with their values (data-spy="affix" data-offset-top="250") -->
                <!-- You can try it on other elements too :-), the sticky position and style can be adjusted in the css/main.css with .affix class -->
                <ul class="breadcrumb" data-spy="affix" data-offset-top="250">
                    <li>
                        <a href="index.html"><i class="glyphicon-display"></i></a> <span class="divider"><i class="icon-angle-right"></i></span>
                    </li>
                    <li>
                        <a href="admin_clubs_list.php">Club List</a> <span class="divider"><i class="icon-angle-right"></i></span>
                    </li>
                    <li class="active"><a href=""><?php echo $sel_club['name_club']; ?></a></li>
                </ul>
                <!-- END Breadcrumb -->

                <!-- Product Info -->
                <div class="row-fluid row-items">
                    <!-- Images -->
                    <div class="span4 gallery" data-toggle="lightbox-gallery">
                        <img src="<?php echo get_featured_img($_GET['clb_id']) ; ?>" alt="Product Image" class="push">
                        <div class="row-fluid">
                            <div class="span3">
                                <a href="img/placeholders/image_720x450_dark.png" class="gallery-link">
                                    <img src="img/placeholders/image_720x450_dark.png" alt="image">
                                </a>
                            </div>
                            <div class="span3">
                                <a href="img/placeholders/image_720x450_light.png" class="gallery-link">
                                    <img src="img/placeholders/image_720x450_light.png" alt="image">
                                </a>
                            </div>
                            <div class="span3">
                                <a href="img/placeholders/image_720x450_dark.png" class="gallery-link">
                                    <img src="img/placeholders/image_720x450_dark.png" alt="image">
                                </a>
                            </div>
                            <div class="span3">
                                <a href="img/placeholders/image_720x450_light.png" class="gallery-link">
                                    <img src="img/placeholders/image_720x450_light.png" alt="image">
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- END Images -->

                    <!-- club Description  -->
                    <div class="span5">
                        <h4 class="sub-header">club Description </h4>
                        <p><?php echo $sel_club['description_club']; ?></p>
                    </div>
                    <!-- END club Description  -->

                    <!-- Extra -->
                    <div class="span3 text-center">
                        <h4 class="sub-header">$ 1260,00</h4>
                        <h4>
                            <i class="icon-star themed-color-sun"></i>
                            <i class="icon-star themed-color-sun"></i>
                            <i class="icon-star themed-color-sun"></i>
                            <i class="icon-star themed-color-sun"></i>
                            <i class="icon-star-empty themed-color-sun"></i><br>
                            <small><em>Based on 4 Reviews</em></small>
                        </h4>
                        <a href="javascript:void(0)" class="btn btn-large btn-success btn-block"><i class="icon-check"></i> Buy Now</a>
                        <a href="javascript:void(0)" class="btn btn-large btn-success btn-block"><i class="icon-gift"></i> Buy as a Gift</a>
                        <a href="javascript:void(0)" class="btn btn-large btn-warning btn-block"><i class="icon-shopping-cart"></i> Add to Cart</a>
                        <a href="javascript:void(0)" class="btn btn-large btn-inverse btn-block"><i class="icon-bookmark-empty"></i> Add to Wishlist</a>
                    </div>
                    <!-- END Extra -->
                </div>
                <!-- END Product Info -->

                <!-- Description, Features and Reviews -->
                <div class="block-tabs block-themed">
                    <!-- Tab Links -->
                    <ul class="nav nav-tabs" data-toggle="tabs">
                        <li class="active"><a href="#club-tabs-desc"><i class="icon-info-sign"></i> Description</a></li>
                        <li><a href="#club-tabs-location">Location</a></li>
                        <li><a href="#club-tabs-phone">Phone</a></li>
                        <li><a href="#club-tabs-email">Email</a></li>
                        <li><a href="#club-tabs-ratings"><i class="icon-magic"></i> Ratings</a></li>
                        <li><a href="#club-tabs-features"><i class="icon-magic"></i> Features</a></li>
                        <li><a href="#club-tabs-reviews"><i class="icon-pencil"></i> Reviews</a></li>
                    </ul>
                    <!-- END Tab Links -->

                    <!-- Tabs Content -->
                    <div class="tab-content">
                        <!-- Description -->
                        <div class="tab-pane active" id="club-tabs-desc">
                            <p class="content-text">
                               <?php echo $sel_club['description_club']; ?>
                            </p>
                            <h5><em>Location</em></h5>
                            <ul class="icons-ul list push">
                                <?php echo $sel_club['location_club'];?>
                            </ul>
                            <h5><em>Phone</em></h5>
                            <ul class="icons-ul list push">
                                <?php echo $sel_club['phone_club'];?>
                            </ul>
                            <h5><em>Mail</em></h5>
                            <ul class="icons-ul list push">
                                <?php echo $sel_club['email_club'];?>
                            </ul>
                        </div>
                        <!-- END Description -->

                         <!-- Locaction -->
                        <div class="tab-pane" id="club-tabs-location">
                            <h4 class="sub-header">Location</h4>
                              <table class="table table-hover table-striped">
                                <tbody>
                                    <?php echo $sel_club['location_club'];?>
                                 </tbody>
                            </table>

                        </div>
                        <!-- END Location -->

                        <!-- Phone -->
                        <div class="tab-pane" id="club-tabs-phone">
                            <h4 class="sub-header">Phone</h4>
                            <table class="table table-hover table-striped">
                                <tbody>
                                    <?php echo $sel_club['phone_club'];?>
                                 </tbody>
                            </table>
                            
                        </div>
                        <!-- END Phone -->

                        <!-- Email -->
                        <div class="tab-pane" id="club-tabs-email">
                            <h4 class="sub-header">Email</h4>
                            <table class="table table-hover table-striped">
                                <tbody>
                                    <?php echo $sel_club['email_club'];?>
                                 </tbody>
                            </table>
                            
                        </div>
                        <!-- END Email -->

                         <!-- Ratings -->
                        <div class="tab-pane" id="club-tabs-ratings">
                            <h4 class="sub-header">Service</h4>
                            <table class="table table-hover table-striped">
                                <tbody>
                                    <tr>
                                        <td class="span3"><strong>Feature #1</strong></td>
                                        <td><em>Feature Details</em></td>
                                    </tr>
                                
                                </tbody>
                            </table>
                            <h4 class="sub-header">Stuff</h4>
                            <table class="table table-hover table-striped">
                                <tbody>
                                    <tr>
                                        <td class="span3"><strong>Feature #1</strong></td>
                                        <td><em>Feature Details</em></td>
                                    </tr>
                                   
                                </tbody>
                            </table>
                            <h4 class="sub-header">Course</h4>
                            <table class="table table-hover table-striped remove-margin">
                                <tbody>
                                    <tr>
                                        <td class="span3"><strong>Feature #1</strong></td>
                                        <td><em>Feature Details</em></td>
                                    </tr>
                                    
                                </tbody>
                            </table>
                            <h4 class="sub-header">Clean</h4>
                            <table class="table table-hover table-striped remove-margin">
                                <tbody>
                                    <tr>
                                        <td class="span3"><strong>Feature #1</strong></td>
                                        <td><em>Feature Details</em></td>
                                    </tr>                              
                                </tbody>
                            </table>
                        </div>
                        <!-- END Ratings -->



                        <!-- Features -->
                        <div class="tab-pane" id="club-tabs-features">
                            <h4 class="sub-header">Category #1</h4>
                            <table class="table table-hover table-striped">
                                <tbody>
                                    <tr>
                                        <td class="span3"><strong>Feature #1</strong></td>
                                        <td><em>Feature Details</em></td>
                                    </tr>
                                    <tr>
                                        <td class="span3"><strong>Feature #2</strong></td>
                                        <td><em>Feature Details</em></td>
                                    </tr>
                                    <tr>
                                        <td class="span3"><strong>Feature #3</strong></td>
                                        <td><em>Feature Details</em></td>
                                    </tr>
                                    <tr>
                                        <td class="span3"><strong>Feature #4</strong></td>
                                        <td><em>Feature Details</em></td>
                                    </tr>
                                    <tr>
                                        <td class="span3"><strong>Feature #5</strong></td>
                                        <td><em>Feature Details</em></td>
                                    </tr>
                                </tbody>
                            </table>
                            <h4 class="sub-header">Category #2</h4>
                            <table class="table table-hover table-striped">
                                <tbody>
                                    <tr>
                                        <td class="span3"><strong>Feature #1</strong></td>
                                        <td><em>Feature Details</em></td>
                                    </tr>
                                    <tr>
                                        <td class="span3"><strong>Feature #2</strong></td>
                                        <td><em>Feature Details</em></td>
                                    </tr>
                                    <tr>
                                        <td class="span3"><strong>Feature #3</strong></td>
                                        <td><em>Feature Details</em></td>
                                    </tr>
                                    <tr>
                                        <td class="span3"><strong>Feature #4</strong></td>
                                        <td><em>Feature Details</em></td>
                                    </tr>
                                    <tr>
                                        <td class="span3"><strong>Feature #5</strong></td>
                                        <td><em>Feature Details</em></td>
                                    </tr>
                                </tbody>
                            </table>
                            <h4 class="sub-header">Category #3</h4>
                            <table class="table table-hover table-striped remove-margin">
                                <tbody>
                                    <tr>
                                        <td class="span3"><strong>Feature #1</strong></td>
                                        <td><em>Feature Details</em></td>
                                    </tr>
                                    <tr>
                                        <td class="span3"><strong>Feature #2</strong></td>
                                        <td><em>Feature Details</em></td>
                                    </tr>
                                    <tr>
                                        <td class="span3"><strong>Feature #3</strong></td>
                                        <td><em>Feature Details</em></td>
                                    </tr>
                                    <tr>
                                        <td class="span3"><strong>Feature #4</strong></td>
                                        <td><em>Feature Details</em></td>
                                    </tr>
                                    <tr>
                                        <td class="span3"><strong>Feature #5</strong></td>
                                        <td><em>Feature Details</em></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- END Features -->

                        <!-- Reviews -->
                        <div class="tab-pane" id="club-tabs-reviews">
                            <!-- First Review -->
                            <div class="media media-hover">
                                <a href="javascript:void(0)" class="pull-left">
                                    <img src="" class="media-object img-circle" alt="Image">
                                </a>
                                <div class="media-body">
                                    <h5 class="media-heading">
                                        <span class="label label-success">
                                            <i class="icon-star"></i>
                                            <i class="icon-star"></i>
                                            <i class="icon-star"></i>
                                            <i class="icon-star"></i>
                                            <i class="icon-star-empty"></i>
                                        </span>
                                        <a class="badge badge-success" data-toggle="tooltip" title="This review helped me!"><i class="icon-thumbs-up"></i> 9</a>
                                        <a class="badge badge-important" data-toggle="tooltip" title="This review didn't help me!"><i class="icon-thumbs-down"></i> 1</a>
                                        <a href="javascript:void(0)">Username1</a>
                                        <small>on <span class="label label-inverse">July 26, 2013</span></small>
                                    </h5>
                                    <p><strong>Good:</strong> First, Second, Third etc</p>
                                    <p><strong>Bad:</strong> First, Second, Third etc</p>
                                    <p><strong>Comments:</strong> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ultrices, justo vel imperdiet gravida, urna ligula hendrerit nibh, ac cursus nibh sapien in purus. Donec lacinia venenatis metus at bibendum? In hac habitasse platea dictumst. Proin ac nibh rutrum lectus rhoncus eleifend. Sed porttitor pretium venenatis. Suspendisse potenti. Aliquam quis ligula elit. Aliquam at orci ac neque semper dictum. Sed tincidunt scelerisque ligula, et facilisis nulla hendrerit non. Suspendisse potenti. Pellentesque non accumsan orci. Praesent at lacinia dolor. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                </div>
                            </div>
                            <!-- END First Review -->

                            <!-- Second Review -->
                            <div class="media media-hover">
                                <a href="javascript:void(0)" class="pull-left">
                                    <img src="img/placeholders/image_64x64_dark.png" class="media-object img-circle" alt="Image">
                                </a>
                                <div class="media-body">
                                    <h5 class="media-heading">
                                        <span class="label label-warning">
                                            <i class="icon-star"></i>
                                            <i class="icon-star"></i>
                                            <i class="icon-star"></i>
                                            <i class="icon-star-empty"></i>
                                            <i class="icon-star-empty"></i>
                                        </span>
                                        <a class="badge badge-success" data-toggle="tooltip" title="This review helped me!"><i class="icon-thumbs-up"></i> 5</a>
                                        <a class="badge badge-important" data-toggle="tooltip" title="This review didn't help me!"><i class="icon-thumbs-down"></i> 4</a>
                                        <a href="javascript:void(0)">Username2</a>
                                        <small>on <span class="label label-inverse">July 25, 2013</span></small>
                                    </h5>
                                    <p><strong>Good:</strong> First, Second, Third etc</p>
                                    <p><strong>Bad:</strong> First, Second, Third etc</p>
                                    <p><strong>Comments:</strong> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ultrices, justo vel imperdiet gravida, urna ligula hendrerit nibh, ac cursus nibh sapien in purus. Donec lacinia venenatis metus at bibendum? In hac habitasse platea dictumst. Proin ac nibh rutrum lectus rhoncus eleifend. Sed porttitor pretium venenatis. Suspendisse potenti. Aliquam quis ligula elit. Aliquam at orci ac neque semper dictum. Sed tincidunt scelerisque ligula, et facilisis nulla hendrerit non. Suspendisse potenti. Pellentesque non accumsan orci. Praesent at lacinia dolor. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                </div>
                            </div>
                            <!-- END Second Review -->

                            <!-- Third Review -->
                            <div class="media media-hover">
                                <a href="javascript:void(0)" class="pull-left">
                                    <img src="img/placeholders/image_64x64_dark.png" class="media-object img-circle" alt="Image">
                                </a>
                                <div class="media-body">
                                    <h5 class="media-heading">
                                        <span class="label label-success">
                                            <i class="icon-star"></i>
                                            <i class="icon-star"></i>
                                            <i class="icon-star"></i>
                                            <i class="icon-star"></i>
                                            <i class="icon-star"></i>
                                        </span>
                                        <a class="badge badge-success" data-toggle="tooltip" title="This review helped me!"><i class="icon-thumbs-up"></i> 3</a>
                                        <a class="badge badge-important" data-toggle="tooltip" title="This review didn't help me!"><i class="icon-thumbs-down"></i> 0</a>
                                        <a href="javascript:void(0)">Username3</a>
                                        <small>on <span class="label label-inverse">July 16, 2013</span></small>
                                    </h5>
                                    <p><strong>Good:</strong> First, Second, Third etc</p>
                                    <p><strong>Bad:</strong> First, Second, Third etc</p>
                                    <p><strong>Comments:</strong> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ultrices, justo vel imperdiet gravida, urna ligula hendrerit nibh, ac cursus nibh sapien in purus. Donec lacinia venenatis metus at bibendum? In hac habitasse platea dictumst. Proin ac nibh rutrum lectus rhoncus eleifend. Sed porttitor pretium venenatis. Suspendisse potenti. Aliquam quis ligula elit. Aliquam at orci ac neque semper dictum. Sed tincidunt scelerisque ligula, et facilisis nulla hendrerit non. Suspendisse potenti. Pellentesque non accumsan orci. Praesent at lacinia dolor. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                </div>
                            </div>
                            <!-- END Third Review -->

                            <!-- Fourth Review -->
                            <div class="media media-hover">
                                <a href="javascript:void(0)" class="pull-left">
                                    <img src="img/placeholders/image_64x64_dark.png" class="media-object img-circle" alt="Image">
                                </a>
                                <div class="media-body">
                                    <h5 class="media-heading">
                                        <span class="label label-important">
                                            <i class="icon-star"></i>
                                            <i class="icon-star"></i>
                                            <i class="icon-star-empty"></i>
                                            <i class="icon-star-empty"></i>
                                            <i class="icon-star-empty"></i>
                                        </span>
                                        <a class="badge badge-success" data-toggle="tooltip" title="This review helped me!"><i class="icon-thumbs-up"></i> 3</a>
                                        <a class="badge badge-important" data-toggle="tooltip" title="This review didn't help me!"><i class="icon-thumbs-down"></i> 0</a>
                                        <a href="javascript:void(0)">Username4</a>
                                        <small>on <span class="label label-inverse">July 5, 2013</span></small>
                                    </h5>
                                    <p><strong>Good:</strong> First, Second, Third etc</p>
                                    <p><strong>Bad:</strong> First, Second, Third etc</p>
                                    <p><strong>Comments:</strong> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ultrices, justo vel imperdiet gravida, urna ligula hendrerit nibh, ac cursus nibh sapien in purus. Donec lacinia venenatis metus at bibendum? In hac habitasse platea dictumst. Proin ac nibh rutrum lectus rhoncus eleifend. Sed porttitor pretium venenatis. Suspendisse potenti. Aliquam quis ligula elit. Aliquam at orci ac neque semper dictum. Sed tincidunt scelerisque ligula, et facilisis nulla hendrerit non. Suspendisse potenti. Pellentesque non accumsan orci. Praesent at lacinia dolor. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                </div>
                            </div>
                            <!-- END Fourth Review -->
                        </div>
                        <!-- END Reviews -->
                    </div>
                    <!-- END Tabs Content -->
                </div>
                <!-- END Description, Features and Reviews -->
            </div>
            <!-- END Page Content -->

            <!-- Footer -->
            <?php include 'inc/footer.php'; ?>
            <!-- END Footer -->
        </div>
        <!-- END Page Container -->

        <!-- Scroll to top link, check main.js - scrollToTop() -->
        <a href="#" id="to-top"><i class="icon-chevron-up"></i></a>

        <!-- User Modal Account, appears when clicking on 'User Settings' link found on user dropdown menu (header, top right) -->
        <div id="modal-user-account" class="modal hide fade">
            <!-- Modal Body -->
            <div class="modal-body remove-padding">
                <!-- Modal Tabs -->
                <div class="block-tabs">
                    <div class="block-options">
                        <a href="javascript:void(0)" class="btn btn-danger" data-dismiss="modal"><i class="icon-remove"></i></a>
                    </div>
                    <ul class="nav nav-tabs" data-toggle="tabs">
                        <li class="active"><a href="#modal-user-account-account"><i class="icon-cog"></i> Account</a></li>
                        <li><a href="#modal-user-account-profile"><i class="icon-user"></i> Profile</a></li>
                    </ul>
                    <div class="tab-content">
                        <!-- Account Tab Content -->
                        <div class="tab-pane active" id="modal-user-account-account">
                            <form action="index.html" method="post" class="form-horizontal" onsubmit="return false;">
                                <div class="control-group">
                                    <label class="control-label" for="modal-account-username">Username</label>
                                    <div class="controls">
                                        <input type="text" id="modal-account-username" name="modal-account-username" value="admin" class="disabled" disabled>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="modal-account-email">Email</label>
                                    <div class="controls">
                                        <input type="text" id="modal-account-email" name="modal-account-email" value="admin@exampleapp.com">
                                    </div>
                                </div>
                                <h4 class="sub-header">Change Password</h4>
                                <div class="control-group">
                                    <label class="control-label" for="modal-account-pass">Current Password</label>
                                    <div class="controls">
                                        <input type="password" id="modal-account-pass" name="modal-account-pass">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="modal-account-newpass">New Password</label>
                                    <div class="controls">
                                        <input type="password" id="modal-account-newpass" name="modal-account-newpass">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="modal-account-newrepass">Retype New Password</label>
                                    <div class="controls">
                                        <input type="password" id="modal-account-newrepass" name="modal-account-newrepass">
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- END Account Tab Content -->

                        <!-- Profile Tab Content -->
                        <div class="tab-pane" id="modal-user-account-profile">
                            <form action="index.html" method="post" class="form-horizontal" onsubmit="return false;">
                                <div class="control-group">
                                    <label class="control-label" for="modal-profile-name">Name</label>
                                    <div class="controls">
                                        <input type="text" id="modal-profile-name" name="modal-profile-name" value="John Doe">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="modal-profile-gender">Gender</label>
                                    <div class="controls">
                                        <select id="modal-profile-gender" name="modal-profile-name">
                                            <option value="m">Male</option>
                                            <option value="f">Female</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="modal-profile-birthdate">Birthdate</label>
                                    <div class="controls">
                                        <div class="input-append">
                                            <input type="text" id="modal-profile-birthdate" name="modal-profile-birthdate" class="input-small input-datepicker">
                                            <span class="add-on"><i class="icon-calendar"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="modal-profile-skills">Skills</label>
                                    <div class="controls">
                                        <select id="modal-profile-skills" name="modal-profile-skills" class="select-chosen" multiple>
                                            <option value="html" selected>html</option>
                                            <option value="css" selected>css</option>
                                            <option value="javascript">javascript</option>
                                            <option value="php">php</option>
                                            <option value="mysql">mysql</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="modal-profile-bio">Bio</label>
                                    <div class="controls">
                                        <textarea id="modal-profile-bio" name="modal-profile-bio" class="textarea-elastic" rows="3">Bio Information..</textarea>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- END Profile Tab Content -->
                    </div>
                </div>
                <!-- END Modal Tabs -->
            </div>
            <!-- END Modal Body -->

            <!-- Modal footer -->
            <div class="modal-footer">
                <button class="btn btn-success" data-dismiss="modal"><i class="icon-save"></i> Save</button>
            </div>
            <!-- END Modal footer -->
        </div>
        <!-- END User Modal Settings -->

        <!-- Excanvas for Flot (Charts plugin) support on IE8 -->
        <!--[if lte IE 8]><script src="js/helpers/excanvas.min.js"></script><![endif]-->

        <!-- Get Jquery library from Google ... -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <!-- ... but if something goes wrong get Jquery from local file -->
        <script>!window.jQuery && document.write(unescape('%3Cscript src="js/vendor/jquery-1.9.1.min.js"%3E%3C/script%3E'));</script>

        <!-- Bootstrap.js -->
        <script src="js/vendor/bootstrap.min.js"></script>

        <!--
        Include Google Maps API for global use.
        If you don't want to use  Google Maps API globally, just remove this line and the gmaps.js plugin from js/plugins.js (you can put it in a seperate file)
        Then iclude them both in the pages you would like to use the google maps functionality
        -->
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>

        <!-- Jquery plugins and custom javascript code -->
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
    </body>
</html>