<?php 
    include 'inc/connection.php';
    include 'inc/functions.php';
?>


<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <?php include 'inc/head.php'; ?>
    </head>

    <!-- Body -->
    <!-- In the PHP version you can set the following options from the config file -->
    <!-- Add the class .hide-side-content to <body> to hide side content by default -->
    <body>
        <!-- Page Container -->
        <!-- In the PHP version you can set the following options from the config file -->
        <!-- Add the class .full-width for a full width page -->
        <div id="page-container" class="full-width">
            <!-- Header -->
            <?php include 'inc/top.php'; ?>
            <!-- END Header -->
            <!-- Left Sidebar -->
            <?php require 'inc/side.php'; ?>
            <!-- END Left Sidebar -->
            <!-- Pre Page Content -->
            <div id="pre-page-content">
                <h1><i class="glyphicon-shopping_bag themed-color"></i>Club List<br><small>List all clubs</small></h1>
            </div>
            <!-- END Pre Page Content -->

            <!-- Page Content -->
            <div id="page-content">
                <!-- Breadcrumb -->
                <!-- You can have the breadcrumb stick on scrolling just by adding the following attributes with their values (data-spy="affix" data-offset-top="250") -->
                <!-- You can try it on other elements too :-), the sticky position and style can be adjusted in the css/main.css with .affix class -->
                <ul class="breadcrumb" data-spy="affix" data-offset-top="250">
                    <li>
                        <a href="index.html"><i class="glyphicon-display"></i></a> <span class="divider"><i class="icon-angle-right"></i></span>
                    </li>
                    <li>
                        <a href="dashboard.php">Dashboard</a> <span class="divider"><i class="icon-angle-right"></i></span>
                    </li>
                    <li class="active"><a href="">Club List</a></li>
                </ul>
                <!-- END Breadcrumb -->

                <!-- Products List Block -->
                <div class="block block-themed block-last">
                    <!-- Products List Title -->
                    <div class="block-title">
                        <div class="block-options">
                            <div class="btn-group">
                                <a class="btn dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)">Order By <i class="icon-angle-down"></i></a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="javascript:void(0)">Price (Low to High)</a></li>
                                    <li><a href="javascript:void(0)">Price (High to Low)</a></li>
                                    <li><a href="javascript:void(0)">Newest</a></li>
                                    <li class="divider"></li>
                                    <li><a href="javascript:void(0)">Best Sellers</a></li>
                                </ul>
                            </div>
                            <div class="btn-group">
                                <a class="btn dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)">10 <i class="icon-angle-down"></i></a>
                                <ul class="dropdown-menu pull-right text-center">
                                    <li><a href="javascript:void(0)">20</a></li>
                                    <li><a href="javascript:void(0)">30</a></li>
                                    <li><a href="javascript:void(0)">40</a></li>
                                    <li class="divider"></li>
                                    <li><a href="javascript:void(0)">All Products</a></li>
                                </ul>
                            </div>
                        </div>
                        <h4><i class="icon-asterisk"></i> Showing Clubs</h4>
                    </div>
                    <!-- END Products List Title -->

                    <!-- Products List Content -->
                    <div class="block-content">
                        <div class="row-fluid row-items">
                            <div class="span3">
                                <!-- Categories Menu Block -->
                                <div class="block">
                                    <!-- Categories Menu Title -->
                                    
                                    
                                    <div class="block-title">
                                        <h4>Activities Menu</h4>
                                    </div>
                                    <!-- END Categories Menu Title -->

                                    <!-- Categories Menu Content -->
                                    
                                    <div class="block-content full">

                                        <ul class="nav nav-pills nav-stacked remove-margin">
                                        	<li><a href="#">All</a></li>
                                            <?php 
                                            	$activity_set = get_all_activities();
                                            	 while ( $activity = mysql_fetch_array($activity_set) ) {
                                            ?>
                                            <li><a href="#"><?php echo $activity['name_activity'] ?></a></li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                    <!-- END Categories Menu Content -->
                                </div>
                                <!-- END Categories Menu Block -->

                                <!-- Filters #1 Block -->
                                <div class="block">
                                    <!-- Filters #1 Title -->
                                    <div class="block-title">
                                        <h4>Filters #1</h4>
                                    </div>
                                    <!-- END Filters #1 Title -->

                                    <!-- Filters #1 Content -->
                                    <div class="block-content full">
                                        <label for="filter1">
                                            <input type="checkbox" id="filter1" name="filter1" class="input-themed">
                                            Filter #1
                                        </label>
                                        <label for="filter2" class="remove-margin">
                                            <input type="checkbox" id="filter2" name="filter2" class="input-themed">
                                            Filter #2
                                        </label>
                                    </div>
                                    <!-- END Filters #1 Content -->
                                </div>
                                <!-- END Filters #1 Block -->

                                <!-- Filters #2 Block -->
                                <div class="block">
                                    <!-- Filters #2 Title -->
                                    <div class="block-title">
                                        <h4>Filters #2</h4>
                                    </div>
                                    <!-- END Filters #2 Title -->

                                    <!-- Filters #2 Content -->
                                    <div class="block-content full">
                                        <label for="filter3">
                                            <input type="checkbox" id="filter3" name="filter3" class="input-themed">
                                            Filter #3
                                        </label>
                                        <label for="filter4" class="remove-margin">
                                            <input type="checkbox" id="filter4" name="filter4" class="input-themed">
                                            Filter #4
                                        </label>
                                    </div>
                                    <!-- END Filters #2 Content -->
                                </div>
                                <!-- END Filters #2 Block -->

                                <!-- Filters #3 Block -->
                                <div class="block">
                                    <!-- Filters #3 Title -->
                                    <div class="block-title">
                                        <h4>Filters #3</h4>
                                    </div>
                                    <!-- END Filters #3 Title -->

                                    <!-- Filters #3 Content -->
                                    <div class="block-content full">
                                        <label for="filter5">
                                            <input type="checkbox" id="filter5" name="filter5" class="input-themed">
                                            Filter #5
                                        </label>
                                        <label for="filter6">
                                            <input type="checkbox" id="filter6" name="filter6" class="input-themed">
                                            Filter #6
                                        </label>
                                        <label for="filter7">
                                            <input type="checkbox" id="filter7" name="filter7" class="input-themed">
                                            Filter #7
                                        </label>
                                        <label for="filter8" class="remove-margin">
                                            <input type="checkbox" id="filter8" name="filter8" class="input-themed">
                                            Filter #8
                                        </label>
                                    </div>
                                    <!-- END Filters #3 Content -->
                                </div>
                                <!-- END Filters #3 Block -->
                            </div>

                            <div class="span9">
                                <a href="admin_add_club.php"><button class="btn btn-large btn-info">Add Club</button></a>
                            	<?php 
                            		$club_set = get_all_clubs();
                            		while ($club = mysql_fetch_array($club_set)) {
                            	?>
                               <div class="media media-hover">
                                    <a href="javascript:void(0)" class="pull-left">
                                        <img src="img/placeholders/image_160x120_dark.png" class="media-object img-rounded" alt="Image">
                                    </a>
                                    <div class="media-body">
                                        <div class="pull-right">
                                           	
                                            <a href="edit_club.php?clb_id=<?php echo urlencode($club['id_club']); ?>" class="btn btn-success">Edit</a>
                                            <a href="delete_club.php?clb_id=<?php echo urlencode($club['id_club']); ?>" class="btn btn-warning"> Delete</a>
                                        </div>
                                        <h4 class="media-heading"><a href="admin_club_single.php?clb_id=<?php echo urlencode($club['id_club']); ?>"><?php echo $club['name_club']; ?></a></h4>
                                        <p><?php echo_200($club['description_club']); ?><br></p>
                                    </div>
                                    
                                </div>
                                <?php } ?>
								

                                
                                <div class="pagination pagination-centered">
                                    <ul>
                                        <li class="disabled"><a href="javascript:void(0)">Prev</a></li>
                                        <li class="active"><a href="javascript:void(0)">1</a></li>
                                        <li><a href="javascript:void(0)">2</a></li>
                                        <li><a href="javascript:void(0)">3</a></li>
                                        <li><a href="javascript:void(0)">4</a></li>
                                        <li><a href="javascript:void(0)">5</a></li>
                                        <li><a href="javascript:void(0)">Next</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END Products List Content -->
                </div>
                <!-- END Products List Block -->
            </div>
            <!-- END Page Content -->

            <!-- Footer -->
            <?php include 'inc/footer.php'; ?>
            <!-- END Footer -->
        </div>
        <!-- END Page Container -->

        <!-- Scroll to top link, check main.js - scrollToTop() -->
        <a href="#" id="to-top"><i class="icon-chevron-up"></i></a>

        <!-- User Modal Account, appears when clicking on 'User Settings' link found on user dropdown menu (header, top right) -->
        <div id="modal-user-account" class="modal hide fade">
            <!-- Modal Body -->
            <div class="modal-body remove-padding">
                <!-- Modal Tabs -->
                <div class="block-tabs">
                    <div class="block-options">
                        <a href="javascript:void(0)" class="btn btn-danger" data-dismiss="modal"><i class="icon-remove"></i></a>
                    </div>
                    <ul class="nav nav-tabs" data-toggle="tabs">
                        <li class="active"><a href="#modal-user-account-account"><i class="icon-cog"></i> Account</a></li>
                        <li><a href="#modal-user-account-profile"><i class="icon-user"></i> Profile</a></li>
                    </ul>
                    <div class="tab-content">
                        <!-- Account Tab Content -->
                        <div class="tab-pane active" id="modal-user-account-account">
                            <form action="index.html" method="post" class="form-horizontal" onsubmit="return false;">
                                <div class="control-group">
                                    <label class="control-label" for="modal-account-username">Username</label>
                                    <div class="controls">
                                        <input type="text" id="modal-account-username" name="modal-account-username" value="admin" class="disabled" disabled>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="modal-account-email">Email</label>
                                    <div class="controls">
                                        <input type="text" id="modal-account-email" name="modal-account-email" value="admin@exampleapp.com">
                                    </div>
                                </div>
                                <h4 class="sub-header">Change Password</h4>
                                <div class="control-group">
                                    <label class="control-label" for="modal-account-pass">Current Password</label>
                                    <div class="controls">
                                        <input type="password" id="modal-account-pass" name="modal-account-pass">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="modal-account-newpass">New Password</label>
                                    <div class="controls">
                                        <input type="password" id="modal-account-newpass" name="modal-account-newpass">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="modal-account-newrepass">Retype New Password</label>
                                    <div class="controls">
                                        <input type="password" id="modal-account-newrepass" name="modal-account-newrepass">
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- END Account Tab Content -->

                        <!-- Profile Tab Content -->
                        <div class="tab-pane" id="modal-user-account-profile">
                            <form action="index.html" method="post" class="form-horizontal" onsubmit="return false;">
                                <div class="control-group">
                                    <label class="control-label" for="modal-profile-name">Name</label>
                                    <div class="controls">
                                        <input type="text" id="modal-profile-name" name="modal-profile-name" value="John Doe">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="modal-profile-gender">Gender</label>
                                    <div class="controls">
                                        <select id="modal-profile-gender" name="modal-profile-name">
                                            <option value="m">Male</option>
                                            <option value="f">Female</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="modal-profile-birthdate">Birthdate</label>
                                    <div class="controls">
                                        <div class="input-append">
                                            <input type="text" id="modal-profile-birthdate" name="modal-profile-birthdate" class="input-small input-datepicker">
                                            <span class="add-on"><i class="icon-calendar"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="modal-profile-skills">Skills</label>
                                    <div class="controls">
                                        <select id="modal-profile-skills" name="modal-profile-skills" class="select-chosen" multiple>
                                            <option value="html" selected>html</option>
                                            <option value="css" selected>css</option>
                                            <option value="javascript">javascript</option>
                                            <option value="php">php</option>
                                            <option value="mysql">mysql</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="modal-profile-bio">Bio</label>
                                    <div class="controls">
                                        <textarea id="modal-profile-bio" name="modal-profile-bio" class="textarea-elastic" rows="3">Bio Information..</textarea>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- END Profile Tab Content -->
                    </div>
                </div>
                <!-- END Modal Tabs -->
            </div>
            <!-- END Modal Body -->

            <!-- Modal footer -->
            <div class="modal-footer">
                <button class="btn btn-success" data-dismiss="modal"><i class="icon-save"></i> Save</button>
            </div>
            <!-- END Modal footer -->
        </div>
        <!-- END User Modal Settings -->

        <!-- Excanvas for Flot (Charts plugin) support on IE8 -->
        <!--[if lte IE 8]><script src="js/helpers/excanvas.min.js"></script><![endif]-->

        <!-- Get Jquery library from Google ... -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <!-- ... but if something goes wrong get Jquery from local file -->
        <script>!window.jQuery && document.write(unescape('%3Cscript src="js/vendor/jquery-1.9.1.min.js"%3E%3C/script%3E'));</script>

        <!-- Bootstrap.js -->
        <script src="js/vendor/bootstrap.min.js"></script>

        <!--
        Include Google Maps API for global use.
        If you don't want to use  Google Maps API globally, just remove this line and the gmaps.js plugin from js/plugins.js (you can put it in a seperate file)
        Then iclude them both in the pages you would like to use the google maps functionality
        -->
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>

        <!-- Jquery plugins and custom javascript code -->
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
    </body>
</html>