<footer>
                <div class="pull-right">
                    Crafted with <i class="icon-heart"></i> by <strong><a href="http://goo.gl/vNS3I" target="_blank">pixelcave</a></strong>
                </div>
                <div class="pull-left">
                    <span id="year-copy"></span> &copy; <strong><a href="http://goo.gl/mssAH" target="_blank">FlatApp 1.2.1</a></strong>
                </div>
            </footer>