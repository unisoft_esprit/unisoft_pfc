<?php 
	function redirect_to( $location = NULL ) {
		if ($location != NULL) {
			header("Location: {$location}");
			exit;
		}
	}
	//Strings Special functions 
	function echo_200($x){

	  if(strlen($x)<=200)
	  {
	    echo $x;
	  }
	  else
	  {
	    $y=substr($x,0,200) . '...';
	    echo $y;
	  }
	}    


	function explode_text_to_lines($value)
	{
		
	$text = trim($value);
	$textAr = explode("\n", $value);
	$textAr = array_filter($textAr, 'trim'); // remove any extra \r characters left behind

	foreach ($textAr as $line) {
	    echo "<li><i class=\"icon-li icon-ok text-success\"></i>".$line."</li> ";
	} 
	}

	function explode_text_to_row($value)
	{
		
	$text = trim($value);
	$textAr = explode("\n", $value);
	$textAr = array_filter($textAr, 'trim'); // remove any extra \r characters left behind

	foreach ($textAr as $line) {
	    echo "<tr><td class=\"span3\"><strong>". $line ."</strong></td></tr>";
	} 

	}


	//Databes Functions
	function get_all_activities() {
		global $connection;
		$query = "SELECT * FROM ftw_activities ";

		$result = mysql_query($query, $connection);

		if (!$result) {
			die("query error");
		}
		return $result;
	}

	function get_all_clubs() {
		global $connection;
		$query = "SELECT * FROM ftw_clubs ";

		$result = mysql_query($query, $connection);

		if (!$result) {
			die("query error");
		}
		return $result;
	}

	function confirm_query($result_set) {
		if (!$result_set) {
			die("Database query failed: " . mysql_error());
		}
	}


	function mysql_prep( $value ) {
		$magic_quotes_active = get_magic_quotes_gpc();
		$new_enough_php = function_exists( "mysql_real_escape_string" ); // i.e. PHP >= v4.3.0
		if( $new_enough_php ) { // PHP v4.3.0 or higher
			// undo any magic quote effects so mysql_real_escape_string can do the work
			if( $magic_quotes_active ) { $value = stripslashes( $value ); }
			$value = mysql_real_escape_string( $value );
		} else { // before PHP v4.3.0
			// if magic quotes aren't already on then add slashes manually
			if( !$magic_quotes_active ) { $value = addslashes( $value ); }
			// if magic quotes are active, then the slashes already exist
		}
		return $value;
	}

	function get_all_activities_type() {
		global $connection;
		$query = "SELECT * FROM ftw_activities_type ";

		$result = mysql_query($query, $connection);

		if (!$result) {
			die("query error");
		}
		return $result;
	}


	function get_activity_by_id($activity_id) {
		global $connection;
		$query = "SELECT * ";
		$query .= "FROM ftw_activities ";
		$query .= "WHERE id_activity=" . $activity_id ." ";
		$query .= "LIMIT 1";
		$result_set = mysql_query($query, $connection);
	
		if ($activity = mysql_fetch_array($result_set)) {
			return $activity;
		} else {
			return NULL;
		}
	}

	function get_club_by_id($club_id) {
		global $connection;
		$query = "SELECT * ";
		$query .= "FROM ftw_clubs ";
		$query .= "WHERE id_club=" . $club_id ." ";
		$query .= "LIMIT 1";
		$result_set = mysql_query($query, $connection);
		if ($club = mysql_fetch_array($result_set)) {
			return $club;
		} else {
			return NULL;
		}
	}

	function get_featured_img($club_id)
	{
		global $connection;
		$query = " SELECT link_media FROM ftw_medias_clubs WHERE fk_id_club = " . $club_id ." LIMIT 1" ;
		$result_set = mysql_query($query , $connection) ;
		if ($img = mysql_fetch_array($result_set)) {
			return $img['link_media'];
		} else {
			return NULL;
		}
	}

	function activities_type_navigation($sel_subject, $sel_page, $public = false) {
		$output = "<ul class=\"subjects\">";
		$subject_set = get_all_subjects($public);
		while ($subject = mysql_fetch_array($subject_set)) {
			$output .= "<li";
			if ($subject["id"] == $sel_subject['id']) { $output .= " class=\"selected\""; }
			$output .= "><a href=\"edit_subject.php?subj=" . urlencode($subject["id"]) . 
				"\">{$subject["menu_name"]}</a></li>";
			$page_set = get_pages_for_subject($subject["id"], $public);
			$output .= "<ul class=\"pages\">";
			while ($page = mysql_fetch_array($page_set)) {
				$output .= "<li";
				if ($page["id"] == $sel_page['id']) { $output .= " class=\"selected\""; }
				$output .= "><a href=\"content.php?page=" . urlencode($page["id"]) .
					"\">{$page["menu_name"]}</a></li>";
			}
			$output .= "</ul>";
		}
		$output .= "</ul>";
		return $output;
	}


	function find_selected_activity() {
		global $sel_activity;
		
		if (isset($_GET['act_id'])) {
			$sel_activity = get_activity_by_id($_GET['act_id']);
			
		}
	}

	function find_selected_Club()
	{
		global $sel_club;
		if(isset($_GET['clb_id'])){
			$sel_club = get_club_by_id($_GET['clb_id']);
		}
	}

 ?>