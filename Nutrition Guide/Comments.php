<?php
 session_start();
 include "dbconnect.php";$ID = $_GET['id']; ?>
<input type="hidden" id="PostID" value="<?php  echo $ID  ?>">
<input type="hidden" id="User" value="<?php  echo $_SESSION['UserMail']  ?>">

<?php
    $Record = $bdd->query("SELECT Comments.*,Users.* FROM Comments inner join Users on Submitter=Email WHERE PostID='$ID' AND Parent='0' ORDER BY Date_Time DESC ");
    while ($Comments = $Record->fetch()) {
        $Date = date_parse($Comments['Date_Time']);
        $month = $Date['month'];
        switch ($month) {
            case 1 :
                $month = "January";
                break;
            case 2 :
                $month = "February";
                break;
            case 3 :
                $month = "March";
                break;
            case 4 :
                $month = "April";
                break;
            case 5 :
                $month = "May";
                break;
            case 6 :
                $month = "June";
                break;
            case 7 :
                $month = "July";
                break;
            case 8 :
                $month = "August";
                break;
            case 9 :
                $month = "September";
                break;
            case 10 :
                $month = "October";
                break;
            case 11 :
                $month = "November";
                break;
            case 12 :
                $month = "December";
                break;
        }

    ?>
    <li class="comment byuser comment-author-admin bypostauthor even thread-even depth-1"  >
        <div class="comment-info">
            <img src="<?php  echo $Comments['ImgUrl']  ?>" width="60" height="60" alt="" style="border-radius:50%;">
            <cite><?php  echo $Comments['Firstname'] . " " . $Comments['Lastname']  ?> Says:
                <br />
                <span class="comment-data"><a><?php  echo $month . " " . $Date['day'] . ", " . $Date['year'] . " at " . $Date['hour'] . ":" . $Date['minute']; ?></a> </span> </cite>
        </div>
        <div class="comment-text">
            <p style="overflow: auto;
               word-wrap:break-word;">
               <?php  echo $Comments['Content']  ?>
            </p>
            <div class="reply">
                <a  class="comment-reply-link ReplyButton" id="<?php  echo $Comments['ID']  ?>">Reply</a> 
            </div>
        </div>
        <!-- Reply -->
        <ul class='children'>
            <li class="comment byuser comment-author-admin bypostauthor odd alt depth-2" style="visibility: hidden;height:0px;" id="Reply<?php  echo $Comments['ID']  ?>">
                    <textarea  name="CommentChild<?php  echo $Comments['ID']  ?>" placeholder="Write a comment ..." style="width: 500px; resize: none; margin-bottom: 0px; border-width: 0px 1px 0px 0px;"></textarea>
            </li>
            <?php
    $CommID = $Comments['ID'];
    $QueryChild = $bdd->query("SELECT Comments.*,Users.* FROM Comments inner join Users on Submitter=Email WHERE PostID='$ID' and Parent='$CommID' ORDER BY `Comments`.`Date_Time` DESC   ");
    while ($CommentsChild = $QueryChild->fetch()) {
        $Date = date_parse($CommentsChild['Date_Time']);
        $month = $Date['month'];
        switch ($month) {
            case 1 :
                $month = "January";
                break;
            case 2 :
                $month = "February";
                break;
            case 3 :
                $month = "March";
                break;
            case 4 :
                $month = "April";
                break;
            case 5 :
                $month = "May";
                break;
            case 6 :
                $month = "June";
                break;
            case 7 :
                $month = "July";
                break;
            case 8 :
                $month = "August";
                break;
            case 9 :
                $month = "September";
                break;
            case 10 :
                $month = "October";
                break;
            case 11 :
                $month = "November";
                break;
            case 12 :
                $month = "December";
                break;
        }

    ?>
            <li class="comment byuser comment-author-admin bypostauthor odd alt depth-2" >
                <div class="comment-info">
                    <img src="<?php  echo $CommentsChild['ImgUrl']  ?>" width="50" height="50" alt="" style="border-radius:50%;">
                    <cite><?php  echo $CommentsChild['Firstname'] . " " . $CommentsChild['Lastname']  ?> Says:
                        <br />
                        <span class="comment-data"><a ><?php  echo $month . " " . $Date['day'] . ", " . $Date['year'] . " at " . $Date['hour'] . ":" . $Date['minute']; ?></a> </span> </cite>
                </div>
                <div class="comment-text">
                    <p>
                        <?php  echo $CommentsChild['Content']  ?>
                    </p>
                </div>
            </li>
            <?php  
    } ?>
        </ul>
    
    </li>
<?php  } ?>

<div id="INSERT"></div>
<script type="text/javascript">
    $(document).ready(function() {
        $(".ReplyButton").click(function() {
            if ($("#Reply" + $(this).attr('id')).attr('style') == "visibility: hidden;height:0px;")
            {
                $("#Reply" + $(this).attr('id')).attr('style', 'visibility: inherit;height:100%;padding:0px;');
            }
            else {
                $("#Reply" + $(this).attr('id')).attr('style', 'visibility: hidden;height:0px;');
                var Parent = $(this).attr('id');
                var Content = $("textarea[name='CommentChild" + Parent + "']").val();
                var Submitter = $("#User").val();
                var PostID = $("#PostID").val();
                $("#INSERT").load("InsertCom.php?Content=" + Content + "&Submitter=" + Submitter + "&Parent=" + Parent + "&id=" + PostID);
                $("textarea[name='CommentChild']").val("");
                //$("#LoadComments").empty();
                $("#LoadComments").load("Comments.php?id=" + PostID);
            }
        });
    });
</script>