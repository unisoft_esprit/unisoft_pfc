

            <div class="twelve columns omega recent-works-items">
                <hr class="vertical-space1">
                <div class="container" style="width: 100%;">
                    <div id="pin-content">
                        <?php
						$Category = $_GET['Category'];
                        include ("dbconnect.php");
$Query = $bdd->query("SELECT Recipes.*,Link,ImgUrl,Firstname,Lastname FROM `Recipes` inner join Attachments ON ID=PostID inner join Users ON Submitter=Email where Placement='Header' AND Category='$Category' ORDER BY Rating DESC LIMIT 6 ");
while ($Record = $Query->fetch()) {
                        ?>

                        <article class="pin-box entry web -item">
                            <div class="img-item">
                                <a href="<?php echo $Record['Link'] ?>" class="prettyPhoto">
                                    <img src="<?php echo $Record['Link'] ?>" alt="">
                                    <span class="zoomex">&nbsp;</span>
                                </a>
                            </div>
                            <div class="pin-ecxt">
                                <h4><a href="Recipe.php?id=<?php echo $Record['ID']; ?>"><?php echo $Record['Title']; ?></a></h4>
                                
                                <?php
                                if ($Record['Rating']<20)
                                   echo '<span class="icomoon-star" aria-hidden="true"></span>';

                                    
                                else {
                                 for ($i = 0;$i < $Record['Rating'];$i = $i + 20) {?>
                                <span class="icomoon-star-6" aria-hidden="true"></span>
                                <?php } }?>
                                <span style="float: right"> <span class="icomoon-heart-7" aria-hidden="true"></span> <small><?php echo $Record['HealthyRate'] ?>%</small> </span>
                            </div>
                            <div class="pin-ecxt2">
                                <img src="<?php echo $Record['ImgUrl'];?>" alt="">
                                <p>
                                    <small>Posted by</small>
                                    <?php echo $Record['Firstname']." ".$Record['Lastname'];?>
                                </p>
                            </div>
                        </article>
                        <?php
}
                        ?>
                    </div>
                    <!-- end-pin-content -->
                    <script src="js/jquery.masonry.min.js"></script>
                    <script src="js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
                    <hr class="vertical-space2">
                </div>
            </div>
