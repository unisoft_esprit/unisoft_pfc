<section class="container home-content" style="background-color: #34495e;width: 100%;padding-bottom: 5%;">
    <div class="row" style="margin-bottom: 0px;">
        <div class="container page-content ">
            <h4 class="subtitle" style="color:#E5E5E5">Top Tips</h4>
            <?php
include ("dbconnect.php");
$Query = $bdd->query("SELECT Tips.*,Link,CONCAT( SUBSTRING( Content, 1, 200 ) , ' ...' )  AS 'Description'  FROM `Tips` inner join Attachments ON ID=PostID where Placement='Header' ORDER BY Likes DESC LIMIT 6 ");
while ($Record = $Query->fetch()) {
    $Date = date_parse($Record['SubmissionDate']);
?>
            <div class="one-third column">
                <article class="tline-box " style="width: 90%;">
                    <div class="img-item">
                        <a class="prettyPhoto" href="<?php echo $Record['Link']; ?>"> <img src="<?php echo $Record['Link']; ?>" alt=""> <span class="zoomex"></span> </a>
                    </div>
                    <br>
                    <div class="tline-ecxt">
                        <h4><a href="Tip?id=<?php echo $Record['ID']; ?>"><?php echo $Record['Title']; ?></a>
                        </h4>
                        <h6 class="blog-author">
                        <span class="icomoon-eye-4" aria-hidden="true"></span><?php echo $Record['Views']; ?>
                        <span style="float: right"><?php echo $Record['Likes']; ?>
                        <span class="icomoon-thumbs-up-3" aria-hidden="true"></span><?php echo $Record['Dislikes']; ?>
                        <span class="icomoon-thumbs-up-4" aria-hidden="true"></span>
                        </span>
                        </h6>
                        </div>
                        <p>
                        <?php echo $Record['Description']; ?>
                        </p>
                        <div class="blog-date-sp">
                        <h3><?php echo $Date['day'] ?></h3>
                        <span>
                        <?php $month = $Date['month'];
							switch ($month) {
								case 1 :
									$month = "January";
									break;
								case 2 :
									$month = "February";
									break;
								case 3 :
									$month = "March";
									break;
								case 4 :
									$month = "April";
									break;
								case 5 :
									$month = "May";
									break;
								case 6 :
									$month = "June";
									break;
								case 7 :
									$month = "July";
									break;
								case 8 :
									$month = "August";
									break;
								case 9 :
									$month = "September";
									break;
								case 10 :
									$month = "October";
									break;
								case 11 :
									$month = "November";
									break;
								case 12 :
									$month = "December";
									break;
							}
							echo $month;
						?>
                        <br><?php echo $Date['year'] ?>
                        </span>
                        </div>
                        <div class="blog-com-sp">
                        <?php
						$Id = $Record['ID'];
						$Quer = $bdd -> query(" SELECT COUNT(Content) AS 'Count' FROM Comments where PostID='$Id' ");
						if ($Data = $Quer -> fetch())
							echo $Data['Count'];
					?> comments
                    </div>
                </article>
            </div>
            <?php
			}
 ?>
        </div>
    </div>
</section>