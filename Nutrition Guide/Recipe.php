<!DOCTYPE html>
<?php
include ("dbconnect.php");
$Error = "";
session_start();

if ($_SERVER["REQUEST_METHOD"] == "POST") {
if (!empty($_POST['Login'])){
    $username = $_POST['EmailLogin'];
    $password = $_POST['PassLogin'];

    $Query = $bdd -> query("SELECT Email,Password FROM Users where Email='$username' and Password='$password'");
    if ($Data = $Query -> fetch()) {
        if ($Data['Email'] == $username AND $Data['Password'] == $password) {
            $_SESSION['UserMail'] = $username;
            header("Location:" . $_SERVER['REQUEST_URI']);

        }
    } else {
        $Error = "Invalid Data";
    }
}}
?>
<html>
    <?php
    if (isset($_GET['id'])) {
        include ("dbconnect.php");
        $ID = $_GET['id'];
        $Record = $bdd->query("SELECT Recipes.*,Ingridents.*,Nutrients.* FROM `Recipes` left join Ingridents ON Ingridents.RecipeID=Recipes.ID left join Nutrients ON Nutrients.RecipeID=Recipes.ID where Recipes.ID='$ID' ");
        $Recipe = $Record->fetch();

        ?>
        <head>

            <!-- Basic Page Needs
                ================================================== -->
            <meta charset="utf-8">
            <title><?php echo $Recipe['Title'] ?> - Nutrition guide - Unisoft</title>
            <meta name="description" content="<?php echo $Recipe['Description'] ?>">
            <meta name="author" content="">

            <!-- Mobile Specific Metas
                ================================================== -->
            <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

            <!-- CSS
                ================================================== -->
            <link rel="stylesheet" href="css/style.css" type="text/css" media="all">
            <link rel="stylesheet" href="css/style-selector.css" type="text/css">
            <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,900|Roboto|Roboto+Slab:300,400' rel='stylesheet' type='text/css'>

            <!-- JS
                ================================================== -->
            <script type="text/javascript" src="js/jquery.min.js"></script>
            <!--[if lt IE 9]>
                <script src="js/modernizr.custom.11889.js" type="text/javascript"></script>
                <![endif]-->
            <!-- HTML5 Shiv events (end)-->
            <script type="text/javascript" src="js/nav-resp.js"></script>
            <script type="text/javascript" src="js/colorize.js"></script>
            <script>
                (function(i, s, o, g, r, a, m) {
                    i['GoogleAnalyticsObject'] = r;
                    i[r] = i[r] ||
                    function() {
                        (i[r].q = i[r].q || []).push(arguments);
                    }, i[r].l = 1 * new Date();
                    a = s.createElement(o), m = s.getElementsByTagName(o)[0];
                    a.async = 1;
                    a.src = g;
                    m.parentNode.insertBefore(a, m);
                })(window, document, 'script', '../../www.google-analytics.com/analytics.js', 'ga');

                ga('create', 'UA-43921752-1', 'webnus.net');
                ga('send', 'pageview');
            </script>
            <!-- Favicons
                ================================================== -->
            <link rel="shortcut icon" href="images/logo.png">
        </head>
        <body>

            <!-- Primary Page Layout
                ================================================== -->
            <div id="wrap" class="colorskin-0">
                <div class="top-bar">
                    <div class="container">
                        <div class="top-links">

                            <a href="AboutUS.php">About us</a> | <a href="Contact.php">Contact</a>|  <?php if( isset($_SESSION['UserMail'])){ ?>
                      <a>  <?php
                                include ("dbconnect.php");
                                $usermail = $_SESSION['UserMail'];
                                $Query = $bdd -> query("SELECT Firstname,Lastname FROM Users where Email='$usermail'");
                                $Record = $Query -> fetch();
                                echo $Record['Firstname'] . " " . $Record['Lastname'];
                            ?></a>
                    | <a id="Logout" href="Logout.php?link=<?php echo $_SERVER['REQUEST_URI']; ?>">Logout</a> 
                      <?php }else { ?>  
                     <a  href="Signup.php" >Sign up</a>
                     <?php } ?>
                        </div>
                        <div class="socailfollow">
                            <a href="#" class="facebook"> <i class="icomoon-facebook"> </i> 
                            </a>
                            <a href="#" class="dribble"> <i class="icomoon-dribbble"> </i> 
                            </a>
                            <a href="#" class="vimeo"> <i class="icomoon-vimeo"> </i> 
                            </a>
                            <a href="#" class="google"><i class="icomoon-google"> </i> </a>
                            <a href="#" class="twitter"><i class="icomoon-twitter"> </i> </a>
                        </div>
                    </div>
                </div>

                <header id="header">
                    <div class="container">
                        <div class="four columns logo">
                            <a href="index.php">
                                <img src="images/logo.png" width="70" height="60" id="img-logo" alt="logo">
                            </a>
                            <h4 class="subtitle" style="color:#9AA6AF ;display: inline;">Nutrition guide</h4>
                            
                        </div>
                        <nav id="nav-wrap" class="nav-wrap1 twelve columns">

                            <ul id="nav">
                                <li >
                                    <a href="index.php">Home</a>
                                </li>
                                <li>
                                    <a href="Recipes.php">Recpies</a>
                                    <ul>
                                        <li>
                                            <a href="Recipes.php?cat=Plates">Plates</a>                                        </li>
                                        <li>
                                            <a href="Recipes.php?cat=Salades">Salades</a>                                        </li>
                                        <li>
                                            <a href="Recipes.php?cat=Juices">Juices</a>                                        </li>
                                        <li>
                                            <a href="Recipes.php?cat=Cookies">Cookies</a>                                        </li>

                                    </ul>
                                </li>
                                <li>
                                    <a href="Tips.php">Tips</a>
                                    <ul>
                                        <li>
                                            <a href="Tips.php?cat=Recommandation">Recommandation</a>
                                        </li>
                                        <li>
                                            <a href="Tips.php?cat=Avoid">To avoid</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="Store.php">Store</a>
                                    <ul>
                                        <li>
                                            <a href="#">Organic Ingredients</a>
                                        </li>
                                        <li>
                                            <a href="#">Medecine</a>
                                        </li>
                                        <li>
                                            <a href="#">Equipments</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <?php
if (isset($_SESSION['UserMail'])) {
    ?>
                                        <?php
                                        $USEREMAIL = $_SESSION['UserMail'];
                                        $Query = $bdd -> query("SELECT ID FROM Users where Email = '$USEREMAIL'");
                                        $Record = $Query -> fetch();
                                        $ID = $Record['ID'];
    ?>

                                    <a href="User.php?id=<?php echo $ID; ?>">Profile</a>
    <?php
    } else {
    ?>
                                    <a href="#">Login</a>
                                    <ul style="width:180px;padding:5%;">
                                        <li>
                                            <form action="<?php echo htmlspecialchars($_SERVER["REQUEST_URI"]); ?>" method="post" style="width:150px;margin:5%;">
                                                <input type="email" name="EmailLogin" value="" placeholder="Email">
                                                <input type="password" name="PassLogin" value="" placeholder="Password">
                                                <div style="color:red; text-weight:300;"><?php echo $Error; ?></div>
                                                <br>
                                                <input type="submit" name="Login" style="background-color:#EF4836;width:150px;margin:2%;" value="Log in">
                                            </form>
                                        </li>

                                    </ul>
<?php } ?>

                                </li>
                            </ul>
                        </nav>
                        <!-- /nav-wrap -->
                    </div>

                </header>

                <section class="container page-content">
                    <hr class="vertical-space2">
                    <div class="eleven columns portfolio-item">
                        <h1><?php echo $Recipe['Title'] ?></h1>
                        <h3><?php echo $Recipe['Description'] ?></h3>
                                             
                    </div>
                    <hr class="vertical-space2">
                    <div class="sixteen columns">
                        <div class="flexslider" >
                            <ul class="slides">
                            <?php 
                        $ID=$Recipe['ID'];
                        $Record=$bdd->query("SELECT * FROM Attachments where PostID='$ID'");
                        while ($IMGS=$Record->fetch()) {
                           
                        ?>
                                <li style="height:500px;">
                                <?php 
                                if ($IMGS['Type']=="Image")
                                    {?>
                                    <img src="<?php echo $IMGS['Link']; ?>" alt="><?php echo $Recipe['Title'] ?>" style="height:500px;">
                                <?php
                                }
                                elseif ($IMGS['Type']=="Video")
                                {
                            ?>
                                <video src="<?php echo $IMGS['Link']; ?>"  style="height:500px;width:100%;" autobuffer autoloop loop controls ></video>
                                </li>
                                <?php
                                }
                                }
                                ?>
                                

                            </ul>
                        </div>

                    </div>
                        <hr class="vertical-space2">
                        <hr class="vertical-space2">

                    <ul id="myTab" class="nav nav-tabs">
                        <li class="active">
                            <a href="#Service1" data-toggle="tab">
                                <span class="  icomoon-meter-medium " aria-hidden="true" style="margin-right:5px;"></span>Health meter</a>
                        </li>
                        <li>
                            <a href="#Service2" data-toggle="tab">
                                <span class="icomoon-food-2" aria-hidden="true" style="margin-right:5px;"></span>Ingridents</a>
                        </li>
                        <li>
                            <a href="#Service3" data-toggle="tab">
                                <span class="icomoon-question-3" aria-hidden="true" style="margin-right:5px;"></span>How to cook</a>
                        </li>

                        <li>
                            <a href="#Service4" data-toggle="tab">
                                <span class="   icomoon-people " aria-hidden="true" style="margin-right:5px;"></span>Serving</a>
                        </li>

                        <li>
                            <a href="#Service5" data-toggle="tab">
                                <span class="icomoon-bubble-dots-4" aria-hidden="true" style="margin-right:5px;"></span>Reviews</a>
                        </li>
                    </ul>
                    <div id="myTabContent" class="tab-content">
                        <div class="tab-pane active" id="Service1">
                            <hr class="vertical-space1">
                            <div class="icon-box">
    <?php
    $Record = $bdd->query("SELECT * FROM Nutrients where RecipeID='$ID'");
    while ($Nutrients = $Record->fetch()) {
        ?>
                                    <div class="progress progress-info progress-striped" data-progress="<?php echo $Nutrients['Percentage']; ?>">
                                        <div class="bar">
                                    <?php echo $Nutrients['Name'] . " - " . $Nutrients['Quantity'] . " " . $Nutrients['Unit']; ?>
                                            <small><?php echo $Nutrients['Percentage']; ?>%</small>
                                        </div>
                                    </div>
    <?php } ?>  
                                <div class="progress progress-success progress-striped" data-progress="<?php echo $Recipe['HealthyRate']; ?>">
                                    <div class="bar">
                                        Healthy -
                                        <small><?php echo $Recipe['HealthyRate']; ?></small>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="tab-pane" id="Service2">
                            <hr class="vertical-space1">
                            <article class="icon-box">
                                <ul style="float:left;display :inline;">
    <?php
    $Record = $bdd->query("SELECT * FROM Ingridents where RecipeID='$ID'");
    while ($Ingridents = $Record->fetch()) {
        ?>
                                        <li class="circle">
                                        <?php echo $Ingridents['Name'] . " : " . $Ingridents['Quantity'] . " " . $Ingridents['Type'] ?>
                                        </li>
                                        <?php } ?>
                                </ul>

                            </article>
                        </div>
                        <div class="tab-pane " id="Service3" style="padding:2%">
                        <span class="  icomoon-clock "><strong>   Duration </strong></span><p> <?php echo $Recipe['Duration'] ?> Minutes</p>
                        <br>
                        <span class="    icomoon-notebook"><strong>   Methode </strong></span>
<p>    <?php echo $Recipe['Methode'] ?></p>
                        </div>
                        <div class="tab-pane" id="Service4">
   <p> <?php echo $Recipe['Serve'] ?></p>

                        </div>
                        <div class="tab-pane" id="Service5">
                            <hr class="vertical-space1">
                            <article class="icon-box">

                                <div class="jcarousel-container">
                                    <div class="jcarousel-clip">
                                    <?php 
$queryRev=$bdd->query("SELECT count(Title) AS 'Count' from Reviews where PostID='$ID'");
$Count=$queryRev->fetch();
if((int)$Count['Count'] != 0){
?>
                                        <ul id="latest-projects" class="jcarousel-list">
 <?php
$queryRev=$bdd->query("SELECT Reviews.* ,Users.* from Reviews inner join Users on User_Email=Email where PostID='$ID'");
while($Reviews=$queryRev->fetch())
{                 
?>                                           <li class="four columns jcarousel-item">

                                                <div class=" testimonial" >

                                                        <div class="testimonial-content" style="width: 250px;" >
                                                        <h4 style="margin-top: 0px;  padding-top: 0px;"><strong><?php echo $Reviews['Title'] ?></strong></h4>
                                                            <q><?php echo $Reviews['Text'] ?></q>
                                                            <div class="testimonial-arrow"></div>
                                                        </div>
                                                        <div class="testimonial-brand" style="width:100%;">
                                                            <img src="<?php echo $Reviews['ImgUrl'] ?>" alt=""><br>
                                                            <h6><strong><?php echo $Reviews['Firstname']." ".$Reviews['Lastname'] ?></strong></h6>

                                                            <?php 
                                                            if ($Reviews['Rate'] < 20)
                                                            { echo '<span class="icomoon-star" aria-hidden="true" style="font-size:150%"></span>';}
                                                          else {
                                                            for ($i = 0; $i < $Reviews['Rate']; $i = $i + 20) {
                                                                ?>

                                                                <span class="icomoon-star-6" aria-hidden="true" style="font-size:150%"></span>
                                                            <?php
                                                            }
                                                            }
                                                            ?>
                                                        </div>

                                                </div>
                                                <!-- end-portfolio-item-->
                                            </li>
                                            <?php } ?>
                                         </ul>
                                         <?php } ?>
                                    </div>
                                </div>
                            </article>
                        </div>

                    </div>
                    <?php if (isset($_SESSION['UserMail'])){
?>
                    <!-- end -->
                          <?php
$usermail=$_SESSION['UserMail'] ;
    ?>
                    <?php
                    if ($_SERVER['REQUEST_METHOD'] == "POST") {
                        $RevTitle = $_POST['RevTitle'];
                        $RevRate = (int)$_POST['RevRate'] * 20;
                        $RevText = $_POST['RevText'];

                        $bdd -> exec("INSERT INTO Reviews (Title,User_Email,Text,Rate,PostID) VALUES ('$RevTitle','$usermail','$RevText',$RevRate,'$ID');");
                        $Record = $bdd -> query("SELECT * FROM Reviews where PostID='$ID' ");
                        $counter = 0;
                        $RateSom = 0;
                        ;
                        while ($RevCount = $Record -> fetch()) {
                            $counter++;
                            $RateSom = $RateSom + $RevCount['Rate'];
                        }
                        if ($counter != 0) {
                            $RateSom = $RateSom / $counter;
                            $bdd -> exec("UPDATE Recipes SET Rating=$RateSom WHERE ID='$ID'  ");
                        }
            header("Location:" . $_SERVER['REQUEST_URI']);

                    }
                    ?>
                    <div class="clear"></div>
                    <hr class="vertical-space2">
                    <form action='<?php echo "Recipe.php?id=$ID"?>' method="post" >
                    <div id="Reviews" >
                            <div style="border-bottom:1px solid #E5E8EC;">
                             <span class="  icomoon-quotes-right-3 " style="font-size:150%;"></span><h3 style="display:inline;">
                                Submit Review
                            </h3 >
                                                        <hr class="vertical-space">

                            </div>
                            <hr class="vertical-space">
                            <input type="text" name="RevTitle" placeholder="Title" required>
                            <div id="Rate" style="font-size:150%;">
                                <input type="hidden" name="RevRate" value="0">
                                <a name="Rate1" id="1"><span class="  icomoon-star " id="RATE1"></span></a>
                                <a name="Rate2" id="2"><span class="  icomoon-star " id="RATE2"></span></a>
                                <a name="Rate3" id="3"><span class="  icomoon-star " id="RATE3"></span></a>
                                <a name="Rate4" id="4"><span class="  icomoon-star " id="RATE4"></span></a>
                                <a name="Rate5" id="5"><span class="  icomoon-star " id="RATE5"></span></a>
                            </div>
                            <script type="text/javascript">
                                $("a[name^='Rate']").hover(function() {
                                    var rate = $(this).attr('id');
                                    for (var i = 1; i < 6; i++) {
                                        if (i <= rate)
                                            $("#RATE" + i).attr('class', 'icomoon-star-3');
                                        else
                                            $("#RATE" + i).attr('class', 'icomoon-star');
                                    }
                                }, function() {
                                    var CurrentRate = $("input[name='RevRate']").val();
                                    for (var i = 1; i < 6; i++) {
                                        if (i <= CurrentRate) {
                                            $("#RATE" + i).attr('class', 'icomoon-star-3');
                                        } else {
                                            $("#RATE" + i).attr('class', 'icomoon-star');
                                        }
                                    }
                                });

                                $("a[name^='Rate']").click(function() {
                                    var rating = $(this).attr('id');
                                    $("input[name='RevRate']").val(rating);
                                    for (var i = 1; i < 6; i++) {
                                        if (i <= rating) {
                                            $("#RATE" + i).attr('class', 'icomoon-star-3');
                                        } else {
                                            $("#RATE" + i).attr('class', 'icomoon-star');
                                        }
                                    }
                                });

                            </script>
                            <br>
                            <textarea name="RevText" rows="8" style="width:400px;resize: none;" aria-required="true" placeholder="Description"></textarea> 
                            <br class="clear">
                            <input type="submit" id="submit" value="Post" class="green small" />
                    </div>
                    </form>
                    <div class="commentbox">
                    <input type="hidden" id="RecipeID" value="<?php echo $ID?>">
                     <span class="icomoon-bubbles-5" style="font-size:150%;"></span><h3 style="display:inline">
                                Comments
                            </h3>
                            <hr class="vertical-space">
                       
                        <div class="post-bottom-section">
                            <div class="right">
                                <ol class="commentlist">
                                <li id="NewComment">
                                    <span class="comment-info"> <img src="images/avatr1.png" width="40" height="40" alt=""> </span>
                                    <span class="comment-text">
                                        <textarea  name="Comment" placeholder="Write a comment ..." style="width:500px;resize: none;"></textarea>
                                        <div class="reply">
                                           <a id="Comment" class="comment-reply-link" style="margin-left:70px;">Post</a> 
                                        </div>
                                    </span>
                                </li>
                                <div id="INSERT"></div>
                                <input type="hidden" id="User" value="<?php  echo $_SESSION['UserMail']  ?>">

                                <script>
                                    $("#Comment").click(function() {
                                        var Content = $("textarea[name='Comment']").val();
                                        var Submitter = $("#User").val();
                                        var PostID = $("#RecipeID").val();
                                        $("#INSERT").load("InsertCom.php?Content=" + Content + "&Submitter=" + Submitter + "&Parent=0&id=" + PostID);
                                        $("textarea[name='Comment']").val("");
                                        //$("#LoadComments").empty();
                                        $("#LoadComments").load("Comments.php?id=" + $("#Recipeid").val());

                                    });
                                </script>
                    <!-- ********************************* -->                                 
                    <div id="LoadComments">
                    </div>
                    <!-- ********************************* -->                                 
                    <?php } ?>
                            </ol>
                            </div>
                        </div>
                      
                        <!-- #respond -->
                    </div>
                     <section class="related-works">
            <div class="container">
                <div class="sixteen columns">
                    <h4 class="subtitle">Related Works</h4>
                </div>
                <div class="clear"></div>
                <div class="jcarousel-container">
                    <div class="jcarousel-clip">
                        <ul id="latest-projects" >
                        <?php 
                        $Query=$bdd->query("SELECT * FROM Recipes where ID='$ID'");
                        $Record=$Query->fetch();
                        $Category=$Record['Category'];
$Query=$bdd->query("SELECT Recipes.*,Link,Placement FROM Recipes inner join Attachments on ID=PostID where ID!='$ID' AND Placement='Header' AND Category='$Category' ORDER BY RAND() LIMIT 4;");
while ($Record=$Query->fetch()) {
    ?>    
                            <li class="portfolio-item four columns jcarousel-item">
                                <a href="Recipe.php?id=<?php echo $Record['ID']; ?>">
                                    <img src="<?php echo $Record['Link']; ?>" alt="">
                                    <h5>
                                        <strong><?php echo $Record['Title']; ?></strong>
                                    </h5>
                                </a>
                                
                                   <?php
        
        if ($Record['Rating'] < 20) echo '<span class="icomoon-star" style="font-size:150%;" aria-hidden="true"></span>'; else {
            for ($i = 0; $i < $Record['Rating']; $i = $i + 20) {
                ?>
                                <span class="icomoon-star-6" style="font-size:150%;" aria-hidden="true"></span>
                            <?php
                            }}
 ?>
<span style="float: right"> <span class="icomoon-heart-7" style="font-size:150%;" aria-hidden="true"></span> <small><?php
 echo $Record['HealthyRate']   ?>%
                            </small> </span>
                                                            
                                <!-- end-portfolio-item-->
                            </li>
                                <?php } ?>                    
                            <!-- end-portfolio-item-->
                        </ul>
                    </div>
                </div>
            </div>
        </section>
                    <!-- Latest Projects - Start -->
                    <hr class="vertical-space1">
                </section>
               
                <!-- home-portfolio -->
                <section class="container">
                    <hr class="vertical-space2">
                    <!-- Latest Projects-end -->

                    <hr class="vertical-space2">
                </section>
                <!-- container -->

                <section class="blox dark nopad section-bg2" style="border-top:0px solid #e5e5e5;">
                    <div class="container aligncenter">
                        <hr class="vertical-space2">
                        <h5>Other Services By</h5>
                        <h1 class="mex-title">UNISOFT</h1>
                        <div class="sixteen columns">

                            <div class="one_third column">

                                <h4 class="subtitle" style="color: #C4C6C8;">Workout</h4>
                                <img src="images/Bodybuild.jpg" alt="Unisoft home page" class="Services_background" />

                            </div>
                            <div class="one_third column">
                                <h4 class="subtitle" style="color: #C4C6C8;">Aesthetics</h4>

                                <img src="images/Esthetic.jpg" alt="Unisoft home page" class="Services_background" />
                            </div>
                            <div class="one_third column">
                                <h4 class="subtitle" style="color: #C4C6C8;">wellness guide</h4>

                                <img src="images/Sexlife.jpg" alt="Unisoft home page" class="Services_background" />
                            </div>

                        </div>
                    </div>
                </section>

                <footer id="footer" style="border-top: 5px solid #252627;">
                    <section class="container footer-in">
                        <div class="one_third column contact-inf">
                            <h4 class="subtitle">Contact Information</h4>
                            <br />
                            <p>
                                <strong>Address:</strong>Esprit - EL Ghazela Ariana,Tunisia
                            </p>
                            <p>
                                <strong>Phone:</strong>+ 1 (234) 567 8901
                            </p>
                            <p>
                                <strong>Fax:</strong>+ 1 (234) 567 8901
                            </p>
                            <p>
                                <strong>Email:</strong>support@yoursite.com
                            </p>
                            <div>

                                <h4 class="subtitle">Stay Connected</h4>
                                <div class="socailfollow">
                                    <a href="#" class="facebook"><i class="icomoon-facebook"> </i></a>
                                    <a href="#" class="dribble"><i class="icomoon-dribbble"> </i></a>
                                    <a href="#" class="vimeo"><i class="icomoon-vimeo"> </i></a>
                                    <a href="#" class="google"><i class="icomoon-google"> </i></a>
                                    <a href="#" class="twitter"><i class="icomoon-twitter"> </i></a>
                                    <a href="#" class="youtube"><i class="icomoon-youtube"> </i></a>
                                </div>
                            </div>
                        </div>
                        <!-- end-contact-info /end -->

                        <!-- tweets  /end -->
                        <div class="two_third column">
                            <h4 class="subtitle">Location</h4>
                            <br />
                            <section class="full-width">
                                <div>
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d797.6600046132598!2d10.189218289020161!3d36.898959160825896!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12e2cb7454c6ed51%3A0x683b3ab5565cd357!2sESPRIT!5e0!3m2!1sen!2s!4v1395085887624" width=100% height="300" frameborder="0" style="border:0"></iframe>
                                </div>
                                <!-- END-contact Map -->
                            </section>
                            <!-- END-Google Map Section -->

                        </div>

                    </section>
                </footer>
                <!-- end-footer -->
                <span id="scroll-top"> <a class="scrollup"><i class="icomoon-arrow-up"> </i></a> 
                </span>
            </div>
            <!-- end-wrap -->

            <!-- End Document
                ================================================== -->
            <script type="text/javascript" src="js/jcarousel.js"></script>
            <script type="text/javascript" src="js/mexin-custom.js"></script>
            <script type="text/javascript" src="js/doubletaptogo.js"></script> 
            <script defer src="js/jquery.flexslider-min.js"></script>
            <script src="js/bootstrap-alert.js"></script>
            <script src="js/bootstrap-dropdown.js"></script>
            <script src="js/bootstrap-tab.js"></script>
            <script type="text/javascript" src="js/jquery.easy-pie-chart.js"></script>
            <script src="js/bootstrap-tooltip.js"></script>
            <script type="text/javascript">
                $(document).ready(function() {
                    $("#LoadComments").load("Comments.php?id=" + $("#RecipeID").val());
                    window.setInterval(function() {
                        //   $("#LoadComments").empty();
                        $("#LoadComments").load("Comments.php?id=" + $("#RecipeID").val())
                    }, 10000);
                });
            </script>
               
 

            
           
        </body>
    <?php
    }
    else
    {
    header("Location:Recipes.php") ;
    }
    ?>
        

    </body></html>
