<!DOCTYPE html>
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
    <!--<![endif]-->
    <head>
        <!-- Basic Page Needs
        ================================================== -->
        <meta charset="utf-8">
        <title>Add Recipe - Nutrition guide - Unisoft</title>
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Mobile Specific Metas
        ================================================== -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <!-- CSS
        ================================================== -->
        <link rel="stylesheet" href="css/style.css" type="text/css" media="all">
        <link rel="stylesheet" href="css/style-selector.css" type="text/css">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,900|Roboto|Roboto+Slab:300,400' rel='stylesheet' type='text/css'>
        <!-- JS
        ================================================== -->
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <!--[if lt IE 9]>
        <script src="js/modernizr.custom.11889.js" type="text/javascript"></script>
        <![endif]-->
        <!-- HTML5 Shiv events (end)-->
        <script type="text/javascript" src="js/nav-resp.js"></script>
        <script type="text/javascript" src="js/colorize.js"></script>
        <script type="text/javascript" src="js/Recipes.js"></script>
        <script>
            (function(i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] ||
                function() {
                    (i[r].q = i[r].q || []).push(arguments);
                }, i[r].l = 1 * new Date();
                a = s.createElement(o), m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m);
            })(window, document, 'script', '../../www.google-analytics.com/analytics.js', 'ga');
            ga('create', 'UA-43921752-1', 'webnus.net');
            ga('send', 'pageview');
        </script>
        <!-- Favicons
        ================================================== -->
        <link rel="shortcut icon" href="images/logo.png">
    </head>
    <body>
        <!-- Primary Page Layout
        ================================================== -->
        <?php
    include ("dbconnect.php");
    
    if ($_SERVER['REQUEST_METHOD'] == "POST") {
        $Title = $_POST['Title'];
        $Description = $_POST['Description'];
        $IMGCounter = (int)$_POST['IMGCounter'];
        $VIDCounter = (int)$_POST['VIDCounter'];
        $NUTCounter = (int)$_POST['NUTCounter'];
        $INGCounter = (int)$_POST['INGCounter'];
        $header = $_FILES["image0"]["name"];
        $Healthymeter = $_POST['Healthymeter'];
        $Duration = $_POST['Duration'];
        $Query=$bdd->query("SELECT RecipeCounter FROM `Recipes` ORDER BY RecipeCounter DESC LIMIT 1 ");
        $Data = $Query -> fetch();
        $ID = "R" . ($Data['RecipeCounter']+1);
        $Methode = $_POST['HowTo'];
        $Serve = $_POST['Serve'];
        $Category = $_POST['Category'];
        $Query = "INSERT INTO Recipes (ID,Title,Submitter,HealthyRate,Description,Methode,Serve,Duration,Category) VALUES ('$ID','$Title','haithem.sboui@esprit.tn','$Healthymeter','$Description','$Methode','$Serve',$Duration,'$Category')";
        $bdd -> exec($Query);
        $hederimg=$ID."0";
        $Query = "INSERT INTO Attachments (Link,PostID,Type,Placement) VALUES ('Uploads/$hederimg.png','$ID','Image','Header'); ";
        $bdd -> exec($Query);
        move_uploaded_file($_FILES["image0"]["tmp_name"],"Uploads/".$ID."0".".png");
        for ($i = 1; $i < $IMGCounter + 1; $i++) {
            $image = $_FILES["image$i"]["name"];
            
            if ($image=="")                    break;
            move_uploaded_file($_FILES["image$i"]["tmp_name"],"Uploads/".$ID.$i.".png");
            $Query = "INSERT INTO Attachments (Link,PostID,Type,Placement) VALUES ('Uploads/$ID$i.png','$ID','Image','Content'); ";
            $bdd -> exec($Query);
        }

        for ($i = 1; $i < $VIDCounter + 1; $i++) {
            $video = $_FILES["video$i"]["name"];
            
            if ($video=="")                    break;
            move_uploaded_file($_FILES["video$i"]["tmp_name"],"Uploads/".$ID.$i.".mp4");
            $Query = "INSERT INTO Attachments (Link,PostID,Type,Placement) VALUES ('Uploads/$ID$i.mp4','$ID','Video','Content'); ";
            $bdd -> exec($Query);
        }

        for ($i = 1; $i < $INGCounter + 1; $i++) {
            
            if ($name="" AND $type="" AND $quantity="")                    break;
            $name = $_POST["Ingname$i"];
            $type = $_POST["Ingtype$i"];
            $quantity = $_POST["Ingquantity$i"];
            $Query = "INSERT INTO Ingridents (Name,RecipeID,Quantity,Type) VALUES ('$name','$ID','$quantity','$type'); ";
            $bdd -> exec($Query);
        }

        for ($i = 1; $i < $NUTCounter + 1; $i++) {
            $name = $_POST["Nutname$i"];
            $unit = $_POST["Nutunit$i"];
            $quantity = $_POST["Nutquantity$i"];
            $Percentage= $_POST["Percentage$i"];
            
            if ($name="" AND $unit="" AND $quantity="" AND $Percentage="")                    break;
            $Query = "INSERT INTO Nutrients (Name,RecipeID,Unit,Quantity,Percentage) VALUES ('$name','$ID','$unit','$quantity','$Percentage'); ";
            $bdd -> exec($Query);
        }
        $TagCount=$_POST['TagCount'];
        for ($i=1;$i<$TagCount+1;$i++)
        {
            $Tagname=$_POST["tag$i"];
            $Query = "INSERT INTO Tags (Name,PostID,Section) VALUES ('$Tagname','$ID','Recipes'); ";
            $bdd -> exec($Query);
        }
        header('Location:Recipes.php');
        exit();
    }

    ?>
        <div id="wrap" class="colorskin-0">
            <div class="top-bar">
                <div class="container">
                    <div class="top-links">
                        <a href="AboutUS.php">About us</a> | <a href="Contact.php">Contact</a>|  <?php session_start();if( isset($_SESSION['UserMail'])){ ?>
                      <a>  <?php include ("dbconnect.php");$usermail=$_SESSION['UserMail'];
                            $Query=$bdd->query("SELECT Firstname,Lastname FROM Users where Email='$usermail'");
                             $Record=$Query->fetch();
                             echo $Record['Firstname']." ".$Record['Lastname'];?></a>
                    | <a id="Logout" href="Logout.php?link=<?php echo $_SERVER['REQUEST_URI']; ?>">Logout</a> 
                      <?php }else { ?>  
                     <a  href="Signup.php" >Sign up</a>
                     <?php }?>
                    </div>
                    <div class="socailfollow">
                        <a href="#" class="facebook"> <i class="icomoon-facebook"> </i> </a>
                        <a href="#" class="dribble"> <i class="icomoon-dribbble"> </i> </a>
                        <a href="#" class="vimeo"> <i class="icomoon-vimeo"> </i> </a>
                        <a href="#" class="google"><i class="icomoon-google"> </i> </a>
                        <a href="#" class="twitter"><i class="icomoon-twitter"> </i> </a>
                    </div>
                </div>
            </div>
            <header id="header">
                <div class="container">
                    <div class="four columns logo">
                        <a href="index.php"> <img src="images/logo.png" width="70" height="60" id="img-logo" alt="logo"> </a>
                        <h4 class="subtitle" style="color:#9AA6AF ;display: inline;">Nutrition guide</h4>
                    </div>
                    <nav id="nav-wrap" class="nav-wrap1 twelve columns">
                        <ul id="nav">
                            <li class="current">
                                <a href="index.php">Home</a>
                            </li>
                            <li>
                                <a href="Recipes.php">Recpies</a>
                                <ul>
                                    <li>
                                        <a href="Recipes.php?cat=Plates">Plates</a>                                    </li>
                                    <li>
                                        <a href="Recipes.php?cat=Salades">Salades</a>                                    </li>
                                    <li>
                                        <a href="Recipes.php?cat=Juices">Juices</a>                                    </li>
                                    <li>
                                        <a href="Recipes.php?cat=Cookies">Cookies</a>                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="Tips.php">Tips</a>
                                <ul>
                                    <li>
                                        <a href="Tips.php?cat=Recommandation">Recommandation</a>
                                    </li>
                                    <li>
                                        <a href="Tips.php?cat=Avoid">To avoid</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="Store.php">Store</a>
                                <ul>
                                    <li>
                                        <a href="#">Organic Ingredients</a>
                                    </li>
                                    <li>
                                        <a href="#">Medecine</a>
                                    </li>
                                    <li>
                                        <a href="#">Equipments</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <?php
if (isset($_SESSION['UserMail'])) {
    ?>
                                        <?php
    $USEREMAIL=$_SESSION['UserMail'];
     $Query=$bdd->query("SELECT ID,Type FROM Users where Email = '$USEREMAIL'");
    $Record=$Query->fetch();
    $ID=$Record['ID'];
    $Type=$Record['Type'];
    if ($Type=="User"){
    ?>

                                    <a href="User.php?id=<?php echo $ID ;?>">Profile</a>
    <?php
    }
    elseif ($Type=="Admin") {
?>
                                    <a href="Admin.php">Administration</a>

<?php
        }
} else {
    ?>
                                    <a href="#">Login</a>
                                    <ul style="width:180px;padding:5%;">
                                        <li>
                                            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post" style="width:150px;margin:5%;">
                                                <input type="email" name="EmailLogin" value="" placeholder="Email">
                                                <input type="password" name="PassLogin" value="" placeholder="Password">
                                                <div style="color:red; text-weight:300;"><?php echo $Error; ?></div>
                                                <br>
                                                <input type="submit" style="background-color:#EF4836;width:150px;margin:2%;" value="Log in">
                                            </form>
                                        </li>

                                    </ul>
<?php } ?>
                            </li>
                        </ul>
                    </nav>
                    <!-- /nav-wrap -->
                </div>
            </header>
            <form method="post" action="<?php  htmlspecialchars($_SERVER['PHP_SELF']); ?>" enctype="multipart/form-data">
                <section class="container page-content">
                    <hr class="vertical-space2">
                    <div class="eleven columns portfolio-item">
                        <h3 style="display : inline-block">Header image</h3>
                        <br>
                        <input   type="file" accept="image/*" name="image0" />
                        <hr class="vertical-space">
                        <input  type="text" name="Title" value="" placeholder="Title"  required >
                        <div>
                            <input type="radio" name="Category" value="Plates" checked style="display:inline;">
                            <h6 style="display:inline;">Plate</h6>
                            <input type="radio" name="Category" value="Salades" style="display:inline;">
                            <h6 style="display:inline;">Salade</h6>
                            <input type="radio" name="Category" value="Juices" style="display:inline;">
                            <h6 style="display:inline;">Juice</h6>
                            <input type="radio" name="Category" value="Cookies" style="display:inline;">
                            <h6 style="display:inline;">Cookie</h6>
                        </div>
                        <hr class="vertical-space">
                        <input style="width:100%" type="text" name="Description" value="" placeholder="Desciption" required >
                        <h3 style="display : inline-block">Tags</h3>
                        <a style="margin-left:15px;display : inline-block" id="TagAdd"> <span class="icomoon-plus "></span> </a>
                        <div id="Tags" >
                        <input type="hidden" value="0" name="TagCount" id="TagCount">
                    </div>
                    <script>
                    var i=1;
                    $("#TagAdd").click(function() {
                        $("#Tags").append('<a class="tagcloud" ><input name="tag'+i+'" style="display:inline-block;width: 70px; margin-bottom: 0px; height: 27px; padding-bottom: 0px; padding-top: 0px;" type="text" style="width:50px;"></a>');
                        $("#TagCount").val(i);
                        i++;
                    });
                    </script>
                    </div>
                    <hr class="vertical-space2">
                    <div class="sixteen columns">
                        <div id="UploadIMG" class="one_half column">
                            <h3 style="display : inline-block">Content images</h3>
                            <a style="margin-left:15px;display : inline-block" id="ImgUpl"> <span class="icomoon-plus "></span> </a>
                            <br>
                            <input type="hidden" id="IMGCounter"  name="IMGCounter" value="0">
                        </div>
                        <div id="UploadVID" class="one_half column">
                            <h3 style="display : inline-block">Content Videos</h3>
                            <a style="margin-left:15px;display : inline-block" id="VidUpl"> <span class="icomoon-plus "></span> </a>
                            <br>
                            <input type="hidden" id="VIDCounter" name="VIDCounter" value="0">
                        </div>
                    </div>
                    <ul id="myTab" class="nav nav-tabs">
                        <li class="active">
                            <a href="#Service1" data-toggle="tab"> <span class="  icomoon-meter-medium " aria-hidden="true" style="margin-right:5px;"></span>Health meter</a>
                        </li>
                        <li>
                            <a href="#Service2" data-toggle="tab"> <span class="icomoon-food-2" aria-hidden="true" style="margin-right:5px;"></span>Ingridents</a>
                        </li>
                        <li>
                            <a href="#Service3" data-toggle="tab"> <span class="icomoon-question-3" aria-hidden="true" style="margin-right:5px;"></span>How to cook</a>
                        </li>
                        <li>
                            <a href="#Service4" data-toggle="tab"> <span class="   icomoon-people " aria-hidden="true" style="margin-right:5px;"></span>Serving</a>
                        </li>
                    </ul>
                    <div id="myTabContent" class="tab-content">
                        <div class="tab-pane active" id="Service1">
                            <hr class="vertical-space1">
                            <div id="NutrientsList">
                                <a style="margin-Bottom:15px;display : inline-block" id="NutAdd"> <span class="icomoon-plus "> Add Nutrient</span> </a>
                                <br>
                                <input type="hidden" id="NUTCounter" name="NUTCounter" value="0">
                                <input type="number" id="Healthymeter" name="Healthymeter" placeholder="Healthymeter">
                            </div>
                        </div>
                        <div class="tab-pane" id="Service2">
                            <hr class="vertical-space1">
                            <article class="icon-box">
                                <div id="IngridentsList">
                                    <a style="margin-Bottom:15px;display : inline-block" id="IngAdd"> <span class="icomoon-plus "> Add Ingridents</span> </a>
                                    <br>
                                    <input type="hidden" id="INGCounter" name="INGCounter" value="0">
                                </div>
                            </article>
                        </div>
                        <div class="tab-pane " id="Service3">
                            <hr class="vertical-space1">
                            <article class="icon-box">
                                <article class="icon-box">
                                    <input type="number" placeholder="Duration" name="Duration">
                                    <textarea style="width :100%;height:200px;" name="HowTo" Required></textarea>
                                </article>
                            </article>
                        </div>
                        <div class="tab-pane" id="Service4">
                            <hr class="vertical-space1">
                            <article class="icon-box">
                                <textarea style="width :100%;height:200px;" name="Serve" Required></textarea>
                            </article>
                        </div>
                    </div>
                    <!-- end -->
                    <div class="clear"></div>
                    <hr class="vertical-space1">
                    <button type="submit" style="float:right;" class="blue" >
                        <span class="icomoon-upload-7" > Accept</span>
                    </button>
                    <!-- Latest Projects - Start -->
                    <hr class="vertical-space1">
                </section>
            </form>
            <!-- home-portfolio -->
            <section class="container">
                <hr class="vertical-space2">
                <!-- Latest Projects-end -->
                <hr class="vertical-space2">
            </section>
            <!-- container -->
            <!-- end-wrap -->
            <!-- End Document
            ================================================== -->
            <script type="text/javascript" src="js/jcarousel.js"></script>
            <script type="text/javascript" src="js/mexin-custom.js"></script>
            <script type="text/javascript" src="js/doubletaptogo.js"></script>
            <!-- FlexSlider -->
            <script defer src="js/jquery.flexslider-min.js"></script>
            <script src="js/bootstrap-alert.js"></script>
            <script src="js/bootstrap-dropdown.js"></script>
            <script src="js/bootstrap-tab.js"></script>
            <script type="text/javascript" src="js/jquery.easy-pie-chart.js"></script>
            <script src="js/bootstrap-tooltip.js"></script>
    </body>
</html>