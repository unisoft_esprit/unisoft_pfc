<?php
include ("dbconnect.php");
$ID = $_GET['id'];
if ($_SERVER['REQUEST_METHOD'] == "GET") {
	$Record = $bdd -> query("SELECT * FROM Recipes where ID='$ID'");
	$Recipe = $Record -> fetch();
	$Record = $bdd -> query("SELECT Count(PostID) AS 'Count' FROM Attachments where PostID='$ID' AND Type='Image'  AND Placement='Content'");
	$AttachmentsIMG = $Record -> fetch();
        $Countimg=$AttachmentsIMG['Count'];

	$Record = $bdd -> query("SELECT Count(PostID) AS 'Count' FROM Attachments where PostID='$ID' AND Type='Video'");
	$AttachmentsVID = $Record -> fetch();
        $Countvid=$AttachmentsVID['Count'];

	$Record = $bdd -> query("SELECT Count(RecipeID) AS 'Count' FROM Ingridents where RecipeID='$ID'");
	$Ingridents = $Record -> fetch();
	$Record = $bdd -> query("SELECT Count(RecipeID) AS 'Count' FROM Nutrients where RecipeID='$ID'");
	$Nutrients = $Record -> fetch();
	$Title = $Recipe['Title'];
	$Description = $Recipe['Description'];
	
	$Healthymeter = $Recipe['HealthyRate'];
	$Duration = $Recipe['Duration'];
	$Query = $bdd -> query("SELECT * FROM Attachments WHERE PostID='$ID' AND Placement='Header'");
	$headerIMG = $Query -> fetch();
	$header = $headerIMG['Link'];
	$Methode = $Recipe['Methode'];
	$Serve = $Recipe['Serve'];
	$Category = $Recipe['Category'];
	$IMGCounter = $Countimg;
	$VIDCounter = $Countvid;

	$NUTCounter = $Nutrients['Count'];
	$INGCounter = $Ingridents['Count'];
} elseif ($_SERVER['REQUEST_METHOD'] == "POST") {
    $Record = $bdd -> query("SELECT * FROM Recipes where ID='$ID'");
    $Recipe = $Record -> fetch();
	$Title = $_POST['Title'];
	$Description = $_POST['Description'];
	$header = $_FILES["image0"]["name"];
	$Healthymeter = $_POST['Healthymeter'];
	$Duration = $_POST['Duration'];
	$Methode = $_POST['HowTo'];
	$Serve = $_POST['Serve'];
	$Category = $_POST['Category'];

    $Record = $bdd -> query("SELECT Count(PostID) AS 'Count' FROM Attachments where PostID='$ID' AND Type='Image' ");
    $AttachmentsIMG = $Record -> fetch();
    $Countimg=$AttachmentsIMG['Count'];

    $Record = $bdd -> query("SELECT Count(PostID) AS 'Count' FROM Attachments where PostID='$ID' AND Type='Video'");
    $AttachmentsVID = $Record -> fetch();
    $Countvid=$AttachmentsVID['Count'];
    $Record = $bdd -> query("SELECT Count(RecipeID) AS 'Count' FROM Ingridents where RecipeID='$ID'");
    $Ingridents = $Record -> fetch();
    $Record = $bdd -> query("SELECT Count(RecipeID) AS 'Count' FROM Nutrients where RecipeID='$ID'");
    $Nutrients = $Record -> fetch();

    $countnut = $Nutrients['Count'];
    $counting = $Ingridents['Count'];

	$IMGCounter = (int)$_POST['IMGCounter'];
	$VIDCounter = (int)$_POST['VIDCounter'];
	$NUTCounter = (int)$_POST['NUTCounter'];
	$INGCounter = (int)$_POST['INGCounter'];

	$Query = "UPDATE Recipes SET Title='$Title',HealthyRate='$Healthymeter',Description='$Description',Methode='$Methode',Serve='$Serve',Duration='$Duration',Category='$Category' where ID='$ID'";
	$bdd -> exec($Query);
	$hederimg = $ID . "0";
	move_uploaded_file($_FILES["image0"]["tmp_name"], "Uploads/" . $ID . "0" . ".png");
	for ($i = $Countimg; $i < $IMGCounter + 1; $i++) {
		$image = $_FILES["image$i"]["name"];
		move_uploaded_file($_FILES["image$i"]["tmp_name"], "Uploads/" . $ID . $i . ".png");
		$Query = "INSERT INTO Attachments (Link,PostID,Type,Placement) VALUES ('Uploads/$ID$i.png','$ID','Image','Content'); ";
		$bdd -> exec($Query);
	}
	for ($i = $Countvid+1; $i < $VIDCounter + 1; $i++) {
		$video = $_FILES["video$i"]["name"];
		move_uploaded_file($_FILES["video$i"]["tmp_name"], "Uploads/" . $ID . $i . ".mp4");
		$Query = "INSERT INTO Attachments (Link,PostID,Type,Placement) VALUES ('Uploads/$ID$i.mp4','$ID','Video','Content'); ";
		$bdd -> exec($Query);
	}
	for ($i = $counting+1; $i < $INGCounter + 1; $i++) {
		$name = $_POST["Ingname$i"];
		$type = $_POST["Ingtype$i"];
		$quantity = $_POST["Ingquantity$i"];
		if (isset($_POST["Ing$i"])) {
			$IngID = $_POST["Ing$i"];
			$Query = "UPDATE Ingridents SET Name='$name',RecipeID='$ID',Type='$type',Quantity='$quantity' WHERE IngridentID='$IngID'";
			$bdd -> exec($Query);
		} else {
			$Query = "INSERT INTO Ingridents (Name,RecipeID,Quantity,Type) VALUES ('$name','$ID','$quantity','$type'); ";
			$bdd -> exec($Query);
		}

	}
	for ($i = $countnut+1; $i < $NUTCounter + 1; $i++) {
		$name = $_POST["Nutname$i"];
		$unit = $_POST["Nutunit$i"];
		$quantity = $_POST["Nutquantity$i"];
		$Percentage = $_POST["Percentage$i"];
		if (isset($_POST["Nut$i"])) {
			$NutID = $_POST["Nut$i"];
			$Query = "UPDATE Nutrients SET Name='$name',RecipeID='$ID',Unit='$unit',Quantity='$quantity',Percentage='$Percentage' WHERE NutrientID='$NutID'  ";
			$bdd -> exec($Query);
		} else {
			$Query = "INSERT INTO Nutrients (Name,RecipeID,Unit,Quantity,Percentage) VALUES ('$name','$ID','$unit','$quantity','$Percentage')  ";
			$bdd -> exec($Query);
		}
	}
	//header('Location:Recipes.php');
}
?>
<!DOCTYPE html>
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
    <!--<![endif]-->
    <head>
        <!-- Basic Page Needs
        ================================================== -->
        <meta charset="utf-8">
        <title>Edit <?php echo $Title?> - Nutrition guide - Unisoft</title>
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Mobile Specific Metas
        ================================================== -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <!-- CSS
        ================================================== -->
        <link rel="stylesheet" href="css/style.css" type="text/css" media="all">
        <link rel="stylesheet" href="css/style-selector.css" type="text/css">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,900|Roboto|Roboto+Slab:300,400' rel='stylesheet' type='text/css'>
        <!-- JS
        ================================================== -->
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <!--[if lt IE 9]>
        <script src="js/modernizr.custom.11889.js" type="text/javascript"></script>
        <![endif]-->
        <!-- HTML5 Shiv events (end)-->
        <script type="text/javascript" src="js/nav-resp.js"></script>
        <script type="text/javascript" src="js/colorize.js"></script>
        <script type="text/javascript" src="js/Recipes.js"></script>
        <!-- Favicons
        ================================================== -->
        <link rel="shortcut icon" href="images/logo.png">
    </head>
    <body>
        <!-- Primary Page Layout
        ================================================== -->
        <div id="wrap" class="colorskin-0">
            <div class="top-bar">
                <div class="container">
                    <div class="top-links">
                        <a href="AboutUS.php">About us</a> | <a href="Contact.php">Contact</a>|  <?php session_start();if( isset($_SESSION['UserMail'])){ ?>
                      <a>  <?php include ("dbconnect.php");$usermail=$_SESSION['UserMail'];
                            $Query=$bdd->query("SELECT Firstname,Lastname FROM Users where Email='$usermail'");
                             $Record=$Query->fetch();
                             echo $Record['Firstname']." ".$Record['Lastname'];?></a>
                    | <a id="Logout" href="Logout.php?link=<?php echo $_SERVER['REQUEST_URI']; ?>">Logout</a> 
                      <?php }else { ?>  
                     <a  href="Signup.php" >Sign up</a>
                     <?php }?>
                    </div>
                    <div class="socailfollow">
                        <a href="#" class="facebook"> <i class="icomoon-facebook"> </i> </a>
                        <a href="#" class="dribble"> <i class="icomoon-dribbble"> </i> </a>
                        <a href="#" class="vimeo"> <i class="icomoon-vimeo"> </i> </a>
                        <a href="#" class="google"><i class="icomoon-google"> </i> </a>
                        <a href="#" class="twitter"><i class="icomoon-twitter"> </i> </a>
                    </div>
                </div>
            </div>
            <header id="header">
                <div class="container">
                    <div class="four columns logo">
                        <a href="index.php"> <img src="images/logo.png" width="70" height="60" id="img-logo" alt="logo"> </a>
                        <h4 class="subtitle" style="color:#9AA6AF ;display: inline;">Nutrition guide</h4>
                    </div>
                    <nav id="nav-wrap" class="nav-wrap1 twelve columns">
                        <ul id="nav">
                            <li class="current">
                                <a href="index.php">Home</a>
                            </li>
                            <li>
                                <a href="Recipes.php">Recpies</a>
                                <ul>
                                    <li>
                                        <a href="Recipes.php?cat=Plates">Plates</a>                                    </li>
                                    <li>
                                        <a href="Recipes.php?cat=Salades">Salades</a>                                    </li>
                                    <li>
                                        <a href="Recipes.php?cat=Juices">Juices</a>                                    </li>
                                    <li>
                                        <a href="Recipes.php?cat=Cookies">Cookies</a>                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="Tips.php">Tips</a>
                                <ul>
                                    <li>
                                        <a href="Tips.php?cat=Recommandation">Recommandation</a>
                                    </li>
                                    <li>
                                        <a href="Tips.php?cat=Avoid">To avoid</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="Store.php">Store</a>
                                <ul>
                                    <li>
                                        <a href="#">Organic Ingredients</a>
                                    </li>
                                    <li>
                                        <a href="#">Medecine</a>
                                    </li>
                                    <li>
                                        <a href="#">Equipments</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <?php
if (isset($_SESSION['UserMail'])) {
    ?>
                                        <?php
    $USEREMAIL=$_SESSION['UserMail'];
   $Query=$bdd->query("SELECT ID,Type FROM Users where Email = '$USEREMAIL'");
    $Record=$Query->fetch();
    $ID=$Record['ID'];
    $Type=$Record['Type'];
    if ($Type=="User"){
    ?>

                                    <a href="User.php?id=<?php echo $ID ;?>">Profile</a>
    <?php
    }
    elseif ($Type=="Admin") {
?>
                                    <a href="Admin.php">Administration</a>

<?php
        }
} else {
    ?>
                                    <a href="#">Login</a>
                                    <ul style="width:180px;padding:5%;">
                                        <li>
                                            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post" style="width:150px;margin:5%;">
                                                <input type="email" name="EmailLogin" value="" placeholder="Email">
                                                <input type="password" name="PassLogin" value="" placeholder="Password">
                                                <div style="color:red; text-weight:300;"><?php echo $Error; ?></div>
                                                <br>
                                                <input type="submit" style="background-color:#EF4836;width:150px;margin:2%;" value="Log in">
                                            </form>
                                        </li>

                                    </ul>
<?php } ?>
                            </li>
                        </ul>
                    </nav>
                    <!-- /nav-wrap -->
                </div>
            </header>
            <form method="POST" action="<?php htmlspecialchars($_SERVER['PHP_SELF']); ?>" enctype="multipart/form-data" name="Recipe">
                <input type="hidden" id="PostID" value="<?php echo $ID; ?>" placeholder="">
                <section class="container page-content">
                    <hr class="vertical-space2">
                    <div class="sixteen columns ">
                        <div class="one_third column">
                            <h3 style="display : inline-block;" class="mex-title"><span class="  icomoon-image "></span> Header images</h3>
                            <br>
                            <img id="HeadImg" src="<?php echo $header; ?>" width="256" height="256">
                            <br>
                            <input   type="file" accept="image/*" name="image0" id="Imginp" value="<?php echo $header; ?>"/>
                        </div>
                        <div class="two_third column">
                            <h3 style="display : inline-block;" class="mex-title"><span class=" icomoon-pencil-5 "></span> Basic Informations</h3>
                            <br>
                            <input  type="text" name="Title" value="<?php echo $Title; ?>" placeholder="Title"  required >
                            <input type="hidden" value="<?php echo $Category; ?>" id="Category">
                            <input type="radio" name="Category" value="Plates" id="Plates" style="display:inline;">
                            <h6 style="display:inline;">Plate</h6>
                            <input type="radio" name="Category" value="Salades" id="Salades" style="display:inline;">
                            <h6 style="display:inline;">Salade</h6>
                            <input type="radio" name="Category" value="Juices" id="Juices" style="display:inline;">
                            <h6 style="display:inline;">Juice</h6>
                            <input type="radio" name="Category" value="Cookies" id="Cookies" style="display:inline;">
                            <h6 style="display:inline;">Cookie</h6>

                            <hr class="vertical-space">
                            <input style="width:100%" type="text" name="Description" value="<?php echo $Description?>" placeholder="Desciption" required >
                        </div>
                    </div>
                    <hr class="vertical-space2">
                    <div class="sixteen columns">
                        <div id="CurrentIMG" class="two_third column"></div>
                        <div id="UploadIMG" class="one_third column">
                            <h3 style="display : inline-block" class="mex-title"><span class="icomoon-upload"> </span>Import images</h3>
                            <a style="margin-left:15px;display : inline-block" id="ImgUpl"> <span class="icomoon-plus "></span> </a>
                            <br>
                            <input type="hidden" id="IMGCounter"  name="IMGCounter" value="<?php echo $IMGCounter; ?>">
                        </div>
                        <hr class="vertical-space">
                        <div id="CurrentVID" class="two_third column">

                        </div>
                        <div id="UploadVID" class="one_third column">
                            <h3 style="display : inline-block" class="mex-title"><span class="icomoon-upload"> </span>Import videos</h3>
                            <a style="margin-left:15px;display : inline-block" id="VidUpl"> <span class="icomoon-plus "></span> </a>
                            <br>
                            <input type="hidden" id="VIDCounter" name="VIDCounter" value="<?php echo $VIDCounter; ?>">
                        </div>
                    </div>
                    <hr class="vertical-space2">
                    <ul id="myTab" class="nav nav-tabs">
                        <li class="active">
                            <a href="#Service1" data-toggle="tab"> <span class="  icomoon-meter-medium " aria-hidden="true" style="margin-right:5px;"></span>Health meter</a>
                        </li>
                        <li>
                            <a href="#Service2" data-toggle="tab"> <span class="icomoon-food-2" aria-hidden="true" style="margin-right:5px;"></span>Ingridents</a>
                        </li>
                        <li>
                            <a href="#Service3" data-toggle="tab"> <span class="icomoon-question-3" aria-hidden="true" style="margin-right:5px;"></span>How to cook</a>
                        </li>
                        <li>
                            <a href="#Service4" data-toggle="tab"> <span class="   icomoon-people " aria-hidden="true" style="margin-right:5px;"></span>Serving</a>
                        </li>
                    </ul>
                    <div id="myTabContent" class="tab-content">
                        <div class="tab-pane active" id="Service1">
                            <hr class="vertical-space1">
                            <div id="NutrientsList">
                                <a style="margin-Bottom:15px;display : inline-block" id="NutAdd"> <span class="icomoon-plus "> Add Nutrient</span> </a>
                                <br>
                                <input type="hidden" id="NUTCounter" name="NUTCounter" value="<?php echo $NUTCounter?>">
                                <input type="number" id="Healthymeter" value="<?php echo $Healthymeter?>" name="Healthymeter" placeholder="Healthymeter">
                                <?php
                                $Record = $bdd -> query("SELECT * FROM Nutrients where RecipeID='$ID'");
                                $i=1;
                                while($Nutrients = $Record -> fetch())
                                {
                                ?>
                                <div >
                                    <input type="hidden" name="Nut<?php echo $i ?>" value="<?php echo $Nutrients['ID']; ?>">
                                    <input required type="text" name="Nutname<?php echo $i?>" value="<?php echo $Nutrients['Name'] ?>" placeholder="Name" style="width:25%;display:inline;">
                                    <input required type="text" name="Percentage<?php echo $i?>" value="<?php echo $Nutrients['Percentage'] ?>" placeholder="Percentage" style="width:20%;display:inline;">
                                    <input type="text" name="Nutunit<?php echo $i?>" value="<?php echo $Nutrients['Unit'] ?>" placeholder="Unit" style="width:30%;display:inline;">
                                    <input type="text" name="Nutquantity<?php echo $i?>" value="<?php echo $Nutrients['Quantity'] ?>" placeholder="Quantity" style="width:20%;display:inline;">
                                </div>
                                <?php
								$i++;
								}
                                ?>
                            </div>
                        </div>
                        <div class="tab-pane" id="Service2">
                            <hr class="vertical-space1">
                            <article class="icon-box">
                                <div id="IngridentsList">
                                    <a style="margin-Bottom:15px;display : inline-block" id="IngAdd"> <span class="icomoon-plus "> Add Ingridents</span> </a>
                                    <br>
                                    <input type="hidden" id="INGCounter" name="INGCounter" value="<?php echo $INGCounter?>">

                                    <?php
                                    $Record = $bdd -> query("SELECT * FROM Ingridents where RecipeID='$ID'");
                                    $i=1;
                                    while($Ingridents = $Record -> fetch())
                                    {
                                    ?>
                                    <div >
                                        <input type="hidden" name="Ing<?php echo $i; ?>" value="<?php echo $Ingridents['ID']; ?>">
                                        <input required type="text" name="Ingname<?php echo $i?>" value="<?php echo $Ingridents['Name'] ?>" placeholder="Name" style="width:40%;display:inline;">
                                        <input required type="text" name="Ingtype<?php echo $i?>" value="<?php echo $Ingridents['Type'] ?>" placeholder="Unit" style="width:20%;display:inline;">
                                        <input type="text" name="Ingquantity<?php echo $i?>" value="<?php echo $Ingridents['Quantity'] ?>" placeholder="Quantity" style="width:30%;display:inline;">
                                        
                                    </div>
                                    <?php
									$i++;
									}
                                    ?>
                                </div>
                            </article>
                        </div>
                        <div class="tab-pane " id="Service3">
                            <hr class="vertical-space1">
                            <article class="icon-box">
                                <article class="icon-box">
                                    <input type="number" placeholder="Duration" name="Duration" value="<?php echo $Duration?>">
                                    <textarea style="width :100%;height:200px;" name="HowTo" Required><?php echo $Recipe['Methode']?></textarea>
                                </article>
                            </article>
                        </div>
                        <div class="tab-pane" id="Service4">
                            <hr class="vertical-space1">
                            <article class="icon-box">
                                <textarea style="width :100%;height:200px;" name="Serve" Required><?php echo $Recipe['Serve']?></textarea>
                            </article>
                        </div>
                    </div>
                    <!-- end -->
                    <div class="clear"></div>
                    <hr class="vertical-space1">
                    <button type="submit" style="float:right;" class="blue" >
                        <span class="icomoon-upload-7" > Accept</span>
                    </button>
                    <!-- Latest Projects - Start -->
                    <hr class="vertical-space1">
                </section>
            </form>
            <!-- home-portfolio -->
            <section class="container">
                <hr class="vertical-space2">
                <!-- Latest Projects-end -->
                <hr class="vertical-space2">
            </section>
            <!-- container -->
            <!-- end-wrap -->
            <!-- End Document
            ================================================== -->
            <script>
				function readURL(input) {
					if (input.files && input.files[0]) {
						var reader = new FileReader();
						reader.onload = function(e) {
							$('#HeadImg').attr('src', e.target.result);
						}
						reader.readAsDataURL(input.files[0]);
					}
				}


				$(document).ready(function() {
					var ID = $("#PostID").val();
					var Category = $("#Category").val();
					$("#" + Category + "").attr('checked', true);
					$("#Imginp").change(function() {
						readURL(this);
					});
					$("#CurrentIMG").load("ImgAtts.php?PostID=" + ID);
					$("#CurrentVID").load("VidAtts.php?PostID=" + ID);

				});
            </script>
            <script type="text/javascript" src="js/jcarousel.js"></script>
            <script type="text/javascript" src="js/mexin-custom.js"></script>
            <script type="text/javascript" src="js/doubletaptogo.js"></script>
            <!-- FlexSlider -->
            <script defer src="js/jquery.flexslider-min.js"></script>
            <script src="js/bootstrap-alert.js"></script>
            <script src="js/bootstrap-dropdown.js"></script>
            <script src="js/bootstrap-tab.js"></script>
            <script type="text/javascript" src="js/jquery.easy-pie-chart.js"></script>
            <script src="js/bootstrap-tooltip.js"></script>
    </body>
</html>