<section class="twelve columns" style="width: 770px;margin-left: 0px;">
    <div class="container" style="width: 100%;">
        <div id="pin-content">
            <?php
            include("dbconnect.php");
            session_start();

              if (isset ($_SESSION['UserMail']))
    {
    $USEREMAIL=$_SESSION['UserMail'];
$Query=$bdd->query("SELECT Type FROM Users where Email='$USEREMAIL' ");
$Record=$Query->fetch();
$UserType=$Record['Type'];
    }

            if (isset($_GET['delete'])) {
                $id = $_GET['delete'];
                $bdd->exec("Delete FROM Attachments where PostID='$id'");
                $bdd->exec("Delete FROM Recipes where ID='$id'");
            }


            if (isset($_GET['Category'])) {
                $Category = $_GET['Category'];
                $Query = $bdd->query("SELECT count('ID') As 'Pages' FROM Recipes where Category='$Category'");
                $Record = $Query->fetch();
                $ElementsNumb = (int) $Record['Pages'];
            }


            if (isset($_GET['tag'])) {
                $tag = $_GET['tag'];
                $Query = $bdd->query("SELECT count('ID') As 'Pages' FROM Tags where Name='$tag'");
                $Record = $Query->fetch();
                $ElementsNumb = (int) $Record['Pages'];
            }

            if (isset($_GET['search'])) {
                $search = $_GET['search'];
                $Query = $bdd->query("SELECT count('ID') As 'Pages' FROM Recipes where Title='$search'");
                $Record = $Query->fetch();
                $ElementsNumb = (int) $Record['Pages'];
            }

            $Page = $_GET['Page'];
            $Limit = ((int) $Page - 1) * 12;
            ?>
            <input type="hidden" id="ElementsNumb" value="<?php echo $ElementsNumb; ?>">
            <?php
            if (isset($_GET['tag'])) {
                $tag = $_GET['tag'];
                $Query = $bdd->query("SELECT Recipes.*,Link,ImgUrl,Firstname,Lastname,Email FROM `Recipes` inner join Tags on Recipes.ID=Tags.PostID inner join Attachments ON Recipes.ID=Attachments.PostID inner join Users ON Submitter=Email where Placement='Header' AND Tags.Name='$tag' ORDER BY SubmissionDate DESC LIMIT $Limit,12 ");
            } elseif (isset($_GET['search'])) {
                $search = $_GET['search'];
                $Query = $bdd->query("SELECT Recipes.*,Link,ImgUrl,Firstname,Lastname,Email FROM `Recipes` inner join Attachments ON Recipes.ID=Attachments.PostID inner join Users ON Submitter=Email where Placement='Header' AND Title Like '%$search%' ORDER BY SubmissionDate DESC LIMIT $Limit,12 ");
            } else
                $Query = $bdd->query("SELECT Recipes.*,Link,ImgUrl,Firstname,Lastname,Email FROM `Recipes` inner join Attachments ON ID=PostID inner join Users ON Submitter=Email where Placement='Header' AND Category='$Category' ORDER BY SubmissionDate DESC LIMIT $Limit,12 ");

            while ($Record = $Query->fetch()) {
                ?>
                <article class="pin-box entry web -item">
                    <div class="img-item">
                        <a href="<?php echo $Record['Link'] ?>" class="prettyPhoto"> <img src="<?php echo $Record['Link'] ?>" alt=""> <span class="zoomex">&nbsp;</span> </a>
                    </div>
                    <div class="pin-ecxt">
    <?php if (isset($_SESSION['UserMail'])) { 
        if ($UserType=="Admin"){?>
                            <span><a id="delete<?php echo $Record['ID']; ?>" name="<?php echo $Record['ID']; ?>">
                                    <span style="font-size:150%;" class="  icomoon-close-4 "></span></a></span>
                                    <?php }
                                    if ($UserType=="Admin" || $Record['Email']==$USEREMAIL ){
                                        ?>
                            <span><a href="RecipeEdit.php?id=<?php echo $Record['ID']; ?>"><span style="font-size:130%;" class="    icomoon-pencil-3 "></span></a></span>
                                                    <?php }} ?>
                        <h4 style="display :inline-block"><a href="Recipe.php?id=<?php echo $Record['ID'] ?>"><?php echo $Record['Title'] ?></a></h4><br>
                        <?php
                        if ($Record['Rating'] < 20)
                            echo '<span class="icomoon-star" aria-hidden="true"></span>';
                        else {
                            for ($i = 0; $i < $Record['Rating']; $i = $i + 20) {
                                ?>
                                <span class="icomoon-star-6" aria-hidden="true"></span>
                                <?php }
                        }
                        ?>
                        <span style="float: right"> <span class="icomoon-heart-7" aria-hidden="true"></span> <small><?php echo $Record['HealthyRate'] ?>%
                            </small> </span>
                    </div>
                    <div class="pin-ecxt2">
                        <img src="<?php echo $Record['ImgUrl']; ?>" alt="">
                        <p>
                            <small>Posted by</small>
    <?php echo $Record['Firstname'] . " " . $Record['Lastname']; ?>
                        </p>
                    </div>
                </article>
                             <?php }
                         ?>
        </div>
        <!-- end-pin-content -->
        <hr class="vertical-space2">
    </div>
    <br class="clear">
    <div class="pagination2 pagination2-centered" style="width:100%;">
        <ul>
            <li class="disabled">
                <a>&laquo;</a>
            </li>
<?php
if (isset($_GET['Category'])) {
    $Category = $_GET['Category'];
    $Query = $bdd->query("SELECT count('ID') As 'Pages' FROM Recipes where Category='$Category'");
} elseif (isset($_GET['tag'])) {
    $tag = $_GET['tag'];
    $Query = $bdd->query("SELECT count('ID') As 'Pages' FROM Tags where Name='$tag'");
} elseif (isset($_GET['search'])) {
    $search = $_GET['search'];
    $Query = $bdd->query("SELECT count('ID') As 'Pages' FROM Recipes where Title='$search'");
}

$Record = $Query->fetch();
$PageLimit = (int) ((int) $Record['Pages'] / 12);
for ($i = 1; $i <= $PageLimit + 1; $i++) {

    if ($i == $Page) {
        ?>
                    <li class="active" id="Selected" value="<?php echo $i; ?>">
                        <a id="<?php echo $i; ?>" class="Pagination"><?php echo $i; ?></a>
                    </li>
                    <?php } else {
                    ?>
                    <li>
                        <a id="<?php echo $i; ?>" class="Pagination"><?php echo $i; ?></a>
                    </li>
                    <?php }
            }
            ?>
            <li>
                <a>&raquo;</a>
            </li>
        </ul>
    </div>
        <script type="text/javascript">
    
    $(".Pagination").click(function() {
        
         var Page = $(this).attr("id");
         $("#RecentRecipes").load('RecipesLoad.php?Category=Plates&Page=' + Page);

     });
</script>
    <div class="white-space"></div>
</section>
<script type="text/javascript">
    $(document).ready(function() {
        $("a[id^='delete']").click(function() {
            var Category = $("#Subtitle").text();
            var link = "RecipesLoad.php?Category=" + Category + "&Page=" + $("li[id='Selected']").val() + "&delete=" + $(this).attr('name');
            $("#RecentRecipes").load(link);
            $("#Subtitle").text(Category);
        });
        $("a[class='tag']").click(function() {
            var Name = $(this).attr('id');
            var link = "RecipesLoad.php?Page=1&tag=" + Name;
            $("#RecentRecipes").load(link);
            $("#Subtitle").html("<div class='tagcloud' style='display:inline-block;'><a style='font-size:100%;height: 23px;'>" + Name + "</a></div>");
        });
    });
</script>