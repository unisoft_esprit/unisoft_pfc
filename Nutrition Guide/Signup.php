<!DOCTYPE html>
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
    <!--<![endif]-->

    <head>

        <!-- Basic Page Needs
        ================================================== -->
        <meta charset="utf-8">
        <title>Sign up - Unisoft</title>
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Mobile Specific Metas
        ================================================== -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <!-- CSS
        ================================================== -->
        <link rel="stylesheet" href="css/style.css" type="text/css" media="all">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,900|Roboto|Roboto+Slab:300,400' rel='stylesheet' type='text/css'>

        <!-- JS
        ================================================== -->
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <!--[if lt IE 9]>
        <script src="js/modernizr.custom.11889.js" type="text/javascript"></script>
        <![endif]-->
        <!-- HTML5 Shiv events (end)-->
        <script type="text/javascript" src="js/nav-resp.js"></script>
        <script type="text/javascript" src="js/Signup.js"></script>
        <!-- Favicons
        ================================================== -->
        <link rel="shortcut icon" href="images/unisoft_1.png">

    </head>

    <body>
       
        <!-- Primary Page Layout
        ================================================== -->

        <div id="wrap" class="colorskin-0">

            <header id="header">
                <div class="container">
                    <div>
                        <a href="index.php" style="display:inline"> <img src="images/unisoft.png" height="100" id="img-logo" alt="logo"> </a>
                        <button class="small blue" style="float:right;display:inline;margin-top:30px;" type="button">
                            Sign in
                        </button>
                    </div>

                </div>

            </header>
            <!-- end-header -->

            <!-- PHP CODE START -->
            <?php
include ("dbconnect.php");
/* Error strings */
$nameError = "";
$EmailError = "";
$PasswordError = "";
$PasswordConfirmError = "";
$AgeError = "";
$BirthError = "";
/* Default Inputs values */
        $firstname = "";
        $Lastname = "";
        $Email = "";
        $Age = "";
      
        $Year = "";


if ($_SERVER["REQUEST_METHOD"] == "POST") {
    
        $firstname = $_POST['txtFirstName'];
        $Lastname = $_POST['txtLastName'];
        $Email = $_POST['txtEmail'];
        $Age = (int)$_POST['Age'];
        $Sex = $_POST['Sex'];
        $Month = (int)$_POST['indMonth'];
        $Day = $_POST['day'];
        $Year = (int)$_POST['Year'];
        $Password = $_POST['txtPassword'];
        $passConf = $_POST['txtPasswordConfirm'];
    if (!isset($firstname) OR !isset($Lastname) OR preg_match("#[^a-zA-Z]#", $firstname) OR preg_match("#[^a-zA-Z]#", $Lastname)) 
        $nameError = "Invalid name !";
    
    
    $Record = $bdd->query("Select * FROM Users where Email='$Email'");
    $user = $Record->fetch();
    if (!($user == FALSE AND isset($Email) AND !empty($Email))) $EmailError = "Invalid mail Or Already exist !";
    
    if (strlen($Password) < 5) $PasswordError = "Password too short !";
    elseif (strlen($Password) > 30) $PasswordError = "Password too long !";
    
    if ($Password != $passConf) $PasswordConfirmError = "Password dont match !";
    
    if ($Age > 120 OR $Age < 10) $AgeError = "Invalid Age";
    
    if ((date("Y") - $Year) < 10 OR $Year < 1900) $BirthError = "Invalid Date !";
    
    if ($Month==0) $BirthError = "Choose a month first !";
    
    if ($nameError == "" AND $EmailError == "" AND $PasswordError == "" AND $PasswordConfirmError == "" AND $AgeError == "" AND $BirthError == "") {
       
        

         $QueryTxt = "INSERT INTO Users (Firstname,Lastname,Email,Age,Sex,Password,Type,Status,Birth) 
                            VALUES ('$firstname','$Lastname','$Email',$Age,'$Sex','$Password','User','Actif',
                                '$Year-$Month-$Day');";

          if($query=$bdd->exec($QueryTxt))
             header("Location:".$_SERVER['REQUEST_URI']);
        
    }
}
?>

            <!-- PHP CODE END -->
            <section class="container page-content" style="width:90%;padding-bottom:2%;">

                <div class="seven columns" style="width:50%">
                    <div class="container aligncenter" style="width:100%">
                        <hr class="vertical-space3">
                        <h1 class="mex-title" style="border-bottom:3px solid #333333;">UNISOFT</h1>
                        <br>
                        <div class="Service column ">
                            <h4 class="subtitle" style="color: #C4C6C8;border-bottom:3px solid #C4C6C8;width:100%;">Nutrition</h4>
                            <img src="images/Nutrition.jpg" alt="Unisoft home page" class="Services_Sign" />
                        </div>
                        <div class="Service column">

                            <h4 class="subtitle" style="color: #C4C6C8;border-bottom:3px solid #C4C6C8; width:100%;">Workout</h4>
                            <img src="images/Bodybuild.jpg" alt="Unisoft home page" class="Services_Sign" />

                        </div>
                        <hr class="vertical-space2">

                        <div class="Service column">
                            <h4 class="subtitle" style="color: #C4C6C8;border-bottom:3px solid #C4C6C8;width:100%;">Aesthetics</h4>

                            <img src="images/Esthetic.jpg" alt="Unisoft home page" class="Services_Sign" />
                        </div>
                        <div class="Service column">
                            <h4 class="subtitle" style="color: #C4C6C8;border-bottom:3px solid #C4C6C8;width:100%;">Sexuel wellness</h4>

                            <img src="images/Sexlife.jpg" alt="Unisoft home page" class="Services_Sign" />
                        </div>

                    </div>
                </div>

                <div class="eight columns offset-by-one" style="padding-left:0px;float:right;">
                    <hr class="vertical-space3">
                    <hr class="vertical-space3">
                    <div class="contact-form">
                        <div class="clr"></div>
                        <form action="<?php htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="post" id="frmContact">
                            <h5>Name</h5>
                            <div style="width:100%;">
                                <input required name="txtFirstName" type="text" class="txbx" placeholder="First name" style="width:40%;display:inline;" value="<?php echo $firstname; ?>" />
                                <input requiredname="txtLastName" type="text" class="txbx" placeholder="Last name" style="width:40%;display:inline;" value="<?php echo $Lastname; ?>"/>
                            </div>
                            <!-- PHP Code Start -->
                            <div class="Error">
                                <?php echo "$nameError"; ?>
                            </div>
                            <!-- PHP Code End -->
                            <h5>Email</h5>
                            <input required name="txtEmail" type="Email" class="txbx" placeholder="Email" value="<?php echo $Email; ?>"  />
                            <!-- PHP Code Start -->
                            <div class="Error">
                                <?php echo "$EmailError"; ?>
                            </div>
                            <!-- PHP Code End -->
                            <h5>Password</h5>
                            <input required name="txtPassword" type="Password" class="txbx" placeholder="Password" value=""/>
                            <!-- PHP Code Start -->
                            <div class="Error">
                                <?php echo "$PasswordError"; ?>
                            </div>
                            <!-- PHP Code End -->
                            <h5>Confirm Password</h5>
                            <input required name="txtPasswordConfirm" type="Password" class="txbx" value="" placeholder="Confirm password" />
                            <!-- PHP Code Start -->
                            <div class="Error">
                                <?php echo "$PasswordConfirmError"; ?>
                            </div>
                            <!-- PHP Code End -->
                            <h5>Age</h5>
                            <input required name="Age" type="number" value="<?php echo $Age; ?>" class="txbx" placeholder="Age" />
                            <!-- PHP Code Start -->
                            <div class="Error">
                                <?php echo "$AgeError"; ?>
                            </div>
                            <!-- PHP Code End -->
                            <h5>Sex</h5>
                            <div class="Sex">
                                <!-- <![endif]-->
                                <label />
                                <select name="Sex" >
                                    <option >Male</option>
                                    <option >Female</option>
                                </select>
                                <!--[if !IE]> -->
                            </div>
                            <h5>Birthday</h5>
                            <span class="Birth" style="width:40%;display:inline;float:left;margin-right:2px;"> 
                                <label style="width:100%;" />
                                <select id="Month" name="Month" style="width:100%;display:inline;" onchange="LoadDays()">
                                    <option ></option>}
                                    <option>January</option>
                                    <option>February</option>
                                    <option>March</option>
                                    <option>April</option>
                                    <option>May</option>
                                    <option>June</option>
                                    <option>July</option>
                                    <option>August</option>
                                    <option>September</option>
                                    <option>October</option>
                                    <option>November</option>
                                    <option>December</option>
                                </select>
                            </span>
                           
                            <span class="Birth" style="width:24%;display:inline;float:left;margin-left:10px;"> <!-- <![endif]--> <label style="width:100%;" />
                                <select id="day"  name="day" style="width:100%;display:inline;"  >
                                  
                                </select> </span>
                            <input required type="number" class="txbx" name="Year" value="<?php echo $Year; ?>" placeholder="year" style="width:20%;display:inline;margin-left:0px;">
                            <!-- PHP Code Start -->
                            <div class="Error">
                                <?php echo "$BirthError"; ?>
                            </div>
                            <!-- PHP Code End -->
                            <hr class="vertical-space">
                             <input id="indMonth" name="indMonth" type="hidden"  >
                            
                            <input type="submit" class=" blue" type="button" value="Sign in" style="float:right;margin:10px;">
                            <hr class="vertical-space">

                        </form>
                    </div>
                    <!-- end-contact-form  -->

                </div>
            </section>
            <!-- container -->

            <span id="scroll-top"> <a class="scrollup"><i class="icomoon-arrow-up"> </i></a> </span>
        </div>
        <!-- end-wrap -->

        <!-- End Document
        ================================================== -->

        <script type="text/javascript" src="js/mexin-custom.js"></script>
        <script type="text/javascript" src="js/doubletaptogo.js"></script>
        <script type="text/javascript" src="js/bootstrap-alert.js"></script>

    </body>

</html>
