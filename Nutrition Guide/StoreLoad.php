
<section class="twelve columns" style="width: 770px;margin-left: 0px;">
    <div class="container" style="width: 100%;">

        <?php
        include("dbconnect.php");
        if (isset($_GET['Location']))
        {
            $Location=$_GET['Location']; 
            if ($Location=="All")
                $Location=""; 
            else
            {
            $Query=$bdd->query("SELECT ID FROM Stores where Title='$Location'");
            $Record=$Query->fetch();
            $StoreID=$Record['ID'];
            }
        }

        if (isset($_GET['delete'])) {
            $id = $_GET['delete'];
            $bdd->exec("Delete FROM Attachments where PostID='$id'");
            $bdd->exec("Delete FROM Products where ID='$id'");
        }

        if (isset($_GET['Category'])) {
            $Category = $_GET['Category'];

            if($Location=="")
            $Query = $bdd->query("SELECT count('ID') As 'Pages' FROM Products where Category='$Category'");
        else
            $Query = $bdd->query("SELECT count('ID') As 'Pages' FROM ProductStoreDetails inner join Stores on StoreID=Stores.ID inner join Products on ProductID=Products.ID where Category='$Category' AND Stores.Title='$Location'");            
            $Record = $Query->fetch();
    
            $ElementsNumb = (int) $Record['Pages'];
        }


        if (isset($_GET['tag'])) {
            $tag = $_GET['tag'];
            $Query = $bdd->query("SELECT count('ID') As 'Pages' FROM Tags where Name='$tag'");
            $Record = $Query->fetch();
            $ElementsNumb = (int) $Record['Pages'];
        }

        if (isset($_GET['search'])) {
            $search = $_GET['search'];
            $Query = $bdd->query("SELECT count('ID') As 'Pages' FROM Products where Title='$search'");
            $Record = $Query->fetch();
            $ElementsNumb = (int) $Record['Pages'];
        }

        $Page = $_GET['Page'];
        $Limit = ((int) $Page - 1) * 10;
        ?>
        <input type="hidden" id="ElementsNumb" value="<?php echo $ElementsNumb; ?>">
        <?php
        if (isset($_GET['tag'])) {
            $tag = $_GET['tag'];
            $Query = $bdd->query("SELECT Products.*,Link FROM `Products` inner join Tags on Products.ID=Tags.PostID inner join Attachments ON Products.ID=Attachments.PostID where Placement='Header' AND Tags.Name='$tag' ORDER BY SubmissionDate DESC LIMIT $Limit,10 ");
        } 
        elseif (isset($_GET['search'])) {
            $search = $_GET['search'];
            if($Location==""){
            $Query = $bdd->query("SELECT Products.*,Link FROM `Products` inner join Attachments ON Products.ID=Attachments.PostID  where Placement='Header' AND Title Like '%$search%'   LIMIT $Limit,10 ");
            }
            else{
            $Query = $bdd->query("SELECT DISTINCT Products.*,Link FROM `Products` inner join ProductStoreDetails on  Products.ID=ProductID inner join Attachments ON Products.ID=Attachments.PostID  where Placement='Header' AND Title Like '%$search%' AND StoreID='$StoreID'  LIMIT $Limit,10 ");
            }
        } 
        else
        {
            if($Location=="")
            $Query = $bdd->query("SELECT Products.*,Link FROM `Products` inner join Attachments ON ID=PostID  where Placement='Header' AND Category='$Category' LIMIT $Limit,10 ");
            else
            $Query = $bdd->query("SELECT DISTINCT Products.*,Link FROM `Products` inner join ProductStoreDetails on  Products.ID=ProductID inner join Attachments ON  Products.ID=Attachments.PostID  where Placement='Header' AND Category='$Category' AND StoreID='$StoreID' LIMIT $Limit,10 ");
      }
       while ($Record = $Query->fetch()) {
            ?>
            <figure class="portfolio-item one-third column entry print  -item">
                <div class="img-item">
                    <a href="<?php echo $Record['Link']; ?>" class="prettyPhoto">
                        <img src="<?php echo $Record['Link']; ?>" alt="">
                        <span class="zoomex">&nbsp;</span>
                    </a>
                </div>
                <figcaption style="margin: 10px;">
                    <h4 style="font-weight:400;"><a href="Product.php?id=<?php echo $Record['ID'] ?>" style="color:#434343;"><?php echo $Record['Title'] ?></a>
                    </h4>
                    <p style="border-bottom: 4px solid #EDEDED;
                       margin-bottom: 40px;
                       padding-bottom: 20px;
                       color:#f39c12;">
    <?php
    if ($Record['Rating'] < 20)
        echo '<span class="icomoon-star" aria-hidden="true"></span>';
    else {
        for ($i = 0; $i < $Record['Rating']; $i = $i + 20) {
            ?>
                                <span class="icomoon-star-6" aria-hidden="true"></span>
        <?php
         }
        }
                       ?>
                        <br />
                        <?php  
                        $ProdID=$Record['ID'];
                        $Query=$bdd->query("SELECT Min(Price) AS 'Min' From ProductStoreDetails where ProductID='$ProdID' ");
                        $Price=$Query->fetch();
                        ?>
                        <small style="color: #e74c3c;font-weight: 800;">Price : <?php echo $Price['Min'] ;?> DT</small>

                    </p>
                </figcaption>
            </figure>

<?php }?>

            <script src="js/jquery.masonry.min.js"></script>
            <script src="js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>

            <hr class="vertical-space2">

        </div>

        <br class="clear">
        <div class="pagination2 pagination2-centered">
      <ul>
                <li class="disabled">
                    <a>&laquo;</a>
                </li>
                <?php
                if (isset($_GET['Category'])) {
                    $Category = $_GET['Category'];
                    $Query = $bdd->query("SELECT count('ID') As 'Pages' FROM Products where Category='$Category'");
                } elseif (isset($_GET['tag'])) {
                    $tag = $_GET['tag'];
                    $Query = $bdd->query("SELECT count('ID') As 'Pages' FROM Tags where Name='$tag'");
                } elseif (isset($_GET['search'])) {
                    $search = $_GET['search'];
                    $Query = $bdd->query("SELECT count('ID') As 'Pages' FROM Products where Title='$search'");
                }

                $Record = $Query->fetch();
                $PageLimit = (int) ((int) $Record['Pages'] / 10);
                for ($i = 1; $i <= $PageLimit + 1; $i++) {

                    if ($i == $Page) {
                        ?>
                        <li class="active" id="Selected" value="<?php echo $i; ?>">
                            <a id="<?php echo $i; ?>"><?php echo $i; ?></a>
                        </li>
                        <?php } else {
                        ?>
                        <li>
                            <a id="<?php echo $i; ?>"><?php echo $i; ?></a>
                        </li>
                        <?php }
                }
                ?>
            <li>
                <a>&raquo;</a>
            </li>
        </ul>
        </div>
        <div class="white-space"></div>
    </section>










<script type="text/javascript">
    $(document).ready(function() {
        $("a[id^='delete']").click(function() {
            var Category = $("#Subtitle").text();
            var link = "RecipesLoad.php?Category=" + Category + "&Page=" + $("li[id='Selected']").val() + "&delete=" + $(this).attr('name');
            $("#RecentRecipes").load(link);
            $("#Subtitle").text(Category);
        });
        $("a[class='tag']").click(function() {
            var Name = $(this).attr('id');
            var link = "RecipesLoad.php?Page=1&tag=" + Name;
            $("#RecentRecipes").load(link);
            $("#Subtitle").html("<div class='tagcloud' style='display:inline-block;'><a style='font-size:100%;height: 23px;'>" + Name + "</a></div>");
        });
    });
</script>