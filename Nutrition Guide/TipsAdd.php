<!DOCTYPE html>
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->

<head>
    <!-- Basic Page Needs
        ================================================== -->
    <meta charset="utf-8">
    <title>Add Tip - Nutrition guide - Unisoft</title>
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Mobile Specific Metas
        ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- CSS
        ================================================== -->
    <link rel="stylesheet" href="css/style.css" type="text/css" media="all">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,900|Roboto|Roboto+Slab:300,400' rel='stylesheet' type='text/css'>
    <!-- JS
        ================================================== -->
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <!--[if lt IE 9]>
        <script src="js/modernizr.custom.11889.js" type="text/javascript"></script>
        <![endif]-->
    <!-- HTML5 Shiv events (end)-->
    <script type="text/javascript" src="js/nav-resp.js"></script>
    <!-- Favicons
        ================================================== -->
    <link rel="shortcut icon" href="images/logo.png" />
    <script type="text/javascript" src="js/Tips.js"></script>
</head>

<body>

<?php

include ("dbconnect.php");
if ($_SERVER['REQUEST_METHOD']=="POST"){
$Title=$_POST['Title'];
$Content=$_POST['Content'];
$Category=$_POST['Category'];
$IMGCounter = (int)$_POST['IMGCounter'];
$VIDCounter = (int)$_POST['VIDCounter'];
$header = $_FILES["image0"]["name"];

$Query=$bdd->query("SELECT TipCounter FROM `Tips` ORDER BY TipCounter DESC LIMIT 1 ");

            $Data=$Query->fetch();
            $ID="T".($Data['TipCounter']+1);
            
             $Query="INSERT INTO Tips (ID,Title,Content,Category,Email) VALUES ('$ID','$Title','$Content','$Category','haithem.sboui@esprit.tn')";
            
            $bdd->exec($Query);
            $hederimg=$ID."0";
            $Query="INSERT INTO Attachments (Link,PostID,Type,Placement) VALUES ('Uploads/$hederimg.png','$ID','Image','Header'); ";
            $bdd->exec($Query);

            move_uploaded_file($_FILES["image0"]["tmp_name"],"Uploads/".$ID."0".".png");


            for ($i=1; $i <$IMGCounter+1 ; $i++) {
                $image=$_FILES["image$i"]["name"];
                if ($image=="")
                    break;
                 move_uploaded_file($_FILES["image$i"]["tmp_name"],"Uploads/".$ID.$i.".png");
            $Query="INSERT INTO Attachments (Link,PostID,Type,Placement) VALUES ('Uploads/$ID$i.png','$ID','Image','Content'); ";
            $bdd->exec($Query);
            }
             for ($i=1; $i <$VIDCounter+1 ; $i++) {
                $video=$_FILES["video$i"]["name"];
            if ($video=="")
                    break;
                move_uploaded_file($_FILES["video$i"]["tmp_name"],"Uploads/".$ID.$i.".mp4");
                $Query = "INSERT INTO Attachments (Link,PostID,Type,Placement) VALUES ('Uploads/$ID$i.mp4','$ID','Video','Content'); ";
                $bdd -> exec($Query);
            }
            $TagCount=$_POST['TagCount'];
        for ($i=1;$i<$TagCount+1;$i++)
        {
            $Tagname=$_POST["tag$i"];
            $Query = "INSERT INTO Tags (Name,PostID,Section) VALUES ('$Tagname','$ID','Tips'); ";
            $bdd -> exec($Query);
        }
            header('Location:Tips.php');
            exit();
    }
?>
    <!-- Primary Page Layout
        ================================================== -->
    <div id="wrap" class="colorskin-0">

        <div class="top-bar">
            <div class="container">
                <div class="top-links">
                    <a href="AboutUS.php">About us</a> | <a href="Contact.php">Contact</a>|  <?php session_start();if( isset($_SESSION['UserMail'])){ ?>
                      <a>  <?php include ("dbconnect.php");$usermail=$_SESSION['UserMail'];
                            $Query=$bdd->query("SELECT Firstname,Lastname FROM Users where Email='$usermail'");
                             $Record=$Query->fetch();
                             echo $Record['Firstname']." ".$Record['Lastname'];?></a>
                    | <a id="Logout" href="Logout.php?link=<?php echo $_SERVER['REQUEST_URI']; ?>">Logout</a> 
                      <?php }else { ?>  
                     <a  href="Signup.php" >Sign up</a>
                     <?php }?>
                </div>
                <div class="socailfollow">
                    <a href="#" class="facebook"> <i class="icomoon-facebook"> </i> 
                    </a>
                    <a href="#" class="dribble"> <i class="icomoon-dribbble"> </i> 
                    </a>
                    <a href="#" class="vimeo"> <i class="icomoon-vimeo"> </i> 
                    </a>
                    <a href="#" class="google"><i class="icomoon-google"> </i> </a>
                    <a href="#" class="twitter"><i class="icomoon-twitter"> </i> </a>
                </div>
            </div>
        </div>

        <header id="header">
            <div class="container">
                <div class="four columns logo">
                    <a href="index.php">
                        <img src="images/logo.png" width="70" height="60" id="img-logo" alt="logo">
                    </a>
                    <h4 class="subtitle" style="color:#9AA6AF ;display: inline;">Nutrition guide</h4>
                </div>
                <nav id="nav-wrap" class="nav-wrap1 twelve columns">

                    <ul id="nav">
                        <li class="current">
                            <a href="index.php">Home</a>
                        </li>
                        <li>
                            <a href="Recipes.php">Recpies</a>
                            <ul>
                                <li>
                                    <a href="Recipes.php?cat=Plates">Plates</a>                                </li>
                                <li>
                                    <a href="Recipes.php?cat=Salades">Salades</a>                                </li>
                                <li>
                                    <a href="Recipes.php?cat=Juices">Juices</a>                                </li>
                                <li>
                                    <a href="Recipes.php?cat=Cookies">Cookies</a>                                </li>

                            </ul>
                        </li>
                        <li>
                            <a href="Tips.php">Tips</a>
                            <ul>
                                <li>
                                    <a href="Tips.php?cat=Recommandation">Recommandation</a>
                                </li>
                                <li>
                                    <a href="Tips.php?cat=Avoid">To avoid</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="Store.php">Store</a>
                            <ul>
                               <li>
                                    <a href="Store.php?cat=Organic_Ingredients">Organic Ingredients</a>
                                </li>
                                <li>
                                    <a href="Store.php?cat=Medecine">Medecine</a>
                                </li>
                                <li>
                                    <a href="Store.php?cat=Equipments">Equipments</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <?php
if (isset($_SESSION['UserMail'])) {
    ?>
                                        <?php
    $USEREMAIL=$_SESSION['UserMail'];
   $Query=$bdd->query("SELECT ID,Type FROM Users where Email = '$USEREMAIL'");
    $Record=$Query->fetch();
    $ID=$Record['ID'];
    $Type=$Record['Type'];
    if ($Type=="User"){
    ?>

                                    <a href="User.php?id=<?php echo $ID ;?>">Profile</a>
    <?php
    }
    elseif ($Type=="Admin") {
?>
                                    <a href="Admin.php">Administration</a>

<?php
        }
} else {
    ?>
                                    <a href="#">Login</a>
                                    <ul style="width:180px;padding:5%;">
                                        <li>
                                            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post" style="width:150px;margin:5%;">
                                                <input type="email" name="EmailLogin" value="" placeholder="Email">
                                                <input type="password" name="PassLogin" value="" placeholder="Password">
                                                <div style="color:red; text-weight:300;"><?php echo $Error; ?></div>
                                                <br>
                                                <input type="submit" style="background-color:#EF4836;width:150px;margin:2%;" value="Log in">
                                            </form>
                                        </li>

                                    </ul>
<?php } ?>

                        </li>
                    </ul>
                </nav>
                <!-- /nav-wrap -->
            </div>

        </header>

        <!-- end-header -->
        <section class="container page-content">
            <hr class="vertical-space2">

            <form method="post" action="<?php htmlspecialchars($_SERVER['PHP_SELF']) ;?>" enctype="multipart/form-data">
                <div class="sixteen columns portfolio-item">
                    <button type="submit" style="float:right;" class="blue">
                        <span class="icomoon-upload-7">Accept</span>
                    </button>
                    <h3 style="display : inline-block">Header image</h3>
                    <br>
                    <input type="file" accept="image/*" name="image0" />
                    <hr class="vertical-space">

                    <input type="text" name="Title" value="" placeholder="Title" required>
                    <textarea name="Content" class="era" rows="8" style="width:500px;" required></textarea>
                    
                    <div>
                        <input type="radio" name="Category" value="Recommandation" checked style="display:inline;">
                        <h6 style="display:inline;">Recommandation</h6>
                        <input type="radio" name="Category" value="Avoid" style="display:inline;">
                        <h6 style="display:inline;">To avoid</h6>
                    </div>
                    <hr class="vertical-space">
                    <h3 style="display : inline-block">Tags</h3>
                        <a style="margin-left:15px;display : inline-block" id="TagAdd"> <span class="icomoon-plus "></span> </a>
                     <div id="Tags" >
                        <input type="hidden" value="0" name="TagCount" id="TagCount">
                    </div>
                    <script>
                    var i=1;
                    $("#TagAdd").click(function() {
                        $("#Tags").append('<a class="tagcloud" ><input name="tag'+i+'" style="display:inline-block;width: 70px; margin-bottom: 0px; height: 27px; padding-bottom: 0px; padding-top: 0px;" type="text" style="width:50px;"></a>');
                        $("#TagCount").val(i);
                        i++;
                    });
                    </script>
                </div>
                <hr class="vertical-space2">
                <div class="sixteen columns">
                    <div id="UploadIMG" class="one_half column">
                        <h3 style="display : inline-block">Content images</h3>
                        <a style="margin-left:15px;display : inline-block" id="ImgUpl">
                            <span class="icomoon-plus "></span>
                        </a>
                        <br>
                        <input type="hidden" id="IMGCounter" name="IMGCounter" value="0">

                    </div>
                    <div id="UploadVID" class="one_half column">
                        <h3 style="display : inline-block">Content Videos</h3>
                        <a style="margin-left:15px;display : inline-block" id="VidUpl">
                            <span class="icomoon-plus "></span>
                        </a>
                        <br>
                        <input type="hidden" id="VIDCounter" name="VIDCounter" value="0">

                    </div>
                </div>


            </form>
            <div class="clear"></div>
            <hr class="vertical-space1">


            <!-- Latest Projects - Start -->
            <hr class="vertical-space1">
            <!-- end-commentbox  -->
                    </section>
        <!-- end-main-conten -->

        <!-- container -->

    </div>
    <!-- end-wrap -->
    <!-- end-header -->

    <!-- End Document
        ================================================== -->

    <script type="text/javascript" src="js/jcarousel.js"></script>
   
    <script type="text/javascript" src="js/mexin-custom.js"></script>
    <script type="text/javascript" src="js/doubletaptogo.js"></script>
    <script src="layerslider/jQuery/jquery-easing-1.3.js" type="text/javascript"></script>
    <script src="layerslider/js/layerslider.kreaturamedia.jquery.js" type="text/javascript"></script>
    <script src="js/layerslider-init.js"></script>
    <script src="js/bootstrap-alert.js"></script>
    <script src="js/bootstrap-dropdown.js"></script>
    <script src="js/bootstrap-tab.js"></script>

</body>

</html>
