
<h4 style="color:#424748;font-weight:900;text-transform: uppercase;font-family: "Open Sans", Helvetica, Arial, sans-serif;text-align:center;">Users</h4>
<?php 
include("dbconnect.php");
if(isset($_GET['Ban']))
{
	$BanID=$_GET['Ban'];
$bdd->exec("UPDATE Users SET Status='Banned' where ID='$BanID'");
}

if(isset($_GET['Release']))
{
	$ReleaseID=$_GET['Release'];
$bdd->exec("UPDATE Users SET Status='Actif' where ID='$ReleaseID'");
}

if(isset($_GET['Delete']))
{
	$DeleteID=$_GET['Delete'];
$bdd->exec("Delete FROM Users where ID='$DeleteID'");
	
}

$Query=$bdd->query("SELECT * FROM Users ");
?>

<table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Email</th>
                            <th>Firstname</th>
                            <th>Lastname</th>
                            <th>Status</th>
                            <th>Ban</th>
                            <th>Delete</th>                            
                        </tr>
                    </thead>
                    <tbody>
<?php 
while ($Record=$Query->fetch())
{
	?>
                        <tr>
                            <td><?php echo $Record['ID'] ?></td>
                            <td><?php echo $Record['Email'] ?></td>
                            <td><?php echo $Record['Firstname'] ?></td>
                            <td><?php echo $Record['Lastname'] ?></td>
                            <td><?php echo $Record['Status'] ?></td>
                            <?php if($Record['Status']=="Actif"){
                            	?>
                            <td><a class="Ban" id="<?php echo $Record['ID'] ?>">Ban</a></td>
                            <?php }
                            elseif ($Record['Status']=="Banned") {	
                            ?>
                            <td><a class="Release" id="<?php echo $Record['ID'] ?>">Release</a></td>
                            <?php } ?>
                            <td><a class="Delete" id="<?php echo $Record['ID'] ?>">Delete</a></td>
                        </tr>
      
      <?php 
      }
      ?>              </tbody>
                </table>
<script>
$(".Ban").click(function(){
	var Id=$(this).attr('ID');
$("#Usersdiv").load("UsersAdmin.php?Ban="+Id);
});
$(".Delete").click(function(){
	var Id=$(this).attr('ID');
$("#Usersdiv").load("UsersAdmin.php?Delete="+Id);
});
$(".Release").click(function(){
	var Id=$(this).attr('ID');
$("#Usersdiv").load("UsersAdmin.php?Release="+Id);
});

</script>
<br>
<hr>