

<!DOCTYPE html>
<?php
include ("dbconnect.php");
$Error = "";
        session_start();

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $username = $_POST['EmailLogin'];
    $password = $_POST['PassLogin'];

     
    $Query = $bdd->query("SELECT Email,Password FROM Users where Email='$username' and Password='$password'");
    if($Data = $Query->fetch()){
    if ($Data['Email'] == $username AND $Data['Password'] == $password) {
        $_SESSION['UserMail'] = $username;
        header("Location:".$_SERVER['REQUEST_URI']);

    }
}
    else
    {
        $Error = "Invalid Data";
    }
}
?>


<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
    <!--<![endif]-->

    <head>
        <!-- Basic Page Needs
            ================================================== -->
        <meta charset="utf-8">
        <title>Nutrition guide - Unisoft</title>
        <meta name="description" content="Unisoft Nutrition guide">
        <meta name="author" content="Unisoft">
        <!-- Mobile Specific Metas
            ================================================== -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <!-- CSS
            ================================================== -->
        <link rel="stylesheet" href="css/style.css" type="text/css" media="all">
        <link rel="stylesheet" href="css/style-selector.css" type="text/css">

        <!-- JS
                ================================================== -->
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/Home.js"></script>

        <!--[if lt IE 9]>
                <script src="js/modernizr.custom.11889.js" type="text/javascript"></script>
                <![endif]-->
        <!-- HTML5 Shiv events (end)-->
        <script type="text/javascript" src="js/nav-resp.js"></script>
        <script type="text/javascript" src="js/colorize.js"></script>
        <script>
            (function(i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] ||
                        function() {
                            (i[r].q = i[r].q || []).push(arguments);
                        }, i[r].l = 1 * new Date();
                a = s.createElement(o), m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m);
            })(window, document, 'script', '../../www.google-analytics.com/analytics.js', 'ga');
            ga('create', 'UA-43921752-1', 'webnus.net');
            ga('send', 'pageview');
        </script>
        <!-- Favicons
                ================================================== -->
        <link rel="shortcut icon" href="images/logo.png" alt="unisoft.com" />
        <script src="js/jquery.masonry.min.js"></script>
        <script src="js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>

    </head>

    <body>
        <!-- Primary Page Layout
                ================================================== -->
        <div id="wrap" class="colorskin-0">
            <div class="top-bar">
                <div class="container">
                    <div class="top-links">
                        <a href="AboutUS.php">About us</a> | <a href="Contact.php">Contact</a>|

<?php if (isset($_SESSION['UserMail'])) { ?>
                            <a>  
                            <?php $usermail=$_SESSION['UserMail'];
                            $Query=$bdd->query("SELECT Firstname,Lastname FROM Users where Email='$usermail'");
                             $Record=$Query->fetch();
                             echo $Record['Firstname']." ".$Record['Lastname'];
                         ?>
                            </a>
                            | <a id="Logout" href="Logout.php?link=<?php echo $_SERVER['REQUEST_URI']; ?>">Logout</a> 
<?php } else { ?>  
                            <a  href="Signup.php" >Sign up</a>
                        <?php } ?>
                    </div>
                    <div class="socailfollow">
                        <a href="#" class="facebook"> <i class="icomoon-facebook"> </i>
                        </a>
                        <a href="#" class="dribble"> <i class="icomoon-dribbble"> </i>
                        </a>
                        <a href="#" class="vimeo"> <i class="icomoon-vimeo"> </i>
                        </a>
                        <a href="#" class="google"><i class="icomoon-google"> </i> </a>
                        <a href="#" class="twitter"><i class="icomoon-twitter"> </i> </a>
                    </div>
                </div>
            </div>
            <header id="header">
                <div class="container">
                    <div class="four columns logo">
                        <a href="index.php">
                            <img src="images/logo.png" width="70" height="60" id="img-logo" alt="unisoft">
                        </a>
                        <h4 class="subtitle" style="color:#9AA6AF ;display: inline;">Nutrition guide</h4>
                    </div>
                    <nav id="nav-wrap" class="nav-wrap1 twelve columns">
                        <ul id="nav">
                            <li class="current">
                                <a href="index.php">Home</a>
                            </li>
                            <li>
                                <a href="Recipes.php">Recpies</a>
                                <ul>

                                    <li>
                                        <a href="Recipes.php?cat=Plates">Plates</a>
                                    </li>
                                    <li>
                                        <a href="Recipes.php?cat=Salades">Salades</a>
                                    </li>
                                    <li>
                                        <a href="Recipes.php?cat=Juices">Juices</a>
                                    </li>
                                    <li>
                                        <a href="Recipes.php?cat=Cookies">Cookies</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="Tips.php">Tips</a>
                                <ul>
                                    <li>
                                        <a href="Tips.php?cat=Recommandation">Recommandation</a>
                                    </li>
                                    <li>
                                        <a href="Tips.php?cat=Avoid">To avoid</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="Store.php">Store</a>
                                <ul>
                                    <li>
                                        <a href="Store.php?cat=Organic_Ingredients">Organic Ingredients</a>
                                    </li>
                                    <li>
                                        <a href="Store.php?cat=Medecine">Medecine</a>
                                    </li>
                                    <li>
                                        <a href="Store.php?cat=Equipments">Equipments</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
<?php
if (isset($_SESSION['UserMail'])) {
    $USEREMAIL=$_SESSION['UserMail'];
    $Query=$bdd->query("SELECT ID,Type FROM Users where Email = '$USEREMAIL'");
    $Record=$Query->fetch();
    $ID=$Record['ID'];
    $Type=$Record['Type'];
    if ($Type=="User"){
    ?>

                                    <a href="User.php?id=<?php echo $ID ;?>">Profile</a>
    <?php
    }
    elseif ($Type=="Admin") {
?>
                                    <a href="Admin.php">Administration</a>

<?php
        }
} else {
    ?>
                                    <a href="#">Login</a>
                                    <ul style="width:180px;padding:5%;">
                                        <li>
                                            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post" style="width:150px;margin:5%;">
                                                <input type="email" name="EmailLogin" value="" placeholder="Email">
                                                <input type="password" name="PassLogin" value="" placeholder="Password">
                                                <div style="color:red; text-weight:300;"><?php echo $Error; ?></div>
                                                <br>
                                                <input type="submit" style="background-color:#EF4836;width:150px;margin:2%;" value="Log in">
                                            </form>
                                        </li>

                                    </ul>
<?php } ?>
                            </li>
                        </ul>
                    </nav>
                    <!-- /nav-wrap -->
                </div>
            </header>
            <!-- end-header -->
            <section id="hero" class="tbg1">
                <div id="layerslider-container-fw">
                    <div id="layerslider" style="width: 100%; height: 436px; margin: 0px auto; ">
                        <div class="ls-layer" style="slidedirection: top; slidedelay: 6000; durationin: 1500; durationout: 1500; delayout: 500;">
                            <img src="images/slide-pics/slide1-bg.jpg" class="ls-bg" alt="">
                           
                            <h1 class="ls-s3" style="position: absolute; top:100px; left: 700px; slidedirection : top; slideoutdirection : top; durationin : 3000; durationout : 750; easingin : easeInOutQuint; easingout : easeInBack; delayin : 1000;">EQUIPMENTS &
                                <br>MERCHANDICES
                            </h1>
                            <h4 class="ls-s3 l1-s1" style="position: absolute; top:230px; left: 700px; slidedirection : bottom; slideoutdirection : bottom; durationin : 3000; durationout : 750; easingin : easeInOutQuint; easingout : easeInBack; delayin : 1000;">We have what you desire</h4>
                            <a class="button red ls-s8" href="Store.php" style="position: absolute; top:320px; left: 700px; slidedirection : bottom; slideoutdirection : bottom; durationin : 3000; durationout : 750; easingin : easeInOutQuint; easingout : easeInOutQuint; delayin : 1100;">Enter Now</a>
                        </div>
                        <div class="ls-layer" style="slidedirection: right; slideoutdirection : top; slidedelay: 5000; durationin: 1500; durationout: 1500; delayout: 500;">
                            <img src="images/slide-pics/slide3-bg.jpg" class="ls-bg" alt="">
                            <img src="images/slide-pics/slide3-pic1.png" class="ls-s6" alt="" style="top: 28px; left: 20px;  slidedirection : fade; slideoutdirection : fade; durationin : 1500; durationout : 750; easingin : easeInOutQuint; easingout : easeInOutQuint; delayin : 500;">
                            <h1 class="ls-s3" style="position: absolute; top:120px; left: 700px; color:#fff; slidedirection : top; slideoutdirection : top; durationin : 3000; durationout : 750; easingin : easeInOutQuint; easingout : easeInBack; delayin : 600;">TIPS</h1>
                            <h4 class="ls-s3 l1-s1" style="position: absolute; top:196px; left: 700px; color:#fff; border-color:#fff; slidedirection : bottom; slideoutdirection : bottom; durationin : 3000; durationout : 750; easingin : easeInOutQuint; easingout : easeInBack; delayin : 700;">Sharing news and advices</h4>
                            <a class="button skyblue ls-s8" href="Tips.php" style="position: absolute; top:284px; left: 700px; slidedirection : bottom; slideoutdirection : bottom; durationin : 3000; durationout : 750; easingin : easeInOutQuint; easingout : easeInOutQuint; delayin : 800;">Enter Now</a>
                        </div>
                        <div class="ls-layer" style="slidedirection: top; slideoutdirection : top; slidedelay: 5000; durationin: 1500; durationout: 1500; delayout: 500;">
                            <img src="images/slide-pics/slide4-bg.jpg" class="ls-bg" alt="">
                            <h1 class="ls-s3" style="position: absolute; top:120px; left: 30px;  slidedirection : top; slideoutdirection : top; durationin : 3000; durationout : 750; easingin : easeInOutQuint; easingout : easeInBack; delayin : 500;">RECIPES</h1>
                            <h4 class="ls-s3 l1-s1" style="position: absolute; top:198px; left: 30px;  slidedirection : bottom; slideoutdirection : bottom; durationin : 3000; durationout : 750; easingin : easeInOutQuint; easingout : easeInBack; delayin : 600;">Explore recipes from all over the world </h4>
                            <a class="button skyblue ls-s8" href="Recipes.php" style="position: absolute; top:286px; left: 30px; slidedirection : bottom; slideoutdirection : bottom; durationin : 3000; durationout : 750; easingin : easeInOutQuint; easingout : easeInOutQuint; delayin : 700;">Enter Now</a>
                        </div>
                    </div>
                </div>
            </section>

            <section class="container home-content" style="background-color: #FFFFFF;width: 100%;padding-bottom: 5%;">
                <div class="row" style="margin-bottom: 0px;">
                    <!-- Start-Recent Works-->
                    <div class="container page-content ">
                        <h4 class="subtitle" style="color:#595959 ;border-bottom: 3px solid rgba(0, 0, 0, 0.27);">Top Recipes</h4>
                        <div class="three columns sidebar leftside">
                            <hr class="vertical-space1">
                            <hr class="vertical-space1">
                            <h5>
                                <strong>Categories</strong>
                            </h5>
                            <div class="listbox1">
                                <ul>
                                    <li>
                                        <a id="Plates">Plates </a>
                                    </li>
                                    <li>
                                        <a id="Salades">Salades</a>
                                    </li>
                                    <li>
                                        <a id="Juices">Juices</a>
                                    </li>
                                    <li>
                                        <a id="Cookies">Cookies </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div id="TopRecepies">
                        </div>
                    </div>
                </div>
            </section>
            <section class="container home-content" style="background-color: #E74C3C;width: 100%;padding-bottom: 5%;">
                <!-- Start Page Content -->
                <div class="row" style="margin-bottom: 0px;">
                    <div class="container page-content ">

                        <h4 class="subtitle" style="color:#E5E5E5">Top Products</h4>
                        <div id="TopProduct">
                        </div>
                    </div>
                </div>
            </section>


            <div id="TopTips">
            </div>

            <section class="blox dark nopad section-bg2" style="border-top:0px solid #e5e5e5;">
                <div class="container aligncenter">
                    <hr class="vertical-space2">
                    <h5>Other Services By</h5>
                    <h1 class="mex-title">UNISOFT</h1>
                    <div class="sixteen columns">
                        <div class="one_third column">
                            <h4 class="subtitle" style="color: #C4C6C8;">Workout</h4>
                            <img src="images/Bodybuild.jpg" alt="Unisoft home page" class="Services_background" />
                        </div>
                        <div class="one_third column">
                            <h4 class="subtitle" style="color: #C4C6C8;">Aesthetics</h4>
                            <img src="images/Esthetic.jpg" alt="Unisoft home page" class="Services_background" />
                        </div>
                        <div class="one_third column">
                            <h4 class="subtitle" style="color: #C4C6C8;">wellness guide</h4>
                            <img src="images/Sexlife.jpg" alt="Unisoft home page" class="Services_background" />
                        </div>
                    </div>
                </div>
            </section>
            <footer id="footer" style="border-top: 5px solid #252627;">
                <section class="container footer-in">
                    <div class="one_third column contact-inf">
                        <h4 class="subtitle">Contact Information</h4>
                        <br />
                        <p>
                            <strong>Address:</strong>
                            Esprit - EL Ghazela Ariana,Tunisia
                        </p>
                        <p>
                            <strong>Phone:</strong>+ 1 (234) 567 8901
                        </p>
                        <p>
                            <strong>Fax:</strong>+ 1 (234) 567 8901
                        </p>
                        <p>
                            <strong>Email:</strong>support@yoursite.com
                        </p>
                        <div>
                            <h4 class="subtitle">Stay Connected</h4>
                            <div class="socailfollow">
                                <a href="#" class="facebook"><i class="icomoon-facebook"> </i></a>
                                <a href="#" class="dribble"><i class="icomoon-dribbble"> </i></a>
                                <a href="#" class="vimeo"><i class="icomoon-vimeo"> </i></a>
                                <a href="#" class="google"><i class="icomoon-google"> </i></a>
                                <a href="#" class="twitter"><i class="icomoon-twitter"> </i></a>
                                <a href="#" class="youtube"><i class="icomoon-youtube"> </i></a>

                          <!--  <iframe src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fpages%2FUnisoft-health-guide&amp;width=48&amp;layout=standard&amp;action=like&amp;show_faces=true&amp;share=true&amp;height=80" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:48px; height:80px;" allowTransparency="true"></iframe>-->
                            </div>
                        </div>
                    </div>
                    <!-- end-contact-info /end -->
                    <!-- tweets  /end -->
                    <div class="two_third column">
                        <h4 class="subtitle">Location</h4>
                        <br />
                        <section class="full-width">
                            <div>
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d797.6600046132598!2d10.189218289020161!3d36.898959160825896!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12e2cb7454c6ed51%3A0x683b3ab5565cd357!2sESPRIT!5e0!3m2!1sen!2s!4v1395085887624" width=100% height="300" frameborder="0" style="border:0"></iframe>
                            </div>
                            <!-- END-contact Map -->
                        </section>
                        <!-- END-Google Map Section -->
                    </div>
                </section>
            </footer>
            <!-- end-footer -->
            <span id="scroll-top"> <a class="scrollup"><i class="icomoon-arrow-up"> </i></a>
            </span>
        </div>
        <!-- end-wrap -->
        <!-- End Document
                ================================================== -->
        <script type="text/javascript" src="js/jcarousel.js"></script>
        <script type="text/javascript">
            jQuery(document).ready(function() {
                jQuery('#latest-projects').jcarousel(), jQuery('#our-clients').jcarousel();
                $("#Logout").click(function() {
                    window.location.replace("Logout.php?link=<?php echo $_SERVER['REQUEST_URI']; ?>");
                });
            });
        </script>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,900|Roboto|Roboto+Slab:300,400' rel='stylesheet' type='text/css'>
        <script type="text/javascript" src="js/mexin-custom.js"></script>
        <script type="text/javascript" src="js/doubletaptogo.js"></script>
        <script src="layerslider/jQuery/jquery-easing-1.3.js" type="text/javascript"></script>
        <script src="layerslider/js/layerslider.kreaturamedia.jquery.js" type="text/javascript"></script>
        <script src="js/layerslider-init.js"></script>
        <script src="js/bootstrap-alert.js"></script>
        <script src="js/bootstrap-dropdown.js"></script>
        <script src="js/bootstrap-tab.js"></script>
    </body>

</html>
