        $(document).ready(function() {
            $("#TopProduct").load("HomeLoadStore.php");
            $("#TopTips").load("HomeLoadTips.php");
            $("#TopRecepies").load('HomeLoadRecpies.php?Category=Plates');
            $('a[id="Plates"]').attr('style', "background-color :#F9F9F9 ; color :#5D6F74;");

            $('a[id="Plates"]').click(function() {
                $("#TopRecepies").load('HomeLoadRecpies.php?Category=Plates');
                $('a[id="Plates"]').attr('style', "background-color :#F9F9F9 ; color :#5D6F74;");
                $('a[id="Salades"]').attr('style', "");
                $('a[id="Juices"]').attr('style', "");
                $('a[id="Cookies"]').attr('style', "");

            });
            $('a[id="Salades"]').click(function() {
                $("#TopRecepies").load('HomeLoadRecpies.php?Category=Salades');
                $('a[id="Salades"]').attr('style', "background-color :#F9F9F9 ; color :#5D6F74;");
                $('a[id="Plates"]').attr('style', "");
                $('a[id="Juices"]').attr('style', "");
                $('a[id="Cookies"]').attr('style', "");
            });
            $('a[id="Juices"]').click(function() {
                $("#TopRecepies").load('HomeLoadRecpies.php?Category=Juices');
                $('a[id="Juices"]').attr('style', "background-color :#F9F9F9 ; color :#5D6F74;");
                $('a[id="Plates"]').attr('style', "");
                $('a[id="Salades"]').attr('style', "");
                $('a[id="Cookies"]').attr('style', "");

            });
            $('a[id="Cookies"]').click(function() {
                $("#TopRecepies").load('HomeLoadRecpies.php?Category=Cookies');
                $('a[id="Cookies"]').attr('style', "background-color :#F9F9F9 ; color :#5D6F74;");
                $('a[id="Plates"]').attr('style', "");
                $('a[id="Juices"]').attr('style', "");
                $('a[id="Salades"]').attr('style', "");

            });
        });
