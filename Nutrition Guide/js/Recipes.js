 $(document).ready(function() {
     var cat = $("#cat").val();
     $("#RecentRecipes").load('RecipesLoad.php?Category=' + cat + '&Page=1');
     $("#Subtitle").text(cat);
     $('a[id="' + cat + '"]').attr('style', "background-color :#F9F9F9 ; color :#5D6F74;");

     $('a[id="Plates"]').click(function() {
         $("#RecentRecipes").load('RecipesLoad.php?Category=Plates&Page=1');
         $("#Subtitle").text("Plates");
         $('a[id="Plates"]').attr('style', "background-color :#F9F9F9 ; color :#5D6F74;");
         $('a[id="Salades"]').attr('style', "");
         $('a[id="Juices"]').attr('style', "");
         $('a[id="Cookies"]').attr('style', "");

     });
     $('a[id="Salades"]').click(function() {
         $("#RecentRecipes").load('RecipesLoad.php?Category=Salades&Page=1');
         $("#Subtitle").text("Salades");
         $('a[id="Salades"]').attr('style', "background-color :#F9F9F9 ; color :#5D6F74;");
         $('a[id="Plates"]').attr('style', "");
         $('a[id="Juices"]').attr('style', "");
         $('a[id="Cookies"]').attr('style', "");
     });
     $('a[id="Juices"]').click(function() {
         $("#RecentRecipes").load('RecipesLoad.php?Category=Juices&Page=1');
         $("#Subtitle").text("Juices");
         $('a[id="Juices"]').attr('style', "background-color :#F9F9F9 ; color :#5D6F74;");
         $('a[id="Plates"]').attr('style', "");
         $('a[id="Salades"]').attr('style', "");
         $('a[id="Cookies"]').attr('style', "");
     });
     $('a[id="Cookies"]').click(function() {
         $("#RecentRecipes").load('RecipesLoad.php?Category=Cookies&Page=1');
         $("#Subtitle").text("Cookies");
         $('a[id="Cookies"]').attr('style', "background-color :#F9F9F9 ; color :#5D6F74;");
         $('a[id="Plates"]').attr('style', "");
         $('a[id="Juices"]').attr('style', "");
         $('a[id="Salades"]').attr('style', "");
     });
     $("#search").on('input', function() {

         $('a[id="Cookies"]').attr('style', "");
         $('a[id="Plates"]').attr('style', "");
         $('a[id="Juices"]').attr('style', "");
         $('a[id="Salades"]').attr('style', "");
         $("#Subtitle").html("<span class='icomoon-search-2'>" + $("#search").val() + "</span>");
         $("#RecentRecipes").load('RecipesLoad.php?search=' + $("#search").val() + '&Page=1');

     });

     // ADD Recipe Page

     $("#ImgUpl").click(function() {
         var Counter = parseInt($("#IMGCounter").val()) + 1;
         $("#IMGCounter").val(Counter);
         $("#UploadIMG").append('<input type="file"  accept="image/*" name="image' + Counter + '"/><hr class="vertical-space">');
     });
     $("#VidUpl").click(function() {
         var Counter = parseInt($("#VIDCounter").val()) + 1;
         $("#VIDCounter").val(Counter);
         $("#UploadVID").append('<input type="file"  accept="video/*"/ name="video' + Counter + '"><hr class="vertical-space">');
     });
     $("#IngAdd").click(function() {
         var Counter = parseInt($("#INGCounter").val()) + 1;
         $("#INGCounter").val(Counter);
         $("#IngridentsList").append('<div><input required type="text" name="Ingname' + Counter + '" value="" placeholder="Name" style="width:40%;display:inline;"><input type="text" name="Ingtype' + Counter + '" value="" placeholder="Unit" style="width:20%;display:inline;"><input type="text" name="Ingquantity' + Counter + '" value="" placeholder="Quantity" style="width:30%;display:inline;"></div>');
     });

     $("#NutAdd").click(function() {
         var Counter = parseInt($("#NUTCounter").val()) + 1;
         $("#NUTCounter").val(Counter);
         $("#NutrientsList").append('<div><input required type="text" name="Nutname' + Counter + '" value="" placeholder="Name" style="width:25%;display:inline;"><input required type="text" name="Percentage' + Counter + '" value="" placeholder="Percentage" style="width:20%;display:inline;"><input type="text" name="Nutunit' + Counter + '" value="" placeholder="Unit" style="width:30%;display:inline;"><input type="text" name="Nutquantity' + Counter + '" value="" placeholder="Quantity" style="width:20%;display:inline;"></div>');
     });

 });
