 function LoadDays() {
     $('#day').empty();
     var Month = document.querySelector('Select[id="Month"]');
     var days = document.querySelector('Select[id="day"]');
     if (Month.value == "January" || Month.value == "March" || Month.value == "May" || Month.value == "July" || Month.value == "August" || Month.value == "October" || Month.value == "December") {
         for (var i = 1; i <= 31; i++) {
             var opt = document.createElement('option');
             opt.value = opt.innerHTML = i;
             days.appendChild(opt);
         }
     } else if (Month.value == "April" || Month.value == "June" || Month.value == "November" || Month.value == "September") {
         for (var i = 1; i <= 30; i++) {
             var opt = document.createElement('option');
             opt.value = opt.innerHTML = i;
             days.appendChild(opt);
         }
     } else if (Month.value == "February") {
         for (var i = 1; i <= 29; i++) {
             var opt = document.createElement('option');
             opt.value = opt.innerHTML = i;
             days.appendChild(opt);
         }
     }
     var ind = $("#Month").prop("selectedIndex");
     $("input:hidden[name=indMonth]").val(ind);


 }
