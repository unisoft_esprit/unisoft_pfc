 $(document).ready(function() {
     var cat = $("#cat").val();

     if (cat == "Organic_Ingredients")
         cat = "Organic Ingredients";

     $("#Subtitle").text(cat);

     if (cat == "Organic Ingredients") {
         cat = "Organic_Ingredients";
     }
     var search = 0;
     var Locations = $("select[name='Locations']").val();
     $('a[id="' + cat + '"]').attr('style', "background-color :#F9F9F9 ; color :#5D6F74;");
     $("#RecentProduct").load('StoreLoad.php?Category=' + cat + '&Page=1&Location=' + Locations);

     $("select[name='Locations']").change(function() {
         Locations = $("select[name='Locations']").val();
         if (search)
             $("#RecentProduct").load('StoreLoad.php?search=' + $("#search").val() + '&Page=1&Location=' + Locations);
         else
             $("#RecentProduct").load('StoreLoad.php?Category=' + cat + '&Page=1&Location=' + Locations);
     });

     $('a[id="Organic_Ingredients"]').click(function() {
         $("#RecentProduct").load('StoreLoad.php?Category=Organic_Ingredients&Page=1&Location=' + Locations);
         $("#Subtitle").text("Organic Ingredients");
         $('a[id="Organic_Ingredients"]').attr('style', "background-color :#F9F9F9 ; color :#5D6F74;");
         $('a[id="Medecine"]').attr('style', "");
         $('a[id="Equipments"]').attr('style', "");
         cat = "Organic_Ingredients";
     });
     $('a[id="Medecine"]').click(function() {
         $("#RecentProduct").load('StoreLoad.php?Category=Medecine&Page=1&Location=' + Locations);
         $("#Subtitle").text("Medecine");
         $('a[id="Medecine"]').attr('style', "background-color :#F9F9F9 ; color :#5D6F74;");
         $('a[id="Organic_Ingredients"]').attr('style', "");
         $('a[id="Equipments"]').attr('style', "");
         cat = "Medecine";
     });
     $('a[id="Equipments"]').click(function() {
         $("#RecentProduct").load('StoreLoad.php?Category=Equipments&Page=1&Location=' + Locations);
         $("#Subtitle").text("Equipments");
         $('a[id="Equipments"]').attr('style', "background-color :#F9F9F9 ; coloru :#5D6F74;");
         $('a[id="Organic_Ingredients"]').attr('style', "");
         $('a[id="Medecine"]').attr('style', "");
         cat = "Equipments";
     });

     $("#search").on('input', function() {
         search = 1;
         $('a[id="Equipments"]').attr('style', "");
         $('a[id="Medecine"]').attr('style', "");
         $('a[id="Organic_Ingredients"]').attr('style', "");
         $("#Subtitle").html("<span class='icomoon-search-2'>" + $("#search").val() + "</span>");
         $("#RecentProduct").load('StoreLoad.php?search=' + $("#search").val() + '&Page=1&Location=' + Locations);


     });

     // ADD Recipe Page

     $("#ImgUpl").click(function() {
         var Counter = parseInt($("#IMGCounter").val()) + 1;
         $("#IMGCounter").val(Counter);
         $("#UploadIMG").append('<input type="file"  accept="image/*" name="image' + Counter + '"/><hr class="vertical-space">');
     });
     $("#VidUpl").click(function() {
         var Counter = parseInt($("#VIDCounter").val()) + 1;
         $("#VIDCounter").val(Counter);
         $("#UploadVID").append('<input type="file"  accept="video/*"/ name="video' + Counter + '"><hr class="vertical-space">');
     });
     $("#IngAdd").click(function() {
         var Counter = parseInt($("#INGCounter").val()) + 1;
         $("#INGCounter").val(Counter);
         $("#IngridentsList").append('<div><input required type="text" name="Ingname' + Counter + '" value="" placeholder="Name" style="width:40%;display:inline;"><input type="text" name="Ingtype' + Counter + '" value="" placeholder="Unit" style="width:20%;display:inline;"><input type="text" name="Ingquantity' + Counter + '" value="" placeholder="Quantity" style="width:30%;display:inline;"></div>');
     });

     $("#NutAdd").click(function() {
         var Counter = parseInt($("#NUTCounter").val()) + 1;
         $("#NUTCounter").val(Counter);
         $("#NutrientsList").append('<div><input required type="text" name="Nutname' + Counter + '" value="" placeholder="Name" style="width:25%;display:inline;"><input required type="text" name="Percentage' + Counter + '" value="" placeholder="Percentage" style="width:20%;display:inline;"><input type="text" name="Nutunit' + Counter + '" value="" placeholder="Unit" style="width:30%;display:inline;"><input type="text" name="Nutquantity' + Counter + '" value="" placeholder="Quantity" style="width:20%;display:inline;"></div>');
     });

 });